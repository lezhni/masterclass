$(function() {
    
    
    
    
      $('.header-slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
        speed: 800,
        dots: false,
      fade: true,
          asNavFor: '.header-carousel'
    });
    
     $('.header-carousel').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      dots: false,
        arrows: true,
         centerMode: true,
         centerPadding: 0,
         asNavFor: '.header-slider',
      focusOnSelect: true
    });  
    
     $('.m-courses-slider').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      dots: true,
        arrows: true,
         centerMode: true,
         appendDots: $(".m-courses-dots"),
         centerPadding: '0px',
      focusOnSelect: true,
         asNavFor: '.numb-slider',
  responsive: [
    {
      breakpoint: 830,
      settings: {
        slidesToShow: 1,
          centerMode: false
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
    });  
    
     $('.numb-slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: false,
         asNavFor: '.m-courses-slider',
         speed: 300,
         cssEase: 'linear',
        arrows: false
    });   
    
    
    
        // Подключение счетчика: Старт
    
      jQuery('.burger').click(function (e) {
        $(this).toggleClass('burger_active');
        $('.burger-menu__wrap').toggleClass('burger-menu__wrap_active');
    }); 
    
     jQuery('.burger-menu__link').click(function (e) {
        $('.burger').removeClass('burger_active');
        $('.burger-menu__wrap').removeClass('burger-menu__wrap_active');
    }); 
    
     jQuery('.clb').click(function (e) {
        e.preventDefault();
         $.arcticmodal('close');
        $('.call-modal').arcticmodal();
    }); 
    
    jQuery('.reg-btn').click(function (e) {
        e.preventDefault();
        $.arcticmodal('close');
        $('.reg-modal').arcticmodal();
    }); 
    
    jQuery('.podar-btn').click(function (e) {
        e.preventDefault();
        $.arcticmodal('close');
        $('.podarok-modal').arcticmodal();
    }); 
    
    jQuery('.sms-btn').click(function (e) {
        e.preventDefault();
        $.arcticmodal('close');
        $('.sms-modal').arcticmodal();
    }); 
    
    
    jQuery('.promo-btn').click(function (e) {
        e.preventDefault();
        $.arcticmodal('close');
        $('.promo-modal').arcticmodal();
    }); 
    
    jQuery('.promowrong-btn').click(function (e) {
        e.preventDefault();
        $.arcticmodal('close');
        $('.promowrong-modal').arcticmodal();
    }); 
    
    jQuery('.present-btn').click(function (e) {
        e.preventDefault();
        $.arcticmodal('close');
        $('.present-modal').arcticmodal();
    }); 
    
    jQuery('.prize-btn').click(function (e) {
        e.preventDefault();
        $.arcticmodal('close');
        $('.prize-modal').arcticmodal();
    }); 

    jQuery('.dost-btn').click(function (e) {
        e.preventDefault();
        $.arcticmodal('close');
        $('.dost-modal').arcticmodal();
    }); 
    
    
    
    
    
    
    jQuery('.cls').click(function (e) {
        e.preventDefault();
        $.arcticmodal('close');
    }); 
    
    $(".form-modal").submit(function() {
        $.ajax({
            type: "POST",
            url: "/wp-admin/mail.php",
            data: $(this).serialize()
        }).done(function() {
            $(this).find("input").val("");
            $('.form-modal__wrap').addClass('form-modal__wrap_sps');
            $('.spasibo').addClass('spasibo_active');
            setTimeout(function (){
              $('.form-modal__wrap').removeClass('form-modal__wrap_sps');
              $('.spasibo').removeClass('spasibo_active');
              $.arcticmodal('close');
            },6000);                       
            $(".form-modal").trigger("reset"); 
        });
        return false;
    });  
    
    $(".form").submit(function() {
        $.ajax({
            type: "POST",
            url: "/wp-admin/mail.php",
            data: $(this).serialize()
        }).done(function() {
            $(this).find("input").val("");
            $('.sps-modal').arcticmodal();
            setTimeout(function (){
              $.arcticmodal('close');
            },6000);                       
            $(".form-modal").trigger("reset"); 
        });
        return false;
    }); 
     

     jQuery('.burger-menu__link').click(function (e) {
        $('.burger').removeClass('burger_active');
        $('.burger-menu').removeClass('burger-menu_active');
        $('.navigation').removeClass('navigation_active');
    }); 
    
               $(window).scroll(function() {
            $('.why__one').each(function(){
                var imagePos = $(this).offset().top;

                var topOfWindow = $(window).scrollTop();
                if (imagePos < topOfWindow+650) {
                    $(this).addClass("fadeInUp");
                }
            });
        });
    
                   $(window).scroll(function() {
            $('.why__two').each(function(){
                var imagePos = $(this).offset().top;

                var topOfWindow = $(window).scrollTop();
                if (imagePos < topOfWindow+650) {
                    $(this).addClass("fadeInUp");
                }
            });
        });
    
                   $(window).scroll(function() {
            $('.why__three').each(function(){
                var imagePos = $(this).offset().top;

                var topOfWindow = $(window).scrollTop();
                if (imagePos < topOfWindow+650) {
                    $(this).addClass("fadeInUp");
                }
            });
        });
    
                   $(window).scroll(function() {
            $('.why__four').each(function(){
                var imagePos = $(this).offset().top;

                var topOfWindow = $(window).scrollTop();
                if (imagePos < topOfWindow+650) {
                    $(this).addClass("fadeInUp");
                }
            });
        });
    
                   $(window).scroll(function() {
            $('.about__video-control').each(function(){
                var imagePos = $(this).offset().top;

                var topOfWindow = $(window).scrollTop();
                if (imagePos < topOfWindow+650) {
                    $(this).addClass("fadeInUp");
                }
            });
        });
    

         // Sticky nav
        $(window).scroll(function() {
            if ($(this).scrollTop() > 63){
                $('.navigation').addClass("sticky");
            }
            else{
                $('.navigation').removeClass("sticky");
            }
        }); 
    
    jQuery('.video-top__btn').click(function (e) {
        $('.video-modal').fadeIn('');
    }); 
    
    jQuery('.cls-vid').click(function (e) {
        $('.video-modal').fadeOut('');
        var videoURL = $('iframe').prop('src');
        videoURL = videoURL.replace("&autoplay=1", "");
        $('iframe').prop('src','');
        $('iframe').prop('src',videoURL);
    }); 
    
    
    
    jQuery('.courses-sec__link').click(function () {
        jQuery(".courses-sec__link").removeClass('courses-sec__link_active');
        jQuery(this).addClass('courses-sec__link_active');
    });

    jQuery('.courses-sec__link_all').click(function () {
        jQuery(".podarok-all").show();
    });

    jQuery('.courses-sec__link_my').click(function () {
        jQuery(".podarok-all").hide();
        jQuery(".podarok-my").show();
    });
    
    
    

    

    

    
     
    


});
