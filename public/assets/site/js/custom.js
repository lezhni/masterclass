(function ($) {
    $('.cls').on('click', function (e) {
        e.preventDefault();
        $.arcticmodal('close');
    });

    $('.js-modal').on('click', function (e) {
        e.preventDefault();
        var modal = $(this).data('modal');
        if (modal == null) {
            return;
        }

        $(modal).find('.js-form-errors').html('');
        $(modal).find('[type="text"], [type="email"], [type="tel"], textarea').val('');

        $.arcticmodal('close');
        $(modal).arcticmodal();
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.js-account-login').on('submit', function (e) {
        e.preventDefault();
        var form = $(this);
        var submitButton = form.find('button, [type="submit"]');
        var errorsContainer = form.find('.js-form-errors');
        var data = form.serialize();

        var action = form.attr('action');
        if (action == null) {
            console.log('action is not defined');
            return;
        }

        $.post({
            url: action,
            data: data,
            beforeSend: function () {
                errorsContainer.html('');
                submitButton.prop('disabled', true);
            },
            success: function (data) {
                errorsContainer.html('');
                document.location.reload();
            },
            error: function (xhr, error) {
                if (xhr.status === 422) {
                    $.each(xhr.responseJSON.errors, function (index, errors) {
                        $.each(errors, function (index, error) {
                            var errorItem = '<li>' + error + '</li>';
                            errorsContainer.append(errorItem);
                        });
                    });
                } else {
                    errorsContainer.html('<li>Произошла непредвиденная ошибка, попробуйте еще раз</li>');
                }
            },
            complete: function () {
                submitButton.prop('disabled', false);
            }
        });
    });

    $('.js-account-register').on('submit', function (e) {
        e.preventDefault();
        var form = $(this);
        var submitButton = form.find('button, [type="submit"]');
        var errorsContainer = form.find('.js-form-errors');
        var data = form.serialize();

        var action = form.attr('action');
        if (action == null) {
            console.log('action is not defined');
            return;
        }

        $.post({
            url: action,
            data: data,
            beforeSend: function () {
                submitButton.prop('disabled', true);
            },
            success: function (data) {
                errorsContainer.html('');
                $.arcticmodal('close');
                $('.sms-modal').arcticmodal();
            },
            error: function (xhr, error) {
                if (xhr.status === 422) {
                    $.each(xhr.responseJSON.errors, function (index, errors) {
                        $.each(errors, function (index, error) {
                            var errorItem = '<li>' + error + '</li>';
                            errorsContainer.append(errorItem);
                        });
                    });
                } else {
                    errorsContainer.html('<li>Произошла непредвиденная ошибка, попробуйте еще раз</li>');
                }
            },
            complete: function () {
                submitButton.prop('disabled', false);
            }
        });
    });

    $('.js-account-verify').on('submit', function (e) {
        e.preventDefault();
        var form = $(this);
        var submitButton = form.find('button, [type="submit"]');
        var errorsContainer = form.find('.js-form-errors');
        var data = form.serialize();

        var action = form.attr('action');
        if (action == null) {
            console.log('action is not defined');
            return;
        }

        $.post({
            url: action,
            data: data,
            beforeSend: function () {
                submitButton.prop('disabled', true);
            },
            success: function (data) {
                errorsContainer.html('');
                $.arcticmodal('close');
                $('.success-reg-modal').arcticmodal();
            },
            error: function (xhr, error) {
                if (xhr.status === 422) {
                    $.each(xhr.responseJSON.errors, function (index, errors) {
                        $.each(errors, function (index, error) {
                            var errorItem = '<li>' + error + '</li>';
                            errorsContainer.append(errorItem);
                        });
                    });
                } else {
                    errorsContainer.html('<li>Произошла непредвиденная ошибка, попробуйте еще раз</li>');
                }
            },
            complete: function () {
                submitButton.prop('disabled', false);
            }
        });
    });

    $('.js-exercise-complete:not(checked)').on('change', function () {
        var checkbox = $(this);
        var form = checkbox.closest('form');

        var courseId = checkbox.data('course-id');
        var exerciseId = checkbox.data('exercise-id');

        var action = form.attr('action');
        if (action == null) {
            console.log('action is not defined');
            return;
        }

        $.post({
            url: action,
            data: {
                courseId: courseId,
                exerciseId: exerciseId,
            },
            beforeSend: function () {
                checkbox.prop('disabled', true);
            },
            success: function (data) {
                alert(data.message);
                if(data.hasOwnProperty('achievements')) {
                    $.each(data.achievements, function (index, achievement) {
                        var achievementModal = $('.achievement-modal');
                        achievementModal.find('.js-achievement-text').text(achievement);
                        achievementModal.arcticmodal();
                    });
                }
            },
            error: function (xhr, error) {
                checkbox.prop('disabled', false);
                checkbox.prop('checked', false);

                $.each(xhr.responseJSON.errors, function (index, errors) {
                    $.each(errors, function (index, error) {
                        alert(error);
                    });
                });
            }
        });
    });

    $('.js-promocode-activate').on('submit', function (e) {
        e.preventDefault();
        var form = $(this);
        var data = form.serialize();
        var submitButton = form.find('[type="submit"]');
        var submitButtonText = submitButton.text();

        var action = form.attr('action');
        if (action == null) {
            console.log('action is not defined');
            return;
        }

        $.post({
            url: action,
            data: data,
            beforeSend: function () {
                submitButton.prop('disabled', true).text('Активируем...');
            },
            success: function (data) {
                var promocodeModal = $('.promocode-modal');
                promocodeModal.find('.js-promocode-text').text(data.message);
                promocodeModal.arcticmodal();
            },
            error: function (xhr, error) {
                $.each(xhr.responseJSON.errors, function (index, errors) {
                    $.each(errors, function (index, error) {
                        alert(error);
                    });
                });
            },
            complete: function () {
                form.find('[type="text"]').val('');
                submitButton.prop('disabled', false).text(submitButtonText);
            }
        });
    });

})(jQuery);