<?php /** @var \Illuminate\Database\Eloquent\Model $entry */ ?>
@switch($entry->status)
    @case(0)
        Ожидает обработки
        @break
    @case(1)
        В обработке
        @break
    @case(2)
        Завершено с ошибкой
        @break
    @case(3)
        Завершено
@endswitch
