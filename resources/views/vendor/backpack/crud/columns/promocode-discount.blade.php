<?php /** @var \App\Models\Promocode $entry */ ?>
@if ($entry->type === 1) {{ $entry->discount }} ₽ @endif
@if ($entry->type === 2) {{ $entry->discount }} % @endif
@if ($entry->type === 3) Возможность выбрать любой курс/личность @endif
@if ($entry->type === 4) Премиум-подписка на {{ $entry->discount }} месяца(ев) @endif