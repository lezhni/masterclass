<li>
    <a href="{{ backpack_url('dashboard') }}">
        <i class="fa fa-dashboard"></i>
        <span>{{ trans('backpack::base.dashboard') }}</span>
    </a>
</li>

<li>
    <a href="{{ backpack_url('page') }}">
        <i class="fa fa-file-text"></i>
        <span>Страницы</span>
    </a>
</li>

<li class="treeview">
    <a href="#">
        <i class="fa fa-user"></i>
        <span>Личности</span>
        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('person') }}"><i class="fa fa-user"></i> <span>Личности</span></a></li>
        <li><a href="{{ backpack_url('person-theme') }}"><i class="fa fa-video-camera"></i> <span>Темы</span></a></li>
    </ul>
</li>

<li class="treeview">
    <a href="#">
        <i class="fa fa-graduation-cap"></i>
        <span>Курсы</span>
        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('course') }}"><i class="fa fa-graduation-cap"></i> <span>Курсы</span></a></li>
        <li><a href="{{ backpack_url('course-part') }}"><i class="fa fa-list-alt"></i> <span>Части</span></a></li>
        <li><a href="{{ backpack_url('course-video') }}"><i class="fa fa-youtube-play"></i> <span>Видео</span></a></li>
        <li><a href="{{ backpack_url('exercise') }}"><i class="fa fa-list-ol"></i> <span>Задания</span></a></li>
    </ul>
</li>

<li class="treeview">
    <a href="#">
        <i class="fa fa-ticket"></i>
        <span>Промокоды</span>
        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('promocode') }}"><i class="fa fa-ticket"></i> <span>Промокоды</span></a></li>
        <li><a href="{{ backpack_url('promocodes-generator') }}"><i class="fa fa-cogs"></i> <span>Генератор</span></a></li>
    </ul>
</li>

<li class="treeview">
    <a href="#">
        <i class="fa fa-trophy"></i>
        <span>Награды и подарки</span>
        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('achievement') }}"><i class="fa fa-trophy"></i> <span>Награды</span></a></li>
        <li><a href="{{ backpack_url('gift') }}"><i class="fa fa-gift"></i> <span>Подарки</span></a></li>
    </ul>
</li>

<li class="treeview">
    <a href="#">
        <i class="fa fa-users"></i>
        <span>Пользователи</span>
        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('user') }}"><i class="fa fa-users"></i> <span>Пользователи</span></a></li>
        <li><a href="{{ backpack_url('subscription') }}"><i class="fa fa-unlock-alt"></i> <span>Премиум-доступ</span></a></li>
    </ul>
</li>