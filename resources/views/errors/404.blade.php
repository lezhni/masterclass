@extends('site.layout')

@section('content')
    <div class="bread">
        <div class="container">
            <div class="bread__wrap">
                <a href="{{ route('home') }}">Главная</a>
                <span>•</span>
                <p>404</p>
            </div>
        </div>
    </div>
    <section class="not-found">
        <div class="container">
            <div class="not-found__wrap">
                <span>Эта страница не существует</span>
                <h1>404 error</h1>
                <a href="{{ route('home') }}" class="btn not-found__btn">Перейти на главную</a>
            </div>
        </div>
    </section>
@endsection