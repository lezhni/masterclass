<div style="display: none;">

    <div class="call-modal">
        <form class="form-modal js-account-login" action="{{ route('loginUser') }}" method="POST">
            <div class="cls"></div>
            <div class="form-modal__wrap">
                <div class="form-modal__cont">
                    <h2>Вход</h2>
                    <div class="form-modal__inputs">
                        <span>Номер телефона</span>
                        <input class="form-modal__name" type="tel" name="phone" placeholder="Ваш телефон" required>
                        <span>Пароль</span>
                        <input class="form-modal__tel" type="password" name="password" placeholder="Ваш пароль" required>
                        <div class="form-modal__btns">
                            <button class="btn form-modal__btn">Войти</button>
                            <a href="#" data-modal=".reg-modal" class="btn btn_dark form-modal__btn_dark reg-btn js-modal">Регистрация</a>
                        </div>
                    </div>
                    <ul class="js-form-errors"></ul>
                </div>
            </div>
        </form>
    </div>

    <div class="reg-modal">
        <form class="form-modal js-account-register" action="{{ route('registerUser') }}" method="POST">
            <div class="cls"></div>
            <div class="form-modal__wrap">
                <div class="form-modal__cont">
                    <h2>Регистрация</h2>
                    <div class="form-modal__inputs">
                        <span>Логин</span>
                        <input class="form-modal__name" type="text" name="name" placeholder="Ваше имя" required>
                        <span>Номер телефона</span>
                        <input class="form-modal__tel" type="text" name="phone" placeholder="Ваш телефон" required>
                        <div class="form-modal__btns">
                            <button class="btn form-modal__btn">Зарегистрироваться</button>
                        </div>
                    </div>
                    <ul class="js-form-errors"></ul>
                </div>
            </div>
        </form>
    </div>

    <div class="sms-modal">
        <form class="form-modal js-account-verify" action="{{ route('verifyUser') }}" method="POST">
            <div class="cls"></div>
            <div class="form-modal__wrap">
                <div class="form-modal__cont">
                    <div class="form-modal__inputs">
                        <span>Введите код из SMS</span>
                        <input class="form-modal__name" type="text" name="code" placeholder="Код" required>
                        <div class="form-modal__btns">
                            <button class="btn form-modal__btn">Отправить</button>
                        </div>
                    </div>
                    <ul class="js-form-errors"></ul>
                </div>
            </div>
        </form>
    </div>

    <div class="reg-modal feedback-modal">
        <form class="form-modal js-feedback" action="{{ route('feedback') }}" method="POST">
            <div class="cls"></div>
            <div class="form-modal__wrap">
                <div class="form-modal__cont">
                    <h2>Оставить заявку</h2>
                    <div class="form-modal__inputs">
                        <span>Логин</span>
                        <input class="form-modal__name" type="text" name="name" placeholder="Ваше имя" required>
                        <span>Номер телефона</span>
                        <input class="form-modal__tel" type="text" name="phone" placeholder="Ваш телефон" required>
                        <div class="form-modal__btns">
                            <button class="btn form-modal__btn">Зарегистрироваться</button>
                        </div>
                    </div>
                    <ul class="js-form-errors"></ul>
                </div>
            </div>
        </form>
    </div>

    <div class="podarok-modal">
        <form class="form-modal js-product-gift" action="" method="POST">
            <div class="cls"></div>
            <div class="form-modal__wrap">
                <div class="form-modal__cont">
                    <h2>Подарить</h2>
                    <div class="form-modal__inputs">
                        <span>Кому подарить</span>
                        <input class="form-modal__name" type="text" name="name" placeholder="Введите имя" required>
                        <span>Номер телефона</span>
                        <input class="form-modal__tel" type="text" name="phone" placeholder="Введите телефон" required>
                        <span>Комментарий</span>
                        <textarea name="text" placeholder="Введите комментарий"></textarea>
                        <div class="form-modal__btns">
                            <button class="btn form-modal__btn">Отправить</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="promo-modal success-reg-modal">
        <form class="form-modal">
            <div class="cls"></div>
            <div>
                <h3>Регистрация успешно завершена</h3>
                <p>Пароль от личного кабинета выслан на указанный номер телефона</p>
            </div>
        </form>
    </div>

    <div class="promo-modal achievement-modal">
        <form class="form-modal">
            <div class="cls"></div>
            <div>
                <h3>Получена награда!</h3>
                <p class="js-achievement-text"></p>
            </div>
        </form>
    </div>

    <div class="promo-modal promocode-modal">
        <form class="form-modal">
            <div class="cls"></div>
            <div>
                <h3>Активация прошла успешно</h3>
                <p class="js-promocode-text"></p>
            </div>
        </form>
    </div>

</div>