<div class="acess__wrap">
    <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="acess-item">
                <h3>{{ $plans['ultimate']['name'] }}</h3>
                <p>{!! nl2br($plans['ultimate']['desc']) !!}</p>
                <div class="acess-item__time">{{ $plans['ultimate']['duration'] }}</div>
                <div class="acess-item__price">
                    <span>{{ $plans['ultimate']['monthly-price'] }}</span>
                    <p>{{ $plans['ultimate']['price-formatted'] }}</p>
                </div>
                <div class="acess-item__btns">
                    @if(! auth()->check())
                        <a href="#" data-modal=".call-modal" class="btn acess-item__btn js-modal">Купить</a>
                        <a href="#" data-modal=".call-modal" class="btn acess-item__btn btn_dark js-modal">Подарить</a>
                    @else
                        <a href="#" class="btn acess-item__btn js-product-buy">Купить</a>
                        <a href="#" data-modal=".podarok-modal" class="btn acess-item__btn btn_dark js-modal">Подарить</a>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="acess-item">
                <h3>{{ $plans['ultimate-plus']['name'] }}</h3>
                <p>{!! nl2br($plans['ultimate-plus']['desc']) !!}</p>
                <div class="acess-item__time">{{ $plans['ultimate-plus']['duration'] }}</div>
                <div class="acess-item__price">
                    <span>{{ $plans['ultimate-plus']['monthly-price'] }}</span>
                    <p>{{ $plans['ultimate-plus']['price-formatted'] }}</p>
                </div>
                <div class="acess-item__btns">
                    @if(! auth()->check())
                        <a href="#" data-modal=".call-modal" class="btn acess-item__btn js-modal">Купить</a>
                        <a href="#" data-modal=".call-modal" class="btn acess-item__btn btn_dark js-modal">Подарить</a>
                    @else
                        <a href="#" class="btn acess-item__btn js-product-buy">Купить</a>
                        <a href="#" data-modal=".podarok-modal" class="btn acess-item__btn btn_dark js-modal">Подарить</a>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="acess-item">
                <h3>{{ $plans['course']['name'] }}</h3>
                <p>{!! nl2br($plans['course']['desc']) !!}</p>
                <div class="acess-item__time">Навсегда</div>
                <div class="acess-item__price">
                    <p>{{ $plans['course']['price-formatted'] }}</p>
                </div>
                <div class="acess-item__btns">
                    @if(! auth()->check())
                        <a href="#" data-modal=".call-modal" class="btn acess-item__btn acess-item__btn_one js-modal">Выбрать курс</a>
                    @else
                        <a href="{{ route('courses') }}" class="btn acess-item__btn acess-item__btn_one">Выбрать курс</a>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="acess-item">
                <h3>{{ $plans['person']['name'] }}</h3>
                <p>{!! nl2br($plans['person']['desc']) !!}</p>
                <div class="acess-item__time">Навсегда</div>
                <div class="acess-item__price">
                    <p>{{ $plans['person']['price-formatted'] }}</p>
                </div>
                <div class="acess-item__btns">
                    @if(! auth()->check())
                        <a href="#" data-modal=".call-modal" class="btn acess-item__btn acess-item__btn_one js-modal">Выбрать личность</a>
                    @else
                        <a href="{{ route('persons') }}" class="btn acess-item__btn acess-item__btn_one">Выбрать личность</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>