@extends('site.layout')

@section('content')
    <div class="bread">
        <div class="container">
            <div class="bread__wrap">
                <a href="{{ route('home') }}">Главная</a>
                <span>•</span>
                <p>Призы и награды</p>
            </div>
        </div>
    </div>
    @if($contest !== null)
        <section class="presents">
            <div class="container">
                <h2>Подарки и награды</h2>
                <div class="presents__wrap">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="presents__img">
                                <img src="{{ $contest['image'] ?? asset('assets/site/img/present-img.jpg') }}" alt="{{ $contest['title'] }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="presents__cont">
                                @if($contest['date'])
                                    <span>Ближайший конкурс <div>{{ $contest['date'] }}</div></span>
                                @endif
                                <h3 class="presents__h3">{{ $contest['title'] }}</h3>
                                <p>{!! nl2br($contest['description']) !!}</p>
                                @if($contest['prize']['title'])
                                    <span>Победитель получит</span>
                                    <h3>{{ $contest['prize']['title'] }}</h3>
                                    @if($contest['prize']['description'])
                                        <span class="presents__desc">{!! nl2br($contest['prize']['description']) !!}</span>
                                    @endif
                                @endif
                                <div class="presents__btns">
                                    <a href="#" data-modal=".feedback-modal" class="btn presents__btn js-modal">Участвовать</a>
                                    @if($contest['full-description'])
                                        <a href="#" data-modal=".contest-modal" class="btn presents__btn btn_dark js-modal">О конкурсе</a>
                                        <div style="display: none;">
                                            <div class="promo-modal contest-modal">
                                                <form class="form-modal">
                                                    <div class="cls"></div>
                                                    <div>
                                                        <h3>О конкурсе</h3>
                                                        {!! $contest['full-description'] !!}
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif
    @if($contest['winner']['title'])
        <section class="ofer-sec ofer-sec_present">
            <div class="container">
                <div class="ofer-sec__wrap">
                    <div class="ofer-sec__left">
                        <h2>Последний победитель</h2>
                        @if($contest['winner']['description'])
                            <p>{!! nl2br($contest['winner']['description']) !!}</p>
                        @endif
                    </div>
                    <div class="ofer-sec__right">
                        <img src="{{ $contest['winner']['image'] ?? asset('assets/site/img/placeholders/offer.jpg') }}" alt="{{ $contest['winner']['title'] }}">
                        <div class="ofer-sec__name">
                            <h3>{{ $contest['winner']['title'] }}</h3>
                            @if($contest['winner']['city'])
                                <p>{{ $contest['winner']['city'] }}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif
    <section class="courses-sec courses-sec_pres">
        <div class="container">
            <h2>Мои награды</h2>
            <div class="courses-sec__wrap">
                <div class="row">
                    @if(count($userAchievements) !== 0)
                        @foreach($userAchievements as $achievement)
                            <div class="col-lg-4 col-md-6">
                                <div class="more__item">
                                    <img src="{{ $achievement['image'] }}" alt="{{ $achievement['title'] }}">
                                    <div>
                                        <h3>{{ $achievement['title'] }}</h3>
                                        <p>{{ $achievement['description'] }}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div style="width: 100%; text-align: center;">
                            <p>Выполняйте задания курсов, чтобы получить награды</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
    <section class="courses-sec courses-sec_pres courses-sec_tab">
        <div class="container">
            <h2>Подарки</h2>
            <div class="courses-sec__tabs">
                <div class="courses-sec__link courses-sec__link_active courses-sec__link_all">Все подарки</div>
                <div class="courses-sec__link courses-sec__link_my">Мои подарки</div>
            </div>
            <div class="courses-sec__wrap">
                <div class="row">
                    @if(count($gifts) !== 0)
                        @foreach($gifts as $gift)
                            <div class="col-lg-4 col-md-6 podarok-all {{ (in_array($gift, $userGifts)) ? 'podarok-my' : '' }}">
                                <div class="more__item">
                                    <img src="{{ $gift['image'] }}" alt="{{ $gift['title'] }}">
                                    <div>
                                        <h3>{{ $gift['title'] }}</h3>
                                        <p>{{ $gift['description'] }}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection