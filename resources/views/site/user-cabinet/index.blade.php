@extends('site.layout')

@section('content')
    <div class="bread">
        <div class="container">
            <div class="bread__wrap">
                <a href="{{ route('home') }}">Главная</a>
                <span>•</span>
                <p>Личный кабинет</p>
            </div>
        </div>
    </div>
    <section class="courses-sec courses-sec_lc courses-sec_top">
        <div class="container">
            <h2>Курсы <span>({{ count($userCourses) }})</span></h2>
            <div class="courses-sec__wrap">
                <div class="row">
                    @if(count($userCourses) !== 0)
                        @foreach($userCourses as $course)
                            <div class="col-md-4">
                                <a href="{{ route('courses.course', ['alias' => $course['alias']]) }}" class="more__item">
                                    <img src="{{ $course['image'] }}" alt="{{ $course['title'] }}">
                                    <div><h3>{{ $course['title'] }}</h3></div>
                                </a>
                            </div>
                        @endforeach
                    @else
                        <div style="width: 100%; text-align: center;">
                            <p>У вас пока что нет ни одного приобретенного курса. <a href="{{ route('courses') }}">Выбрать подходящий</a></p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
    <section class="courses-sec courses-sec_lc">
        <div class="container">
            <h2>Личности <span>({{ count($userPersons) }})</span></h2>
            <div class="courses-sec__wrap">
                <div class="row">
                    @if(count($userPersons) !== 0)
                        @foreach($userPersons as $person)
                            <div class="col-md-4">
                                <a href="{{ route('persons.person', ['alias' => $person['alias']]) }}" class="more__item">
                                    <img src="{{ $person['image'] }}" alt="{{ $person['title'] }}">
                                    <div><h3>{{ $person['title'] }}</h3></div>
                                </a>
                            </div>
                        @endforeach
                    @else
                        <div style="width: 100%; text-align: center;">
                            <p>У вас пока что нет ни одного приобретенного мастер-класса от личностей. <a href="{{ route('persons') }}">Выбрать подходящий</a></p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
    @if(count($plans) !== 0)
        <section class="acess acess_sec">
            <div class="shine"></div>
            <div class="container">
                <h2>Доступы</h2>
                @include('site.blocks.pricing-plans')
            </div>
        </section>
    @endif
@endsection