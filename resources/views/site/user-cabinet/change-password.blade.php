<form action="{{ route('changePassword') }}" method="POST">
    <input type="password" name="password" placeholder="Новый пароль">
    <input type="password" name="password_repeat" placeholder="Повторите ввод">
    <input type="submit" value="Сменить пароль">
    @csrf
</form>

@if ($errors->any())
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif