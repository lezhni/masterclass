@extends('site.layout')

@section('content')
    <div class="bread">
        <div class="container">
            <div class="bread__wrap">
                <a href="{{ route('home') }}">Главная</a>
                <span>•</span>
                <a href="{{ route('persons') }}">Личности</a>
                <span>•</span>
                <a href="{{ route('persons.person', ['alias' => $person['alias']]) }}">{{ $person['title'] }}</a>
                <span>•</span>
                <p>{{ $theme['title'] }}</p>
            </div>
        </div>
    </div>
    <section class="video-top">
        <div class="container">
            <div class="video-top__img">
                <img src="{{ $person['big_image'] }}" alt="{{ $person['title'] }} - {{ $theme['title'] }}">
                <a href="#" class="video-top__btn"></a>
            </div>
        </div>
    </section>
    <div class="video-modal">
        <div class="container">
            <div class="video-modal__wrap">
                <div class="cls-vid"></div>
                <div class="video-modal__cont">{!! $theme['video_code'] !!}</div>
            </div>
        </div>
    </div>
    <section class="sec-top">
        <div class="container">
            <span>{{ $theme['title'] }}</span>
            <h1>{{ $person['title'] }}</h1>
            <p>{!! nl2br($theme['description']) !!}</p>
        </div>
    </section>
    @if(count($person['themes']) !== 0)
        <section class="courses-sec">
            <div class="container">
                <h2>Другие темы личности</h2>
                <div class="courses-sec__wrap">
                    <div class="row">
                        @foreach($person['themes'] as $anotherTheme)
                        @php if ($theme['alias'] === $anotherTheme['alias']) continue; @endphp
                            <div class="col-md-4">
                                <a href="{{ route('persons.theme', ['personAlias' => $person['alias'], 'themeAlias' => $anotherTheme['alias']]) }}" class="more__item">
                                    <img src="{{ $person['image'] }}" alt="{{ $anotherTheme['title'] }}">
                                    <div><h3>{{ $anotherTheme['title'] }}</h3></div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
                <a href="{{ route('persons.person', ['personAlias' => $person['alias']]) }}" class="courses-sec__btn btn">Показать все</a>
            </div>
        </section>
    @endif
@endsection