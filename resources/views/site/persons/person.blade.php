@extends('site.layout')

@section('content')
    <div class="bread">
        <div class="container">
            <div class="bread__wrap">
                <a href="{{ route('home') }}">Главная</a>
                <span>•</span>
                <a href="{{ route('persons') }}">Личности</a>
                <span>•</span>
                <p>{{ $person['title'] }}</p>
            </div>
        </div>
    </div>
    <section class="video-top">
        <div class="container">
            <div class="video-top__img">
                <img src="{{ $person['big_image'] }}" alt="{{ $person['title'] }}">
                <a href="#" class="video-top__btn"></a>
            </div>
        </div>
    </section>
    <div class="video-modal">
        <div class="container">
            <div class="video-modal__wrap">
                <div class="cls-vid"></div>
                <div class="video-modal__cont">{!! $person['trailer_code'] !!}</div>
            </div>
        </div>
    </div>
    <section class="sec-top">
        <div class="container">
            <h1>{{ $person['title'] }}</h1>
        </div>
    </section>
    <section class="sec-desc">
        <div class="container">
            <div class="sec-desc__wrap">
                <div class="sec-desc__item">
                    <div class="sec-desc__img">
                        <img src="{{ asset('assets/site/img/goal.svg') }}" alt="{{ $person['advantages'][0]['title'] }}">
                    </div>
                    <h3>{{ $person['advantages'][0]['title'] }}</h3>
                    <p>{!! nl2br($person['advantages'][0]['text']) !!}</p>
                </div>
                <span></span>
                <div class="sec-desc__item">
                    <div class="sec-desc__img">
                        <img src="{{ asset('assets/site/img/goal.svg') }}" alt="{{ $person['advantages'][1]['title'] }}">
                    </div>
                    <h3>{{ $person['advantages'][1]['title'] }}</h3>
                    <p>{!! nl2br($person['advantages'][1]['text']) !!}</p>
                </div>
                <span></span>
                <div class="sec-desc__contr">
                    @if (! auth()->check())
                        <a href="#" data-modal=".call-modal" class="sec-desc__btn btn js-modal">Получить полный доступ</a>
                        <p><span>{{ $plans['ultimate-plus']['price'] }} руб. / год</span> - доступ ко всем разделам</p>
                        <div class="sec-desc__or"><p>или</p></div>
                        <div class="sec-desc__btns">
                            <a href="#" data-modal=".call-modal" class="btn_dark btn js-modal">Купить доступ</a>
                            <a href="#" data-modal=".call-modal" class="btn btn_dark js-modal"><img src="{{ asset('assets/site/img/gift.svg') }}" alt=""> Подарить</a>
                        </div>
                        <p class="sec-desc__price">{{ $plans['person']['price'] }} руб.</p>
                    @else
                        <a href="#" class="sec-desc__btn btn js-product-buy">Получить полный доступ</a>
                        <p><span>{{ $plans['ultimate-plus']['price'] }} руб. / год</span> - доступ ко всем разделам</p>
                        <div class="sec-desc__or"><p>или</p></div>
                        <div class="sec-desc__btns">
                            <a href="#" class="btn_dark btn js-product-buy">Купить доступ</a>
                            <a href="#" data-modal=".podarok-modal" class="btn btn_dark js-modal"><img src="{{ asset('assets/site/img/gift.svg') }}" alt=""> Подарить</a>
                        </div>
                        <p class="sec-desc__price">{{ $plans['person']['price'] }} руб.</p>
                    @endif
                </div>

            </div>
        </div>
    </section>
    <section class="ofer-sec">
        <div class="container">
            <div class="ofer-sec__wrap">
                <div class="ofer-sec__left">
                    <h2>{{ $person['offer']['title'] }}</h2>
                    <p>{!! nl2br($person['offer']['text']) !!}</p>
                </div>
                <div class="ofer-sec__right">
                    <img src="{{ $person['offer']['image'] }}" alt="{{ $person['offer']['title'] }}">
                </div>
            </div>
        </div>
    </section>
    <section class="cennosti">
        <div class="container">
            <div class="cennosti__wrap">
                <div class="row">
                    @foreach($person['values'] as $value)
                        <div class="col-md-4">
                            <div class="cennosti__item">
                                <div class="cennosti__img">
                                    <img src="{{ asset('assets/site/img/cennosti-img.svg') }}" alt="{{ $value['title'] }}">
                                </div>
                                <h3>{{ $value['title'] }}</h3>
                                <p>{!! nl2br($value['text']) !!}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <section class="garanty">
        <div class="container">
            <div class="garanty__wrap">
                <div class="garanty__img">
                    <img src="{{ asset('assets/site/img/guarantee.svg') }}" alt="{{ $person['guaranty']['title'] }}">
                </div>
                <div class="garanty__cont">
                    <h3>{{ $person['guaranty']['title'] }}</h3>
                    <p>{!! nl2br($person['guaranty']['text']) !!}</p>
                </div>
            </div>
        </div>
    </section>
    @if(count($person['themes']) !== 0)
        <section class="peaces">
            <div class="container">
                <h2>Темы личности</h2>
                <ul class="peaces__wrap">
                    @foreach($person['themes'] as $theme)
                        <li>
                            <a href="{{ route('persons.theme', ['personAlias' => $person['alias'], 'themeAlias' => $theme['alias']]) }}">
                                <span>{{ $theme['number'] }}</span>
                                <div>
                                    <h3>{{ $theme['title'] }}</h3>
                                    <p>{{ $theme['description'] }}</p>
                                </div>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </section>
    @endif
    @if(count($plans) !== 0)
        <section class="acess acess_sec">
            <div class="shine"></div>
            <div class="container">
                <h2>Доступы</h2>
                @include('site.blocks.pricing-plans')
            </div>
        </section>
    @endif
@endsection