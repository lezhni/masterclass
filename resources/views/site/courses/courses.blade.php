@extends('site.layout')

@section('content')
    <div class="bread">
        <div class="container">
            <div class="bread__wrap">
                <a href="{{ route('home') }}">Главная</a>
                <span>•</span>
                <p>Курсы</p>
            </div>
        </div>
    </div>
    @if($highlightedCourse !== null)
        <section class="video-top">
            <div class="container">
                <div class="video-top__img">
                    <img src="{{ $highlightedCourse['big_image'] }}" alt="{{ $highlightedCourse['title'] }}">
                    <a href="#" class="video-top__btn"></a>
                </div>
            </div>
        </section>
        <div class="video-modal">
            <div class="container">
                <div class="video-modal__wrap">
                    <div class="cls-vid"></div>
                    <div class="video-modal__cont">{!! $highlightedCourse['trailer_code'] !!}</div>
                </div>
            </div>
        </div>
        <section class="sec-top">
            <div class="container">
                <h1>{{ $highlightedCourse['title'] }}</h1>
            </div>
        </section>
        <section class="sec-desc">
            <div class="container">
                <div class="sec-desc__wrap">
                    <div class="sec-desc__item">
                        <div class="sec-desc__img">
                            <img src="{{ asset('assets/site/img/goal.svg') }}" alt="{{ $highlightedCourse['advantages'][0]['title'] }}">
                        </div>
                        <h3>{{ $highlightedCourse['advantages'][0]['title'] }}</h3>
                        <p>{!! nl2br($highlightedCourse['advantages'][0]['text']) !!}</p>
                    </div>
                    <span></span>
                    <div class="sec-desc__item">
                        <div class="sec-desc__img">
                            <img src="{{ asset('assets/site/img/goal.svg') }}" alt="{{ $highlightedCourse['advantages'][1]['title'] }}">
                        </div>
                        <h3>{{ $highlightedCourse['advantages'][1]['title'] }}</h3>
                        <p>{!! nl2br($highlightedCourse['advantages'][1]['text']) !!}</p>
                    </div>
                    <span></span>
                    <div class="sec-desc__contr">
                        @if (! auth()->check())
                            <a href="#" data-modal=".call-modal" class="sec-desc__btn btn js-modal">Получить полный доступ</a>
                            <p><span>{{ $plans['ultimate-plus']['price'] }} руб. / год</span> - доступ ко всем разделам</p>
                            <div class="sec-desc__or"><p>или</p></div>
                            <div class="sec-desc__btns">
                                <a href="#" data-modal=".call-modal" class="btn_dark btn js-modal">Купить доступ</a>
                                <a href="#" data-modal=".call-modal" class="btn btn_dark js-modal"><img src="{{ asset('assets/site/img/gift.svg') }}" alt=""> Подарить</a>
                            </div>
                            <p class="sec-desc__price">{{ $plans['course']['price'] }} руб.</p>
                        @else
                            <a href="#" class="sec-desc__btn btn js-product-buy">Получить полный доступ</a>
                            <p><span>{{ $plans['ultimate-plus']['price'] }} руб. / год</span> - доступ ко всем разделам</p>
                            <div class="sec-desc__or"><p>или</p></div>
                            <div class="sec-desc__btns">
                                <a href="#" class="btn_dark btn js-product-buy">Купить доступ</a>
                                <a href="#" data-modal=".podarok-modal" class="btn btn_dark js-modal"><img src="{{ asset('assets/site/img/gift.svg') }}" alt=""> Подарить</a>
                            </div>
                            <p class="sec-desc__price">{{ $plans['course']['price'] }} руб.</p>
                        @endif
                    </div>

                </div>
            </div>
        </section>
        <section class="ofer-sec">
            <div class="container">
                <div class="ofer-sec__wrap">
                    <div class="ofer-sec__left">
                        <h2>{{ $highlightedCourse['offer']['title'] }}</h2>
                        <p>{!! nl2br($highlightedCourse['offer']['text']) !!}</p>
                    </div>
                    <div class="ofer-sec__right">
                        <img src="{{ $highlightedCourse['offer']['image'] }}" alt="{{ $highlightedCourse['offer']['title'] }}">
                    </div>
                </div>
                <a href="{{ route('courses.course', ['alias' => $highlightedCourse['alias']]) }}" class="btn ofer-sec__btn">Подробнее о курсе</a>
            </div>
        </section>
    @endif
    @if(count($courses) !== 0)
        <section class="courses-sec">
            <div class="container">
                <h2>Курсы</h2>
                <div class="courses-sec__wrap">
                    <div class="row">
                        @foreach($courses as $course)
                            <div class="col-md-4">
                                <a href="{{ route('courses.course', ['alias' => $course['alias']]) }}" class="more__item">
                                    <img src="{{ $course['image'] }}" alt="{{ $course['title'] }}">
                                    <div><h3>{{ $course['title'] }}</h3></div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    @endif
    @if(count($plans) !== 0)
        <section class="acess acess_sec">
            <div class="shine"></div>
            <div class="container">
                <h2>Доступы</h2>
                @include('site.blocks.pricing-plans')
            </div>
        </section>
    @endif
@endsection