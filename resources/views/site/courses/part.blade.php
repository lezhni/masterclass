@extends('site.layout')

@section('content')
    <div class="bread">
        <div class="container">
            <div class="bread__wrap">
                <a href="{{ route('home') }}">Главная</a>
                <span>•</span>
                <a href="{{ route('courses') }}">Курсы</a>
                <span>•</span>
                <a href="{{ route('courses.course', ['alias' => $course['alias']]) }}">{{ $course['title'] }}</a>
                <span>•</span>
                <p>{{ $part['title'] }}</p>
            </div>
        </div>
    </div>
    <section class="video-top">
        <div class="container">
            <div class="video-top__img">
                <img src="{{ $part['big_image'] }}" alt="{{ $part['title'] }}">
                <a href="#" class="video-top__btn"></a>
            </div>
        </div>
    </section>
    <div class="video-modal">
        <div class="container">
            <div class="video-modal__wrap">
                <div class="cls-vid"></div>
                <div class="video-modal__cont">{!! $part['trailer_code'] !!}</div>
            </div>
        </div>
    </div>
    <section class="sec-top">
        <div class="container">
            <span>{{ $part['title'] }}</span>
            <h1>{{ $course['title'] }}</h1>
            <p>{!! nl2br($part['description']) !!}</p>
        </div>
    </section>
    @if(count($courseParts) !== 0)
        <section class="peaces peaces_sec">
            <div class="container">
                <h2>Части курса</h2>
                <ul class="peaces__wrap">
                    @foreach($courseParts as $anotherPart)
                        <a href="{{ route('courses.part', ['courseAlias' => $course['alias'], 'partAlias' => $anotherPart['alias']]) }}" {{ ($part['alias'] === $anotherPart['alias']) ? 'class=active' : '' }}>
                            <span>{{ $anotherPart['number'] }}</span>
                            <div><h3>{{ $anotherPart['title'] }}</h3></div>
                        </a>
                    @endforeach
                </ul>
            </div>
        </section>
    @endif
    @if(count($part['videos']) !== 0)
        <section class="courses-sec courses-sec_noboard">
            <div class="container">
                <h2>Личности в этой части</h2>
                <div class="courses-sec__wrap">
                    <div class="row">
                        @foreach($part['videos'] as $video)
                            <div class="col-md-4">
                                <a href="{{ route('courses.video', ['courseAlias' => $course['alias'], 'partAlias' => $part['alias'], 'videoAlias' => $video['alias']]) }}" class="more__item">
                                    <img src="{{ $video['image'] }}" alt="{{ $video['title'] }}">
                                    <div><h3>{{ $video['title'] }}</h3></div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    @endif
    @if(count($plans) !== 0)
        <section class="acess acess_sec">
            <div class="shine"></div>
            <div class="container">
                <h2>Доступы</h2>
                @include('site.blocks.pricing-plans')
            </div>
        </section>
    @endif
@endsection