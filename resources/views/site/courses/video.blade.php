@extends('site.layout')

@section('content')
    <div class="bread">
        <div class="container">
            <div class="bread__wrap">
                <a href="{{ route('home') }}">Главная</a>
                <span>•</span>
                <a href="{{ route('courses') }}">Курсы</a>
                <span>•</span>
                <a href="{{ route('courses.course', ['alias' => $course['alias']]) }}">{{ $course['title'] }}</a>
                <span>•</span>
                <a href="{{ route('courses.part', ['courseAlias' => $course['alias'], 'partAlias' => $part['alias']]) }}">{{ $part['title'] }}</a>
                <span>•</span>
                <p>{{ $video['title'] }}</p>
            </div>
        </div>
    </div>
    <section class="video-top">
        <div class="container">
            <div class="video-top__img">
                <img src="{{ $video['big_image'] }}" alt="{{ $video['title'] }}">
                <a href="#" class="video-top__btn"></a>
            </div>
        </div>
    </section>
    <div class="video-modal">
        <div class="container">
            <div class="video-modal__wrap">
                <div class="cls-vid"></div>
                <div class="video-modal__cont">{!! $video['video_code'] !!}</div>
            </div>
        </div>
    </div>
    <section class="sec-top">
        <div class="container">
            <span>{{ $part['title'] }}</span>
            <h1>{{ $video['title'] }}</h1>
            <p>{!! nl2br($video['description']) !!}</p>
        </div>
    </section>
    <section class="tasc">
        <div class="container">
            <div class="tasc__wrap">
                <div class="row">
                    <div class="col-lg-6">
                        @if(count($video['exercises']) !== 0)
                            <h2>Выполните задание по видео</h2>
                            <p>Отметьте выполненное задание после выполнения</p>
                            <form class="tasc-items" action="{{ route('exercise.complete') }}" method="POST">
                                @foreach($video['exercises'] as $exercise)
                                    <div class="tasc-item">
                                        <div class="tasc-item__input">
                                            <input
                                                type="checkbox"
                                                id="exercise-{{ $exercise['id'] }}"
                                                class="js-exercise-complete"
                                                data-course-id="{{ $course['id'] }}"
                                                data-exercise-id="{{ $exercise['id'] }}"
                                                @if(in_array($exercise, $userExercises)) checked="checked" disabled="disabled" @endif
                                            >
                                            <label for="exercise-{{ $exercise['id'] }}"></label>
                                        </div>
                                        <div class="tasc-item__cont">
                                            <h3>{{ $exercise['title'] }}</h3>
                                            <p>{{ $exercise['description'] }}</p>
                                        </div>
                                    </div>
                                @endforeach
                            </form>
                        @endif
                    </div>
                    <div class="col-lg-5 ml-auto">
                        @if(count($courseParts) !== 0)
                            <section class="peaces peaces_sec peaces_sm">
                                <h2>Все части курса</h2>
                                <ul class="peaces__wrap">
                                    @foreach($courseParts as $anotherPart)
                                        <a href="{{ route('courses.part', ['courseAlias' => $course['alias'], 'partAlias' => $anotherPart['alias']]) }}" {{ ($part['alias'] === $anotherPart['alias']) ? 'class=active' : '' }}>
                                            <span>{{ $anotherPart['number'] }}</span>
                                            <div><h3>{{ $anotherPart['title'] }}</h3></div>
                                        </a>
                                    @endforeach
                                </ul>
                            </section>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    @if(count($partVideos) !== 0)
        <section class="courses-sec courses-sec_noboard">
            <div class="container">
                <h2>Другие видео в данной части</h2>
                <div class="courses-sec__wrap">
                    <div class="row">
                        @foreach($partVideos as $anotherVideo)
                            <div class="col-md-4">
                                <a href="{{ route('courses.video', ['courseAlias' => $course['alias'], 'partAlias' => $part['alias'], 'videoAlias' => $anotherVideo['alias']]) }}" class="more__item">
                                    <img src="{{ $anotherVideo['image'] }}" alt="{{ $anotherVideo['title'] }}">
                                    <div><h3>{{ $anotherVideo['title'] }}</h3></div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    @endif
@endsection