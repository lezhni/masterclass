<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>StarMasterClass</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ url('assets/site/img/favicon/fav.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ url('assets/site/img/favicon/apple-touch-icon-180x180.png') }}">
    <link rel="stylesheet" href="{{ url('assets/site/css/main.min.css?v=1') }}">
</head>
<body id="body">
    <nav class="navigation">
        <div class="container">
            <div class="navigation__wrap">
                <a href="{{ route('home') }}" class="logo"><img src="{{ url('assets/site/img/logo.svg') }}" alt="MasterClass"></a>
                <ul class="menu">
                    <li><a href="{{ route('courses') }}">Курсы</a></li>
                    <li><a href="{{ route('persons') }}">Личности</a></li>
                    <li><a href="{{ route('pricing') }}">Доступы</a></li>
                    @if (! auth()->check())
                        <li><a href="#" data-modal=".call-modal" class="js-modal">Призы и награды</a></li>
                    @else
                        <li><a href="{{ route('userCabinet.achievements') }}">Призы и награды</a></li>
                    @endif
                </ul>
                <div class="login">
                    @if(! auth()->check())
                        <a href="#" data-modal=".call-modal" class="login__enter js-modal">Вход</a>
                        <span>/</span>
                        <a href="#" data-modal=".reg-modal" class="login__registration js-modal">Регистрация</a>
                    @else
                        <a href="{{ route('userCabinet') }}" class="login__registration">Мои доступы</a>
                        <span>/</span>
                        <a href="{{ route('logoutUser') }}" class="login__logout">Выход</a>
                    @endif
                </div>
            </div>
        </div>
    </nav>

    @yield('content')

    <footer class="footer">
        <div class="container">
            <div class="footer__wrap">
                <ul class="menu">
                    <li><a href="{{ route('courses') }}">Курсы</a></li>
                    <li><a href="{{ route('persons') }}">Личности</a></li>
                    <li><a href="{{ route('pricing') }}">Доступы</a></li>
                    @if (! auth()->check())
                        <li><a href="#" data-modal=".call-modal" class="burger-menu__link go_to js-modal">Призы и награды</a></li>
                    @else
                        <li><a href="{{ route('userCabinet.achievements') }}" class="burger-menu__link go_to">Призы и награды</a></li>
                    @endif
                </ul>
                @if(! auth()->check())
                    <a href="#" data-modal=".call-modal" class="why__btn btn btn_black js-modal">
                        <img src="{{ url('assets/site/img/person-fut.svg') }}" alt="">
                        <div>вход/регистрация</div><img src="{{ url('assets/site/img/btn-arr.svg') }}" alt="">
                    </a>
                @endif
            </div>
            <div class="footer__bottom">
                <div class="credits">
                    ООО «МастерКласс», ИНН 29320234, КПП 823042342, <br>
                    Юр. Адрес: Москва, улица Просторная, дом 10, <br>
                    <span>{{ date('Y') }} © Все права защищены. </span>
                </div>
                <a href="https://vse.digital" target="_blank" class="vse">
                    <p>Разработка сайта <br>студия</p>
                    <img src="{{ url('assets/site/img/vse.png') }}" alt="Разработка сайта — Всё.digital">
                </a>
            </div>
        </div>
    </footer>
    <div class="bg__wrap">
        <div class="container">
            <div class="burger bg"></div>
        </div>
    </div>
    <div class="burger-menu__wrap">
        <div class="container">
            <div class="burger-menu__cont">
                <ul class="burger-menu">
                    <li><a href="{{ route('home') }}" class="burger-menu__link go_to">Главная</a></li>
                    <li><a href="{{ route('courses') }}" class="burger-menu__link go_to">Курсы</a></li>
                    <li><a href="{{ route('persons') }}" class="burger-menu__link go_to">Личности</a></li>
                    <li><a href="{{ route('pricing') }}" class="burger-menu__link go_to">Доступы</a></li>
                    @if (! auth()->check())
                        <li><a href="#" data-modal=".call-modal" class="burger-menu__link go_to js-modal">Призы и награды</a></li>
                    @else
                        <li><a href="{{ route('userCabinet.achievements') }}" class="burger-menu__link go_to">Призы и награды</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
    @include('site.blocks.modals')
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="{{ url('assets/site/js/scripts.min.js?v=1') }}"></script>
    <script src="{{ url('assets/site/js/custom.js?v=1') }}"></script>
</body>
</html>