@extends('site.layout')

@section('content')
    <div class="bread">
        <div class="container">
            <div class="bread__wrap">
                <a href="{{ route('home') }}">Главная</a>
                <span>•</span>
                <p>Доступы</p>
            </div>
        </div>
    </div>
    <section class="acess acess_sec acess_main">
        <div class="shine"></div>
        <div class="container">
            <h2>Доступы</h2>
            @include('site.blocks.pricing-plans')
        </div>
    </section>
    <section class="promo-code">
        <div class="container">
            <h2>Промокод</h2>
            @if(! auth()->check())
                <div class="promo-code__wrap">
                    <input type="text" name="promocode" placeholder="Введите промокод" required>
                    <a class="btn promo-code__btn js-modal" href="#" data-modal=".call-modal">Активировать</a>
                </div>
            @else
                <form class="promo-code__wrap js-promocode-activate" action="{{ route('promocode.activate') }}" method="POST">
                    <input type="text" name="promocode" placeholder="Введите промокод" required>
                    <button type="submit" class="btn promo-code__btn">Активировать</button>
                </form>
            @endif
        </div>
    </section>
    <section class="contacts">
        <div class="container">
            <h2>Контакты</h2>
            <div class="contacts__wrap">
                <p>8 800 842 99 42</p>
                <p>support@masterclass.ru</p>
            </div>
        </div>
    </section>
@endsection