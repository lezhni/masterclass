@extends('site.layout')

@section('content')
    <header class="header">
        <div class="header-slider">
            @if(count($slider) !== 0)
                @foreach($slider as $slide)
                    <div>
                        <div class="header-slide">
                            <div class="container">
                                <div class="header-slide__cont">
                                    <div class="header-slide__img">
                                        <img src="{{ $slide['slider_image'] ?? asset('assets/site/img/placeholders/slider.png') }}" alt="{{ $slide['person'] }} - {{ $slide['title'] }}">
                                    </div>
                                    <h1>{{ $slide['person'] }}</h1>
                                    <div class="header-slide__subtitle">{{ $slide['title'] }}</div>
                                    <p>Получите полную годовую <br>подписку на все курсы от всех звёзд</p>
                                    <div class="header-slide__price"><span>{{ $plans['ultimate-plus']['price'] }}</span> руб. / год</div>
                                    <div class="header-slide__btns">
                                        @if(! auth()->check())
                                            <a href="#" data-modal=".call-modal" class="header-slide__btn btn js-modal">Получить доступ к личности</a>
                                            <a href="#" data-modal=".call-modal" class="header-slide__btn btn btn_dark js-modal"><img src="{{ asset('assets/site/img/gift.svg') }}" alt="">Подарить</a>
                                        @else
                                            <a href="#" class="header-slide__btn btn js-product-buy">Получить доступ к личности</a>
                                            <a href="#" data-modal=".podarok-modal" class="header-slide__btn btn btn_dark js-modal"><img src="{{ asset('assets/site/img/gift.svg') }}" alt="">Подарить</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
        <div class="header-carousel__wrap">
            <div class="row header-carousel">
                @if(count($slider) !== 0)
                    @foreach($slider as $slide)
                        <div>
                            <div class="header-carousel__slide col-auto">
                                <img src="{{ $slide['image'] ?? asset('assets/site/img/placeholders/card.png') }}" alt="{{ $slide['person'] }} - {{ $slide['title'] }}">
                                <div>
                                    <h3>{{ $slide['person'] }}</h3>
                                    <p>{{ $slide['title'] }}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </header>
    <section class="why">
        <div class="container">
            <div class="why__wrap">
                <a href="{{ route('persons') }}" class="why__btn btn btn_black">
                    <img src="{{ asset('assets/site/img/star.svg') }}" alt="">
                    <div>Все личности</div><img src="{{ asset('assets/site/img/btn-arr.svg') }}" alt="Посмотреть всех личностей">
                </a>
                <img src="{{ asset('assets/site/img/why-bg.png') }}" alt="" class="why__bg">
                <img src="{{ asset('assets/site/img/why-micro.jpg') }}" alt="" class="why__one animated">
                <img src="{{ asset('assets/site/img/why-mus.jpg') }}" alt="" class="why__two animated">
                <img src="{{ asset('assets/site/img/why-cor.jpg') }}" alt="" class="why__three animated">
                <img src="{{ asset('assets/site/img/why-comp.jpg') }}" alt="" class="why__four animated">
                <div class="why__items">
                    <div class="why__item">
                        <div class="why__img">
                            <img src="{{ asset('assets/site/img/why-tube.svg') }}" alt="">
                        </div>
                        <h3>{{ $page['about'][0]['title'] }}</h3>
                        <p>{!! nl2br($page['about'][0]['text']) !!}</p>
                    </div>
                    <div class="why__line"></div>
                    <div class="why__item">
                        <div class="why__img">
                            <img src="{{ asset('assets/site/img/why-cap.png') }}" alt="">
                        </div>
                        <h3>{{ $page['about'][1]['title'] }}</h3>
                        <p>{!! nl2br($page['about'][1]['text']) !!}</p>
                    </div>
                    <div class="why__line"></div>
                    <div class="why__item">
                        <div class="why__img">
                            <img src="{{ asset('assets/site/img/why-time.svg') }}" alt="">
                        </div>
                        <h3>{{ $page['about'][2]['title'] }}</h3>
                        <p>{!! nl2br($page['about'][2]['text']) !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="m-courses">
        <div class="container">
            <h2>Курсы</h2>
        </div>
        <div class="m-courses-slider__wrap">
            <div class="shine"></div>
            <div class="numb-slider">
                @foreach ($courses as $index => $course)
                <div><div class="numb-slide"><span>{{ (++$index) }}</span>/{{ count($courses) }}</div></div>
                @endforeach
            </div>
            <div class="m-courses-slider">
                @foreach ($courses as $course)
                    <div>
                        <a href="{{ route('courses.course', ['alias' => $course['alias']]) }}" class="m-courses__slide">
                            <img src="{{ $course['image'] }}" alt="{{ $course['title'] }}">
                            <div><h3>{{ $course['title'] }}</h3></div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="container">
            <div class="m-courses__bottom">
                <div class="number-five">
                    <img src="{{ asset('assets/site/img/number-five.png') }}" alt="Посмореть все курсы">
                </div>
                <div class="m-courses-dots"></div>
                <a href="{{ route('courses') }}" class="why__btn btn btn_black m-courses__btn">
                    <div>Все курсы</div>
                    <img src="{{ asset('assets/site/img/btn-arr.svg') }}" alt="Посмореть все курсы">
                </a>
            </div>
        </div>
    </section>
    <section class="about">
        <div class="container">
            <h2>О проекте</h2>
            <div class="about__wrap">
                <div class="row">
                    <div class="col-md-9">
                        <div class="about__video">
                            <div class="about__video-control animated">
                                <p>Посмотрите <br>2-х минутное <br>видео</p>
                                <a href="{{ $page['video'] }}" target="_blank"></a>
                            </div>
                            <img src="{{ $page['image'] ?? asset('assets/site/img/basc-video.jpg') }}" alt="Посмотрите 2-х минутное видео">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="about__items">
                            <div class="about__item">
                                <img src="{{ url('assets/site/img/about-point.png') }}" alt="{{ $page['advantages'][0] }}">
                                <p>{!! nl2br($page['advantages'][0]) !!}</p>
                            </div>
                            <div class="about__item">
                                <img src="{{ url('assets/site/img/about-point.png') }}" alt="{{ $page['advantages'][1] }}">
                                <p>{!! nl2br($page['advantages'][1]) !!}</p>
                            </div>
                            <div class="about__item">
                                <img src="{{ url('assets/site/img/about-point.png') }}" alt="{{ $page['advantages'][2] }}">
                                <p>{!! nl2br($page['advantages'][2]) !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="about__info">
                <div class="row">
                    <div class="col-lg-3">
                        <h3>{!! nl2br($page['project']['title']) !!}</h3>
                    </div>
                    <div class="col-md-3 d-none d-lg-block">
                        <div class="about__line"></div>
                    </div>
                    <div class="col-lg-6">
                        <p>{!! nl2br($page['project']['text']) !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{--<section class="more">
        <div class="container">
            <h2>Смотрите также</h2>
            <div class="more__wrap">
                <div class="row">
                    <div class="col-md-6">
                        <a href="#" class="more__item">
                            <img src="{{ url('assets/site/img/galkin.jpg') }}" alt="">
                            <div>
                                <h3>Максим Галкин</h3>
                                <p>Мода и красота</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <a href="#" class="more__item">
                            <img src="{{ url('assets/site/img/galkin.jpg') }}" alt="">
                            <div>
                                <h3>Максим Галкин</h3>
                                <p>Мода и красота</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>--}}
    @if(count($plans) !== 0)
        <section class="acess">
            <div class="shine"></div>
            <div class="container">
                <h2>Доступы</h2>
                @include('site.blocks.pricing-plans')
            </div>
        </section>
    @endif
@endsection