<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', \App\Http\Controllers\Site\HomePageController::class)->name('home');
Route::get('pricing', \App\Http\Controllers\Site\PricingPageController::class)->name('pricing');

Route::group(['prefix' => 'persons'], function() {
    Route::get('/', \App\Http\Controllers\Site\PersonsController::class . '@persons')->name('persons');
    Route::get('{personAlias}', \App\Http\Controllers\Site\PersonsController::class . '@person')->name('persons.person');
    Route::get('{personAlias}/{themeAlias}', \App\Http\Controllers\Site\PersonsController::class . '@theme')->middleware('access.person')->name('persons.theme');
});

Route::group(['prefix' => 'courses'], function() {
    Route::get('/', \App\Http\Controllers\Site\CoursesController::class . '@courses')->name('courses');
    Route::get('{courseAlias}', \App\Http\Controllers\Site\CoursesController::class . '@course')->name('courses.course');
    Route::get('{courseAlias}/{partAlias}', \App\Http\Controllers\Site\CoursesController::class . '@part')->middleware('access.course')->name('courses.part');
    Route::get('{courseAlias}/{partAlias}/{videoAlias}', \App\Http\Controllers\Site\CoursesController::class . '@video')->middleware('access.course')->name('courses.video');
});

Route::post('exercise', \App\Http\Controllers\Site\ExercisesController::class)->name('exercise.complete');
Route::post('promocode', \App\Http\Controllers\Site\PromocodeController::class)->name('promocode.activate');
Route::post('feedback', \App\Http\Controllers\Site\FeedbackController::class)->name('feedback');

Route::group(['prefix' => 'user-cabinet', 'middleware' => 'auth'], function() {
    Route::get('/', \App\Http\Controllers\Site\UserCabinetController::class)->name('userCabinet');
    Route::get('gifts-achievements', \App\Http\Controllers\Site\UserCabinetController::class . '@achievements')->name('userCabinet.achievements');
    // Route::get('change-password', \App\Http\Controllers\Site\UserCabinetController::class . '@passwordChanging')->name('changePasswordPage');
    // Route::post('change-password', \App\Http\Controllers\Site\UserCabinetController::class . '@changePassword')->name('changePassword');
});

Route::post('login', \App\Http\Controllers\Site\Auth\LoginController::class . '@login')->name('loginUser');
Route::get('logout', \App\Http\Controllers\Site\Auth\LoginController::class . '@logout')->name('logoutUser');
Route::post('registration', \App\Http\Controllers\Site\Auth\RegistrationController::class . '@register')->name('registerUser');
Route::post('verification', \App\Http\Controllers\Site\Auth\RegistrationController::class . '@verify')->name('verifyUser');
// Route::get('restore-password', \App\Http\Controllers\Site\Auth\RestorePasswordController::class . '@passwordRestoration')->name('passwordRestorationPage');
// Route::post('restore-password', \App\Http\Controllers\Site\Auth\RestorePasswordController::class . '@restorePassword')->name('restorePassword');
// Route::get('restore-password/verify', \App\Http\Controllers\Site\Auth\RestorePasswordController::class . '@passwordVerification')->name('passwordVerificationPage');
// Route::post('restore-password/verify', \App\Http\Controllers\Site\Auth\RestorePasswordController::class . '@verifyPassword')->name('verifyPassword');

Route::group(['prefix' => config('backpack.base.route_prefix'), 'middleware' => 'admin'], function() {
    CRUD::resource('user', \App\Http\Controllers\Admin\UserCrudController::class);
    CRUD::resource('person', \App\Http\Controllers\Admin\PersonCrudController::class);
    CRUD::resource('person-theme', \App\Http\Controllers\Admin\PersonThemeCrudController::class);
    CRUD::resource('course', \App\Http\Controllers\Admin\CourseCrudController::class);
    CRUD::resource('course-part', \App\Http\Controllers\Admin\CoursePartCrudController::class);
    CRUD::resource('course-video', \App\Http\Controllers\Admin\CourseVideoCrudController::class);
    CRUD::resource('subscription', \App\Http\Controllers\Admin\SubscriptionCrudController::class);
    CRUD::resource('achievement', \App\Http\Controllers\Admin\AchievementCrudController::class);
    CRUD::resource('exercise', \App\Http\Controllers\Admin\ExerciseCrudController::class);
    CRUD::resource('gift', \App\Http\Controllers\Admin\GiftCrudController::class);
    CRUD::resource('promocode', \App\Http\Controllers\Admin\PromocodeCrudController::class);
    CRUD::resource('promocodes-generator', \App\Http\Controllers\Admin\PromocodesGeneratorCrudController::class);
});