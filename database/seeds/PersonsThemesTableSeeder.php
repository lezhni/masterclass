<?php

use App\Models\Person;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class PersonsThemesTableSeeder extends Seeder
{
    public function run()
    {
        $now = Carbon::now();
        $lastPerson = Person::first();
        if (! $lastPerson instanceof Person) {
            return;
        }

        DB::table('personsThemes')->insert([
            [
                'title' => 'Любовь и красота',
                'description' => 'Обходит каждое значение массива array, передавая его в callback-функцию. Если callback-функция возвращает true, данное значение из array возвращается в результирующий массив. Ключи массива сохраняются.',
                'alias' => 'lubov-i-krasota',
                'video_code' => '<iframe width="100%" height="100%" src="https://www.youtube.com/embed/UQV3bZc3fHM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>',
                'person_id' => $lastPerson->id,
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'title' => 'Молодость и здоровье',
                'description' => 'Обходит каждое значение массива array, передавая его в callback-функцию. Если callback-функция возвращает true, данное значение из array возвращается в результирующий массив. Ключи массива сохраняются.',
                'alias' => 'molodost-i-zdorovie',
                'video_code' => '<iframe width="100%" height="100%" src="https://www.youtube.com/embed/UQV3bZc3fHM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>',
                'person_id' => $lastPerson->id,
                'created_at' => $now,
                'updated_at' => $now,
            ],
        ]);
    }
}
