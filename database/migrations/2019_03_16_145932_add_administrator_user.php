<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;

class AddAdministratorUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        $admin = new User();
        $admin->name = 'Администратор';
        $admin->email = 'admin@masterclass.local';
        $admin->password = Hash::make('password');
        $admin->is_admin = true;
        $admin->created_at = Carbon::now();
        $admin->updated_at = Carbon::now();
        $admin->saveOrFail();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        User::where('email', 'admin@masterclass.local')->delete();
    }
}
