<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Carbon;

class AddFirstContest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('contests')->insert([
            'content' => 'Условия конкурса',
            'winner' => 'Победитель',
            'published' => false,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('contests')->delete();
    }
}
