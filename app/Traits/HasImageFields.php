<?php

namespace App\Traits;

use Illuminate\Support\Str;
use Image;
use Storage;

trait HasImageFields
{
    /**
     * @param $value
     */
    public function setImageAttribute($value)
    {
        $this->processImageField($value, 'image');
    }

    /**
     * @param $value
     */
    public function setBigImageAttribute($value)
    {
        $this->processImageField($value, 'big_image');
    }

    /**
     * @param $value
     */
    public function setSliderImageAttribute($value)
    {
        $this->processImageField($value, 'slider_image');
    }

    /**
     * @param $value
     */
    public function setOfferImageAttribute($value)
    {
        $this->processImageField($value, 'offer_image');
    }

    /**
     * @param $value
     * @param string $fieldName
     */
    private function processImageField($value, string $fieldName)
    {
        if ($value === null) {
            Storage::disk('images')->delete($this->{$fieldName});
            $this->attributes[$fieldName] = null;
            return;
        }

        if (! Str::startsWith($value, 'data:image')) {
            return;
        }

        $image = Image::make($value)->encode('png', 100);
        $filename = md5($value.time()).'.png';
        $path = self::IMAGES_DIR . '/' . $filename;

        Storage::disk('images')->put($path, $image->stream());
        $this->attributes[$fieldName] = $path;
    }
}