<?php

if (! function_exists('generate_promocode'))
{
    /**
     * @return string
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    function generate_promocode()
    {
        $generatorService = app()->make(\App\Services\GeneratorService::class);
        $generatedCode = $generatorService->generateRandomCode(16);
        return $generatedCode;
    }
}