<?php

namespace App\Services;

use App\Helpers\Formatter;
use App\Models\Promocode;
use App\Models\Subscription;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\UnauthorizedException;

/**
 * Class PromocodesService
 * @package App\Services
 */
class PromocodesService
{
    /**
     * @var \App\Helpers\Formatter
     */
    private $formatter;

    /**
     * PromocodesService constructor.
     * @param \App\Helpers\Formatter $formatter
     */
    public function __construct(Formatter $formatter)
    {
        $this->formatter = $formatter;
    }

    /**
     * Activate an promocode for user.
     *
     * @param \App\Models\Promocode $promocode
     * @param \App\Models\User|null $user
     * @throws \Throwable
     */
    public function activate(Promocode $promocode, $user = null)
    {
        $user = $user ?? Auth::getUser();
        if (! $user instanceof User) {
            throw new UnauthorizedException('Чтобы применить промокод, вы должны быть авторизованы на сайте');
        }

        $now = Carbon::now();

        // Delete all previously activated promocodes of user
        $user->promocodes()->delete();

        $promocode->activated_at = $now;
        $promocode->user()->associate($user);
        $promocode->saveOrFail();

        // Promocode for premium access are used instantly
        if ($promocode->type === Promocode::PREMIUM_TYPE) {
            $this->activatePremiumPromocode($promocode, $user);
        }
    }

    /**
     * Get a promocode's pretty name.
     *
     * @param \App\Models\Promocode $promocode
     * @return string
     */
    public function getPrettyName(Promocode $promocode): string
    {
        $name = 'Промокод';

        switch ($promocode->type) {
            case Promocode::DISCOUNT_SUM_TYPE :
                $formattedDiscountSum = $this->formatter->formatWithCurrency($promocode->discount);
                $name = "Промокод на скидку в {$formattedDiscountSum} при покупке курса/личности";
                break;

            case Promocode::DISCOUNT_PERCENTS_TYPE :
                $name = "Промокод на скидку в {$promocode->discount}% при покупке курса/личности";
                break;

            case Promocode::FREE_CONTENT_TYPE :
                $name = 'Промокод на бесплатную личность/курс';
                break;

            case Promocode::PREMIUM_TYPE :
                $formattedDuration = $this->formatter->formatDurationInMonths($promocode->discount);
                $name = "Промокод на премиум-доступ сроком {$formattedDuration}";
                break;
        }

        return $name;
    }

    /**
     * @param \App\Models\Promocode $promocode
     * @param \App\Models\User $user
     * @throws \Throwable
     */
    private function activatePremiumPromocode(Promocode $promocode, User $user)
    {
        $now = Carbon::now();

        $existingSubscription = $user->subscription;
        if ($existingSubscription instanceof Subscription) {
            $subscriptionExpirationDate = $existingSubscription->expires_at->addMonths($promocode->discount);
            $existingSubscription->expires_at = $subscriptionExpirationDate;
            $existingSubscription->updated_at = $now;
            $user->push();
            return;
        }

        $subscriptionExpirationDate = $now->addMonths($promocode->discount);
        $user->subscription()->create([
            'expires_at' => $subscriptionExpirationDate,
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        $promocode->delete();
        return;
    }
}