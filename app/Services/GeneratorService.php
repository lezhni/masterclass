<?php

namespace App\Services;

use Illuminate\Support\Str;

/**
 * Class GeneratorService
 * @package App\Services
 */
class GeneratorService
{
    /**
     * @param int $length
     * @return string
     */
    public function generateVerificationCode($length = 5)
    {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $generatedCode = '';
        for ($i = 0; $i < $length; $i++) {
            $generatedCode .= $characters[rand(0, $charactersLength - 1)];
        }

        return $generatedCode;
    }

    /**
     * @param int $length
     * @return string
     */
    public function generateRandomCode($length = 8)
    {
        return Str::random($length);
    }
}