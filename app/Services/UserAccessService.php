<?php

namespace App\Services;

use App\Models\BackpackUser;
use App\Models\Subscription;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Throwable;

/**
 * Class UserAccessService
 * @package App\Helpers
 */
class UserAccessService
{
    /**
     * Check is user has access to content of selected person.
     *
     * @param \App\Models\Person|\Illuminate\Database\Eloquent\Builder $person
     * @param \App\Models\User|null $user
     * @return bool
     */
    public function canAccessPerson($person, $user = null): bool
    {
        try {
            $user = $this->performBasicCheck($user);
        } catch (Throwable $exception) {
            return false;
        }

        if (! $user instanceof User && $user === true) {
            return true;
        }

        $userPersons = $user->persons;
        $personId = $person->id;
        return $userPersons->contains('id', $personId);
    }

    /**
     * Check is user has access to content of selected course.
     *
     * @param \App\Models\Course|\Illuminate\Database\Eloquent\Builder $course
     * @param \App\Models\User|null $user
     * @return bool
     */
    public function canAccessCourse($course, $user = null): bool
    {
        try {
            $user = $this->performBasicCheck($user);
        } catch (Throwable $exception) {
            return false;
        }

        if (! $user instanceof User && $user === true) {
            return true;
        }

        $userCourses = $user->courses;
        $courseId = $course->id;
        return $userCourses->contains('id', $courseId);
    }


    /**
     * Check is user has access to all courses and persons.
     *
     * @param \App\Models\User|null $user
     * @return bool
     */
    public function hasPremiumAccess($user): bool
    {
        $isHasSubscription = Subscription::active()->where('user_id', $user->id)->exists();
        return $isHasSubscription;
    }

    /**
     * Perform basic access checking.
     *
     * @param \App\Models\User|null $user
     * @return \App\Models\User|\Illuminate\Contracts\Auth\Authenticatable|bool
     * @throws \Exception
     */
    protected function performBasicCheck($user)
    {
        // Site administrator has access to all data.
        if (backpack_user() instanceof BackpackUser) {
            return true;
        }

        if ($user === null) {
            $user = Auth::user();
        }

        if (! $user instanceof User) {
            throw new \Exception("No authorized users");
        }

        $hasPremium = $this->hasPremiumAccess($user);
        if ($hasPremium) {
            return true;
        }

        return $user;
    }
}