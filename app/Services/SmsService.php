<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Log;

/**
 * Class SmsService
 * @package App\Services
 */
class SmsService
{
    /**
     * @var \GuzzleHttp\Client
     */
    protected $client;

    /**
     * @var string mixed
     */
    protected $apiKey;

    /**
     * SmsService constructor.
     */
    public function __construct()
    {
        $this->apiKey = config('sms.api_key');
        $this->client = new Client();
    }

    /**
     * Send a single sms-message.
     *
     * @param string $to
     * @param string $msg
     * @return bool
     */
    public function send($to, $msg): bool
    {
        try {
            $response = $this->doRequest('send', compact('to', 'msg'));
        } catch (GuzzleException $exception) {
            report($exception);
            return false;
        }

        if ($response['status'] !== 'OK') {
            Log::error("Failed to send sms; error code: {$response['status_code']}");
            return false;
        }

        foreach ($response['sms'] as $phone => $data) {
            if ($data['status'] !== 'OK') {
                Log::error("Failed to send sms for {$phone}; error code: {$data['status_code']}; reason: {$data['status_text']}");
                return false;
            }
        }

        return true;
    }

    /**
     * Do a api request to sms web-service.
     *
     * @param string $action
     * @param array $params
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function doRequest($action, $params): array
    {
        $requestUrl = 'https://sms.ru/sms/' . $action;

        $defaultParams = ['api_id' => $this->apiKey, 'json' => 1];
        $params = array_merge($defaultParams, $params);

        $response = $this->client->request('GET', $requestUrl, ['query' => $params]);

        $responseBody = $response->getBody();
        $responseBody = json_decode($responseBody, true);
        return $responseBody;
    }
}