<?php

namespace App\Http\Requests\Site;

use Illuminate\Foundation\Http\FormRequest;

class FeedbackRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contact' => 'nullable|string',
            'email' => 'nullable|email',
            'phone' => 'required|phone:AUTO,RU,mobile',
            'message' => 'nullable|string',
            'subject' => 'required|string',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email' => 'Невалидный email',
            'phone' => 'Укажите номер телефона в международном формате',
            'subject' => 'Укажите тему заявки',
        ];
    }
}
