<?php

namespace App\Http\Requests\Site;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\UnauthorizedException;

class ExerciseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'courseId' => 'required|exists:courses,id',
            'exerciseId' => 'required|exists:exercises,id',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'courseId.exists' => 'Выбранного курса не существует',
            'courseId.required' => 'Укажите курс, для которого выполняется задание',
            'exerciseId.exists' => 'Выбранного задания не существует',
            'exerciseId.required' => 'Укажите выполняемое задание',
        ];
    }

    /**
     * Handle a failed authorization attempt.
     *
     * @return void
     *
     */
    protected function failedAuthorization()
    {
        throw new UnauthorizedException('Авторизуйтесь на сайте для выполнения заданий');
    }


}
