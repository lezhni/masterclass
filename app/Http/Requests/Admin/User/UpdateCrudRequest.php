<?php

namespace App\Http\Requests\Admin\User;

class UpdateCrudRequest extends AbstractCrudRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->request->get('id');
        $rules = parent::rules();

        $rules['phone'] = 'required|phone:AUTO,RU,mobile|unique:users,phone,' . $id;
        $rules['password'] = 'nullable|min:8';
        $rules['password_repeat'] = 'nullable|required_with:password|same:password';
        return $rules;
    }
}
