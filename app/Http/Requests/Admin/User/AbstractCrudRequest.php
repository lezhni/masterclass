<?php

namespace App\Http\Requests\Admin\User;

use Illuminate\Foundation\Http\FormRequest;

class AbstractCrudRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'phone' => 'required|phone:AUTO,RU,mobile|unique:users,phone',
            'password' => 'required|min:8',
            'password_repeat' => 'required|same:password',
            'persons' => 'nullable|exists:persons,id',
            'courses' => 'nullable|exists:courses,id',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Имя обязательно',
            'phone.required' => 'Телефон обязателен',
            'phone.phone' => 'Введите номер телефона в международном формате',
            'phone.unique' => 'Пользователь с таким телефоном уже существует',
            'password.min' => 'Пароль должен содержать от 8 символов',
            'password.required' => 'Введите пароль',
            'password_repeat.required' => 'Повторите пароль',
            'password_repeat.required_with' => 'Повторите пароль',
            'password_repeat.same' => 'Пароль и повтор пароля не совпадают',
            'persons.exists' => 'Выбранная личность(и) не существует',
            'courses.exists' => 'Выбранный курс(ы) не существует',
        ];
    }
}
