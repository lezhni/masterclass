<?php

namespace App\Http\Requests\Admin\CoursePart;

use Illuminate\Foundation\Http\FormRequest;

class AbstractCrudRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'description' => 'nullable|string',
            'alias' => 'nullable|string|unique:coursesParts,alias',
            'trailer_code' => 'required|string',
            'big_image' => 'nullable',
            'course_id' => 'required|integer|exists:courses,id',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'Введите название части',
            'alias.unique' => 'Такой алиас уже используется',
            'trailer_code.required' => 'Добавьте iframe для видео трейлера',
            'course_id.required' => 'Выберите связанный курс',
            'course_id.exists' => 'Выбранный курс не существует',
        ];
    }
}
