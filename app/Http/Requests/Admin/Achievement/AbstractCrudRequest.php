<?php

namespace App\Http\Requests\Admin\Achievement;

use Illuminate\Foundation\Http\FormRequest;

class AbstractCrudRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'description' => 'required|string',
            'image' => 'required',
            'course_id' => 'nullable|integer|exists:courses,id|unique:achievements,course_id',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'Название обязательно',
            'description.required' => 'Описание обязательно',
            'image.required' => 'Изображение обязательно',
            'course__id.required' => 'Выберите курс',
            'course_id.exists' => 'Выбранный курс не существует',
        ];
    }
}