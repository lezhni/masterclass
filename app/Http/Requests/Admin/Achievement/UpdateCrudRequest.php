<?php

namespace App\Http\Requests\Admin\Achievement;

class UpdateCrudRequest extends AbstractCrudRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->request->get('id');
        $rules = parent::rules();

        $rules['course_id'] = 'nullable|integer|exists:courses,id|unique:achievements,course_id,' . $id;
        return $rules;
    }
}