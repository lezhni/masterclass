<?php

namespace App\Http\Requests\Admin\Person;

use Illuminate\Foundation\Http\FormRequest;

class AbstractCrudRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'alias' => 'nullable|string|unique:persons,alias',
            'price' => 'required|integer',
            'trailer_code' => 'required|string',
            'image' => 'nullable',
            'big_image' => 'nullable',
            'slider_image' => 'nullable',

            'advantage-1-title' => 'required|string',
            'advantage-1-text' => 'required|string',
            'advantage-2-title' => 'required|string',
            'advantage-2-text' => 'required|string',
            'offer-title' => 'required|string',
            'offer-text' => 'required|string',
            'offer_image' => 'required',
            'value-1-title' => 'required|string',
            'value-1-text' => 'required|string',
            'value-2-title' => 'required|string',
            'value-2-text' => 'required|string',
            'value-3-title' => 'required|string',
            'value-3-text' => 'required|string',
            'guaranty-title' => 'required|string',
            'guaranty-text' => 'required|string',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'Введите имя личности',
            'alias.unique' => 'Такой алиас уже используется',
            'price.required' => 'Укажите стоимость',
            'trailer_code.required' => 'Добавьте iframe для видео трейлера',
            'advantage-1-title.required' => 'Заполните все продающие элементы',
            'advantage-1-text.required' => 'Заполните все продающие элементы',
            'advantage-2-title.required' => 'Заполните все продающие элементы',
            'advantage-2-text.required' => 'Заполните все продающие элементы',
            'offer-title.required' => 'Заполните все продающие элементы',
            'offer-text.required' => 'Заполните все продающие элементы',
            'offer_image.required' => 'Загрузите изображение оффера',
            'value-1-title.required' => 'Заполните все продающие элементы',
            'value-1-text.required' => 'Заполните все продающие элементы',
            'value-2-title.required' => 'Заполните все продающие элементы',
            'value-2-text.required' => 'Заполните все продающие элементы',
            'value-3-title.required' => 'Заполните все продающие элементы',
            'value-3-text.required' => 'Заполните все продающие элементы',
            'guaranty-title.required' => 'Заполните все продающие элементы',
            'guaranty-text.required' => 'Заполните все продающие элементы',
        ];
    }
}
