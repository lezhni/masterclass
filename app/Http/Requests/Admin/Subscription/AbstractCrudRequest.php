<?php

namespace App\Http\Requests\Admin\Subscription;

use Illuminate\Foundation\Http\FormRequest;

class AbstractCrudRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|integer|exists:users,id|unique:subscriptions,user_id',
            'expires_at' => 'nullable|date|after:now',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required' => 'Выберите пользователя',
            'integer' => 'Невалидный ID пользователя',
            'exists' => 'Выбранный пользователь не существует',
            'unique' => 'У выбранного пользователя уже есть подписка',
            'date' => 'Укажите валидную дату',
            'after' => 'Дата окончания подписки должна быть позже текущей даты',
        ];
    }
}