<?php

namespace App\Http\Requests\Admin\Subscription;

class UpdateCrudRequest extends AbstractCrudRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->request->get('id');
        $rules = parent::rules();

        $rules['user_id'] = 'required|integer|exists:users,id|unique:subscriptions,user_id,' . $id;
        return $rules;
    }
}