<?php

namespace App\Http\Requests\Admin\CourseVideo;

class UpdateCrudRequest extends AbstractCrudRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->request->get('id');
        $rules = parent::rules();

        $rules['alias'] = 'nullable|string|unique:coursesVideos,alias,' . $id;
        return $rules;
    }
}