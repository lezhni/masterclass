<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PromocodesGeneratorCrudRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|integer|in:1,2,3,4',
            'amount' => 'required|integer',
            'discount' => 'nullable|required_if:type,1|required_if:type,2|required_if:type,4|integer',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'type.required' => 'Тип обязателен',
            'amount.required' => 'Укажите количество промокодов для генерации',
            'discount.required_if' => 'Значение скидки или срок премиум-подписки обязательны для указанного типа',
            'discount.integer' => 'Значение скидки или срок премиум-подписки должны быть числом',
        ];
    }
}