<?php

namespace App\Http\Requests\Admin\PersonTheme;

use Illuminate\Foundation\Http\FormRequest;

class AbstractCrudRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'description' => 'nullable|string',
            'alias' => 'nullable|string|unique:themes,alias',
            'video_code' => 'required|string',
            'person_id' => 'required|integer|exists:persons,id',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'Введите название темы',
            'alias.unique' => 'Такой алиас уже используется',
            'video_code.required' => 'Добавьте iframe для видео',
            'person_id.required' => 'Выберите связанную личность',
            'person_id.exists' => 'Выбранная личность не существует',
        ];
    }
}
