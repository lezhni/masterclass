<?php

namespace App\Http\Requests\Admin\PersonTheme;

class UpdateCrudRequest extends AbstractCrudRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->request->get('id');
        $rules = parent::rules();

        $rules['alias'] = 'nullable|string|unique:personsThemes,alias,' . $id;
        return $rules;
    }
}