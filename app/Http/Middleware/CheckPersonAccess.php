<?php

namespace App\Http\Middleware;

use App\Models\Person;
use App\Services\UserAccessService;
use Closure;
use Illuminate\Support\Facades\Route;

/**
 * Class CheckPersonAccess
 * @package App\Http\Middleware
 */
class CheckPersonAccess
{
    /**
     * @var \App\Services\UserAccessService
     */
    private $accessService;

    /**
     * CheckPersonAccess constructor.
     * @param \App\Services\UserAccessService $accessService
     */
    public function __construct(UserAccessService $accessService)
    {
        $this->accessService = $accessService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $currentRoute = Route::current();
        $personAlias = $currentRoute->parameter('personAlias');

        $person = Person::where('alias', $personAlias)->first();
        if (! $person instanceof Person) {
            return $next($request);
        }

        $canAccess = $this->accessService->canAccessPerson($person);
        if (! $canAccess) {
            return redirect()->route('persons.person', $personAlias);
        }

        return $next($request);
    }
}
