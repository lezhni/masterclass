<?php

namespace App\Http\Middleware;

use App\Models\Course;
use App\Models\CoursePart;
use App\Services\UserAccessService;
use Closure;
use Illuminate\Support\Facades\Route;

/**
 * Class CheckCourseAccess
 * @package App\Http\Middleware
 */
class CheckCourseAccess
{
    /**
     * @var \App\Services\UserAccessService
     */
    private $accessService;

    /**
     * CheckCourseAccess constructor.
     * @param \App\Services\UserAccessService $accessService
     */
    public function __construct(UserAccessService $accessService)
    {
        $this->accessService = $accessService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $currentRoute = Route::current();
        $courseAlias = $currentRoute->parameter('courseAlias');
        $partAlias = $currentRoute->parameter('partAlias');

        $course = Course::where('alias', $courseAlias)->first();
        if (! $course instanceof Course) {
            return $next($request);
        }

        $part = $course->parts()->where('alias', $partAlias)->first();
        if (! $part instanceof CoursePart) {
            return $next($request);
        }

        $canAccess = $this->accessService->canAccessCourse($course);
        if (! $canAccess) {
            return redirect()->route('courses.course', compact('courseAlias'));
        }

        return $next($request);
    }
}
