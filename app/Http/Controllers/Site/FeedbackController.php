<?php

namespace App\Http\Controllers\Site;

use App\Helpers\FeedbackHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Site\FeedbackRequest;
use App\Jobs\SendFeedback;

class FeedbackController extends Controller
{
    protected $feedbackHelper;

    public function __construct(FeedbackHelper $feedbackHelper)
    {
        $this->feedbackHelper = $feedbackHelper;
    }

    public function __invoke(FeedbackRequest $request)
    {
        $data = $request->validated();
        $application = $this->feedbackHelper->prepareApplicationData($data);

        SendFeedback::dispatch($application);

        return response()->json([
            'status' => 'success',
            'message' => 'Ваша заявка успешно отправлена!',
        ]);
    }
}
