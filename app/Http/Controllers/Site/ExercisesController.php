<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Requests\Site\ExerciseRequest;
use App\Models\Achievement;
use App\Models\Course;
use App\Models\Exercise;
use App\Services\UserAccessService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\UnauthorizedException;

class ExercisesController extends Controller
{
    /**
     * @var \App\Services\UserAccessService
     */
    private $accessService;

    /**
     * ExercisesController constructor.
     * @param \App\Services\UserAccessService $accessService
     */
    public function __construct(UserAccessService $accessService)
    {
        $this->accessService = $accessService;
    }

    /**
     * Complete an exercise and get an achievements (probably).
     *
     * @param \App\Http\Requests\Site\ExerciseRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(ExerciseRequest $request)
    {
        $courseId = $request->post('courseId');
        $course = Course::find($courseId);

        $hasAccess = $this->accessService->canAccessCourse($course);
        if ($hasAccess) {
            throw new UnauthorizedException('У вас нет доступа к заданиям выбранного курса');
        }

        $exerciseId = $request->post('exerciseId');
        $exercise = Exercise::find($exerciseId);

        $user = Auth::user();
        if ($user->exercises->contains($exercise)) {
            return response()->json([
                'message' => 'Задание уже выполнено',
            ]);
        }

        /** @var \Illuminate\Database\Eloquent\Collection $userCourseExercises */
        $userCourseExercises = $user->exercises()->whereCourseIs($course)->get();
        /** @var \Illuminate\Database\Eloquent\Collection $courseExercises */
        $courseExercises = $course->exercises;

        $user->exercises()->save($exercise);
        $userCourseExercises->add($exercise);

        // Check, is user finished all exercises in course, unlock a new achievement for him
        $isCourseFinished = $courseExercises->diff($userCourseExercises)->isEmpty();
        if (! $isCourseFinished) {
            return response()->json([
                'message' => 'Задание отмечено как выполненное',
            ]);
        }

        $achievement = $course->achievement;
        if ($user->achievements->contains($achievement)) {
            return response()->json([
                'message' => "Награда за завершение этого курса уже получено",
            ]);
        }

        $userAchievements = $user->achievements;
        $user->achievements()->save($achievement);
        $userAchievements->add($achievement);

        // Check, is user unlocked all achievements, unlock a last achievement(s)
        $achievements = Achievement::has('course')->get();

        $isCoursesFinished = $achievements->diff($userAchievements)->isEmpty();
        if (! $isCoursesFinished) {
            return response()->json([
                'message' => "Задание отмечено как выполненное",
                'achievements' => [
                    "Вы получили награду «{$achievement->title}»!",
                ],
            ]);
        }

        // Unlock for user a meta-achievement (achievement for achievements)
        $finalAchievement = Achievement::doesntHave('course')->first();
        if ($userAchievements->contains($finalAchievement)) {
            return response()->json([
                'message' => "Награда за завершение всех курсов уже получена",
            ]);
        }

        $user->achievements()->save($finalAchievement);

        return response()->json([
            'message' => "Задание отмечено как выполненное",
            'achievements' => [
                "Вы завершили все курсы, поздравляем!. Награда «{$finalAchievement->title}» пополняет вашу копилку",
            ]
        ]);
    }
}
