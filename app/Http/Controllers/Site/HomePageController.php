<?php

namespace App\Http\Controllers\Site;

use App\Helpers\CourseHelper;
use App\Helpers\HomePageHelper;
use App\Helpers\PersonHelper;
use App\Helpers\PricingPlans\Helper as PricingPlansHelper;
use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Page;
use App\Models\PersonTheme;
use Throwable;

/**
 * Class HomePageController
 * @package App\Http\Controllers\Site
 */
class HomePageController extends Controller
{
    /**
     * @var \App\Helpers\HomePageHelper
     */
    private $homePageHelper;

    /**
     * @var \App\Helpers\PricingPlans\Helper
     */
    private $pricingPlansHelper;

    /**
     * @var \App\Helpers\CourseHelper
     */
    private $courseHelper;

    /**
     * @var \App\Helpers\PersonHelper
     */
    private $personHelper;

    /**
     * HomePageController constructor.
     * @param \App\Helpers\HomePageHelper $homePageHelper
     * @param \App\Helpers\PricingPlans\Helper $pricingPlansHelper
     * @param \App\Helpers\CourseHelper $courseHelper
     * @param \App\Helpers\PersonHelper $personHelper
     */
    public function __construct(
        HomePageHelper $homePageHelper,
        PricingPlansHelper $pricingPlansHelper,
        CourseHelper $courseHelper,
        PersonHelper $personHelper
    )
    {
        $this->homePageHelper = $homePageHelper;
        $this->pricingPlansHelper = $pricingPlansHelper;
        $this->courseHelper = $courseHelper;
        $this->personHelper = $personHelper;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke()
    {
        try {
            /** @var \App\Models\Page $page */
            $page = Page::findBySlugOrFail('/');
            $page = $this->homePageHelper->prepareHomePageData($page);
        } catch (Throwable $exception) {
            abort(404);
        }

        $sliderThemes = PersonTheme::with(['person'])->where('extras->show_in_slider', '1')->take(5)->get();
        $slider = $this->homePageHelper->prepareSliderData($sliderThemes);

        $courses = Course::take(5)->get();
        $courses = $this->courseHelper->prepareItemsData($courses);

        $persons = [];
        $persons = $this->personHelper->prepareItemsData($persons);

        $plans = $this->pricingPlansHelper->getPricingPlans();

        return view('site.pages.home', compact('page', 'slider', 'courses', 'persons', 'plans'));
    }
}
