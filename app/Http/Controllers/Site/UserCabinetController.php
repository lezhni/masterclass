<?php

namespace App\Http\Controllers\Site;

use App\Helpers\CourseHelper;
use App\Helpers\PersonHelper;
use App\Helpers\PricingPlans\Helper;
use App\Helpers\UserCabinetHelper;
use App\Http\Controllers\Controller;
use App\Models\Achievement;
use App\Models\Course;
use App\Models\Gift;
use App\Models\Person;
use App\Services\UserAccessService;
use Illuminate\Support\Facades\Auth;

/**
 * Class UserCabinetController
 * @package App\Http\Controllers\Site
 */
class UserCabinetController extends Controller
{
    /**
     * @var \App\Helpers\PricingPlans\Helper
     */
    private $pricingPlansHelper;

    /**
     * @var \App\Helpers\CourseHelper
     */
    private $courseHelper;

    /**
     * @var \App\Helpers\PersonHelper
     */
    private $personHelper;

    /**
     * @var \App\Helpers\UserCabinetHelper
     */
    private $userCabinetHelper;

    /**
     * @var \App\Services\UserAccessService
     */
    private $accessService;

    /**
     * UserCabinetController constructor.
     * @param \App\Helpers\PricingPlans\Helper $pricingPlansHelper
     * @param \App\Helpers\CourseHelper $courseHelper
     * @param \App\Helpers\PersonHelper $personHelper
     * @param \App\Helpers\UserCabinetHelper $userCabinetHelper
     * @param \App\Services\UserAccessService $accessService
     */
    public function __construct(
        Helper $pricingPlansHelper,
        CourseHelper $courseHelper,
        PersonHelper $personHelper,
        UserCabinetHelper $userCabinetHelper,
        UserAccessService $accessService
    )
    {
        $this->pricingPlansHelper = $pricingPlansHelper;
        $this->courseHelper = $courseHelper;
        $this->personHelper = $personHelper;
        $this->userCabinetHelper = $userCabinetHelper;
        $this->accessService = $accessService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function __invoke()
    {
        $currentUser = Auth::getUser();

        $hasPremiumAccess = $this->accessService->hasPremiumAccess($currentUser);
        if (! $hasPremiumAccess) {
            $userCourses = Course::whereHas('users', function ($query) use ($currentUser) {
                /** @var \Illuminate\Database\Eloquent\Builder $query */
                $query->where('users.id', $currentUser->id);
            })->get();
            $userPersons = Person::whereHas('users', function ($query) use ($currentUser) {
                /** @var \Illuminate\Database\Eloquent\Builder $query */
                $query->where('users.id', $currentUser->id);
            })->get();
        } else {
            $userCourses = Course::all();
            $userPersons = Person::all();
        }

        $userCourses = $this->courseHelper->prepareItemsData($userCourses);
        $userPersons = $this->personHelper->prepareItemsData($userPersons);

        $plans = $this->pricingPlansHelper->getPricingPlans();

        return view('site.user-cabinet.index', compact('userCourses', 'userPersons', 'plans'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function achievements()
    {
        $currentUser = Auth::getUser();

        $contest = $this->userCabinetHelper->getContest();

        $gifts = Gift::all();
        $gifts = $this->userCabinetHelper->prepareGiftsData($gifts);

        $userAchievements = Achievement::whereHas('users', function ($query) use ($currentUser) {
            /** @var \Illuminate\Database\Eloquent\Builder $query */
            $query->where('users.id', $currentUser->id);
        })->get();
        $userAchievements = $this->userCabinetHelper->prepareAchievementsData($userAchievements);

        $userGifts = Gift::whereHas('users', function ($query) use ($currentUser) {
            /** @var \Illuminate\Database\Eloquent\Builder $query */
            $query->where('users.id', $currentUser->id);
        })->get();
        $userGifts = $this->userCabinetHelper->prepareGiftsData($userGifts);

        return view('site.user-cabinet.achievements', compact('contest', 'userAchievements', 'gifts', 'userGifts'));
    }

    /*public function passwordChanging()
    {
        return view('site.user-cabinet.change-password');
    }*/

    /*public function changePassword(PasswordChangingRequest $request)
    {
        $password = $request->post('password');
        $password = htmlspecialchars($password);

        $currentUser = Auth::user();
        $currentUser->password = Hash::make($password);

        try {
            $currentUser->saveOrFail();
        } catch (Throwable $exception) {
            report($exception);
            return redirect()
                ->back()
                ->withErrors(['message' => 'Произошла непредвиденная ошибка, попробуйте еще раз']);
        }

        return redirect()
            ->route('userCabinet')
            ->with('message', 'Пароль успешно изменен');
    }*/
}
