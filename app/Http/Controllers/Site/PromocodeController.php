<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Requests\Site\PromocodeRequest;
use App\Models\Promocode;
use App\Services\PromocodesService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\UnauthorizedException;
use Throwable;

/**
 * Class PromocodeController
 * @package App\Http\Controllers\Site
 */
class PromocodeController extends Controller
{
    /**
     * @var \App\Services\PromocodesService
     */
    private $promocodeService;

    /**
     * PromocodeController constructor.
     * @param \App\Services\PromocodesService $promocodeService
     */
    public function __construct(PromocodesService $promocodeService)
    {
        $this->promocodeService = $promocodeService;
    }

    /**
     * Activate a promocode.
     *
     * @param \App\Http\Requests\Site\PromocodeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(PromocodeRequest $request)
    {
        $promocode = $request->post('promocode');
        $promocode = htmlspecialchars($promocode);

        /** @var \App\Models\Promocode $promocode */
        $promocode = Promocode::unactivated()->where('code', $promocode)->first();
        if (! $promocode instanceof Promocode) {
            return response()->json([
                'message' => 'Такой промокод не существует',
                'errors' => [
                    'promocode' => [
                        'Такой промокод не существует',
                    ],
                ],
            ], 422);
        }

        try {
            $user = Auth::getUser();
            $this->promocodeService->activate($promocode, $user);
        } catch(UnauthorizedException $exception) {
            return response()->json([
                'message' => 'Чтобы применить промокод, вы должны быть авторизованы на сайте',
                'errors' => [
                    'auth' => [
                        'Чтобы применить промокод, вы должны быть авторизованы на сайте',
                    ],
                ],
            ], 403);
        } catch (Throwable $exception) {
            dd($exception->getMessage());
            return response()->json([
                'message' => 'Произошла непредвиденная ошибка, попробуйте еще раз',
                'errors' => [
                    'code' => [
                        'Произошла непредвиденная ошибка, попробуйте еще раз',
                    ],
                ],
            ], 500);
        }

        $promocodePrettyName = $this->promocodeService->getPrettyName($promocode);
        return response()->json([
            'message' => "{$promocodePrettyName} успешно активирован!",
        ]);
    }
}
