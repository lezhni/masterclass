<?php

namespace App\Http\Controllers\Site;

use App\Helpers\CourseHelper;
use App\Helpers\PersonHelper;
use App\Helpers\PricingPlans\Helper;
use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Exercise;
use App\Models\Person;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Throwable;

/**
 * Class CoursesController
 * @package App\Http\Controllers\Site
 */
class CoursesController extends Controller
{
    /**
     * @var \App\Helpers\CourseHelper
     */
    private $courseHelper;

    /**
     * @var \App\Helpers\PersonHelper
     */
    private $personHelper;

    /**
     * @var \App\Helpers\PricingPlans\Helper
     */
    private $pricingPlansHelper;

    /**
     * CoursesController constructor.
     * @param \App\Helpers\CourseHelper $courseHelper
     * @param \App\Helpers\PersonHelper $personHelper
     * @param \App\Helpers\PricingPlans\Helper $pricingPlansHelper
     */
    public function __construct(CourseHelper $courseHelper, PersonHelper $personHelper, Helper $pricingPlansHelper)
    {
        $this->courseHelper = $courseHelper;
        $this->personHelper = $personHelper;
        $this->pricingPlansHelper = $pricingPlansHelper;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function courses()
    {
        $courses = Course::orderBy('id', 'desc')->get();
        if ($courses->isEmpty()) {
            abort(404);
        }

        $courses = $this->courseHelper->prepareItemsData($courses);
        $highlightedCourse = $this->courseHelper->getHighlightedCourse($courses);

        $plans = $this->pricingPlansHelper->getPricingPlans();

        return view('site.courses.courses', compact('courses', 'highlightedCourse', 'plans'));
    }

    /**
     * @param string $courseAlias
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function course($courseAlias)
    {
        try {
            /** @var \App\Models\Course $course */
            $course = Course::with('parts')->where('alias', $courseAlias)->firstOrFail();
        } catch (Throwable $exception) {
            abort(404);
        }

        $courseVideos = $course->videos;
        $course = $this->courseHelper->prepareItemData($course);

        /** @var Collection $courses */
        $courses = Course::where('alias', '!=', $course['alias'])->take(3)->get();
        $courses = $this->courseHelper->prepareItemsData($courses);

        $coursePersons = Person::whereHas('videos', function ($query) use ($courseVideos) {
            /** @var \Illuminate\Database\Eloquent\Builder $query */
            $videosAliases = $courseVideos->pluck('alias')->toArray();
            $query->whereIn('alias', $videosAliases);
        })->get();
        $coursePersons = $this->personHelper->prepareItemsData($coursePersons);

        $plans = $this->pricingPlansHelper->getPricingPlans();

        return view('site.courses.course', compact('course', 'courses', 'coursePersons', 'plans'));
    }

    /**
     * @param string $courseAlias
     * @param string $partAlias
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function part($courseAlias, $partAlias)
    {
        try {
            /** @var \App\Models\Course $course */
            $course = Course::with('parts')->where('alias', $courseAlias)->firstOrFail();
        } catch (Throwable $exception) {
            abort(404);
        }

        try {
            /** @var \App\Models\CoursePart $part */
            $part = $course->parts()->where('alias', $partAlias)->firstOrFail();
            $part->load('videos');
        } catch (Throwable $exception) {
            abort(404);
        }

        $courseParts = $course->parts;
        $courseParts = $this->courseHelper->preparePartsData($courseParts);

        $course = $this->courseHelper->prepareItemData($course);
        $part = $this->courseHelper->preparePartData($part);

        $plans = $this->pricingPlansHelper->getPricingPlans();

        return view('site.courses.part', compact('course', 'part', 'courseParts', 'plans'));
    }

    /**
     * @param string $courseAlias
     * @param string $partAlias
     * @param string $videoAlias
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function video($courseAlias, $partAlias, $videoAlias)
    {
        try {
            /** @var \App\Models\Course $course */
            $course = Course::with('parts')->where('alias', $courseAlias)->firstOrFail();
        } catch (Throwable $exception) {
            abort(404);
        }

        try {
            /** @var \App\Models\CoursePart $part */
            $part = $course->parts()->where('alias', $partAlias)->firstOrFail();
            $part->load('videos');
        } catch (Throwable $exception) {
            abort(404);
        }

        try {
            /** @var \App\Models\CourseVideo $video */
            $video = $part->videos()->where('alias', $videoAlias)->firstOrFail();
            $video->load(['person', 'exercises']);
        } catch (Throwable $exception) {
            abort(404);
        }

        $courseParts = $this->courseHelper->preparePartsData($course->parts);

        $partVideos = $part->videos->filter(function ($item) use ($video) {
            return $item->alias !== $video->alias;
        });
        $partVideos = $this->courseHelper->prepareVideosData($partVideos);

        $userExercises = [];
        $user = Auth::getUser();
        if ($user instanceof User) {
            $userExercises = Exercise::whereHas('users', function ($query) use ($user) {
                /** @var \Illuminate\Database\Eloquent\Builder $query */
                $query->where('users.id', $user->id);
            })->get();
            $userExercises = $this->courseHelper->prepareExercisesData($userExercises);
        }

        $course = $this->courseHelper->prepareItemData($course);
        $part = $this->courseHelper->preparePartData($part);
        $video = $this->courseHelper->prepareVideoData($video);

        return view('site.courses.video', compact('course', 'courseParts', 'part', 'partVideos', 'video', 'userExercises'));
    }
}