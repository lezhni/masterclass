<?php

namespace App\Http\Controllers\Site;

use App\Helpers\PricingPlans\Helper;
use App\Http\Controllers\Controller;
use App\Models\Page;
use Throwable;

/**
 * Class PricingPageController
 * @package App\Http\Controllers\Site
 */
class PricingPageController extends Controller
{
    /**
     * @var \App\Helpers\PricingPlans\Helper
     */
    private $pricingPlansHelper;

    /**
     * PricingPageController constructor.
     * @param \App\Helpers\PricingPlans\Helper $pricingPlansHelper
     */
    public function __construct(Helper $pricingPlansHelper)
    {
        $this->pricingPlansHelper = $pricingPlansHelper;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke()
    {
        try {
            /** @var \App\Models\Page $page */
            $page = Page::findBySlugOrFail('/pricing');
        } catch (Throwable $exception) {
            abort(404);
        }

        $plans = $this->pricingPlansHelper->getPricingPlans();
        if (count($plans) === 0) {
            abort(404);
        }

        return view('site.pages.pricing', compact('plans'));
    }
}
