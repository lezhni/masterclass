<?php

namespace App\Http\Controllers\Site;

use App\Helpers\PersonHelper;
use App\Helpers\PricingPlans\Helper;
use App\Http\Controllers\Controller;
use App\Models\Person;
use Throwable;

/**
 * Class PersonsController
 * @package App\Http\Controllers\Site
 */
class PersonsController extends Controller
{
    /**
     * @var \App\Helpers\PersonHelper
     */
    private $personHelper;

    /**
     * @var \App\Helpers\PricingPlans\Helper
     */
    private $pricingPlansHelper;

    /**
     * PersonsController constructor.
     * @param \App\Helpers\PersonHelper $personHelper
     * @param \App\Helpers\PricingPlans\Helper $pricingPlansHelper
     */
    public function __construct(PersonHelper $personHelper, Helper $pricingPlansHelper)
    {
        $this->personHelper = $personHelper;
        $this->pricingPlansHelper = $pricingPlansHelper;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function persons()
    {
        $persons = Person::orderBy('id', 'desc')->get();
        if ($persons->isEmpty()) {
            abort(404);
        }

        $persons = $this->personHelper->prepareItemsData($persons);
        $highlightedPerson = $this->personHelper->getHighlightedPerson($persons);

        $plans = $this->pricingPlansHelper->getPricingPlans();

        return view('site.persons.persons', compact('persons', 'highlightedPerson', 'plans'));
    }

    /**
     * @param $personAlias
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function person($personAlias)
    {
        try {
            /** @var \App\Models\Person $person */
            $person = Person::with('themes')->where('alias', $personAlias)->firstOrFail();
        } catch (Throwable $exception) {
            abort(404);
        }

        $person = $this->personHelper->prepareItemData($person);

        $plans = $this->pricingPlansHelper->getPricingPlans();

        return view('site.persons.person', compact('person', 'plans'));
    }

    /**
     * @param $personAlias
     * @param $themeAlias
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function theme($personAlias, $themeAlias)
    {
        try {
            /** @var \App\Models\Person $person */
            $person = Person::with('themes')->where('alias', $personAlias)->firstOrFail();
        } catch (Throwable $exception) {
            abort(404);
        }

        try {
            /** @var \App\Models\PersonTheme $theme */
            $theme = $person->themes()->where('alias', $themeAlias)->firstOrFail();
        } catch (Throwable $exception) {
            abort(404);
        }

        $person = $this->personHelper->prepareItemData($person);
        $theme = $this->personHelper->prepareThemeData($theme);

        return view('site.persons.theme', compact('person', 'theme'));
    }
}
