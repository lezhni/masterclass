<?php

namespace App\Http\Controllers\Site\Auth;

use App\Http\Requests\Site\Auth\PasswordRestorationRequest;
use App\Http\Requests\Site\Auth\VerificationRequest;
use App\Models\User;
use App\Models\VerificationCode;
use App\Services\GeneratorService;
use App\Services\SmsService;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Propaganistas\LaravelPhone\PhoneNumber;
use Throwable;

/**
 * Class RestorePasswordController
 * @package App\Http\Controllers\Site\Auth
 */
class RestorePasswordController
{
    /**
     * @var \App\Services\GeneratorService
     */
    protected $generatorService;

    /**
     * @var \App\Services\SmsService
     */
    protected $smsService;

    /**
     * RestorePasswordController constructor.
     * @param \App\Services\GeneratorService $generatorService
     * @param \App\Services\SmsService $smsService
     */
    public function __construct(GeneratorService $generatorService, SmsService $smsService)
    {
        $this->generatorService = $generatorService;
        $this->smsService = $smsService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function passwordRestoration()
    {
        return view('site.auth.password-restoration');
    }

    /**
     * @param \App\Http\Requests\Site\Auth\PasswordRestorationRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
//    public function restorePassword(PasswordRestorationRequest $request)
//    {
//        $phone = $request->post('phone');
//        $phone = PhoneNumber::make($phone, 'AUTO')->formatE164();
//
//        if (! User::where('phone', $phone)->exists()) {
//            return redirect()
//                ->back()
//                ->withInput()
//                ->withErrors(['phone' => 'На указанный номер не зарегистрирован аккаунт']);
//        }
//
//        // If same verification code already exists in db, generate another one
//        $code = $this->generatorService->generateVerificationCode();
//        while (VerificationCode::where('code', $code)->exists()) {
//            $code = $this->generatorService->generateVerificationCode();
//        }
//
//        VerificationCode::insert([
//            'code' => $code,
//            'phone' => $phone,
//            'expires_at' => Carbon::now()->addSeconds(VerificationCode::LIFETIME_IN_SECONDS),
//        ]);
//
//        $message = "Тестирование: код подтверждения {$code}";
//        $isSended = $this->smsService->send($phone, $message);
//        if (! $isSended) {
//            return redirect()
//                ->back()
//                ->withInput()
//                ->withErrors(['message' => 'Произошла непредвиденная ошибка, попробуйте еще раз']);
//        }
//
//        return redirect()
//            ->route('passwordVerificationPage')
//            ->withErrors([ 'message' => 'Введите код подтверждения, отправленный на указанный номер']);
//    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
//    public function passwordVerification()
//    {
//        return view('site.auth.password-restoration-verification');
//    }

    /**
     * @param \App\Http\Requests\Site\Auth\VerificationRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
//    public function verifyPassword(VerificationRequest $request)
//    {
//        $code = $request->post('code');
//
//        $existingCode = VerificationCode::where('code', $code)->first();
//        if ($existingCode->expires_at->isPast()) {
//            try {
//                $existingCode->delete();
//            } catch (Throwable $exception) {
//                report($exception);
//                return redirect()
//                    ->back()
//                    ->withInput()
//                    ->withErrors(['message' => 'Произошла непредвиденная ошибка, попробуйте еще раз']);
//            }
//
//            return redirect()
//                ->route('passwordRestorationPage')
//                ->withErrors(['code' => 'Срок жизни кода подтверждения истек']);
//        }
//
//        $phone = $existingCode->phone;
//        $user = User::where('phone', $phone)->first();
//        if (! $user instanceof User) {
//            return redirect()
//                ->back()
//                ->withInput()
//                ->withErrors(['phone' => 'Пользователь с указанным номером телефона не существует']);
//        }
//
//        $password = $this->generatorService->generateRandomCode();
//
//        try {
//            $user->password = Hash::make($password);
//            $user->saveOrFail();
//        } catch (Throwable $exception) {
//            report($exception);
//            return redirect()
//                ->back()
//                ->withErrors(['message' => 'Произошла непредвиденная ошибка, попробуйте еще раз']);
//        }
//
//        $message = "Тестирование: пароль для входа {$password}";
//        $isSended = $this->smsService->send($phone, $message);
//        if (! $isSended) {
//            return redirect()
//                ->back()
//                ->withInput()
//                ->withErrors(['message' => 'Произошла непредвиденная ошибка, попробуйте еще раз']);
//        }
//
//        try {
//            $existingCode->delete();
//        } catch (Throwable $exception) {
//            report($exception);
//        }
//
//        return response()->json([
//            'message' => "Новый пароль выслан на ваш номер телефона",
//        ]);
//    }
}