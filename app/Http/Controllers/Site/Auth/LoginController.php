<?php

namespace App\Http\Controllers\Site\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Site\Auth\LoginRequest;
use Illuminate\Support\Facades\Auth;
use Propaganistas\LaravelPhone\PhoneNumber;

class LoginController extends Controller
{
    public function login(LoginRequest $request)
    {
        $phone = $request->post('phone');
        $phone = PhoneNumber::make($phone, 'AUTO')->formatE164();
        $password = $request->post('password');

        $credentials = compact('phone', 'password');
        if (Auth::attempt($credentials)) {
            return response()->json([
                'message' => 'Вы успешно авторизованы на сайте',
                'redirect' => route('userCabinet'),
            ]);
        }

        return response()->json([
            'message' => 'Невалидные данные для авторизации',
            'errors' => [
                'login' => [
                    'Телефон или пароль указаны неверно',
                ],
            ],
        ], 422);
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('home');
    }
}
