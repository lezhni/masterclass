<?php

namespace App\Http\Controllers\Site\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Site\Auth\RegistrationRequest;
use App\Http\Requests\Site\Auth\VerificationRequest;
use App\Models\User;
use App\Models\VerificationCode;
use App\Services\GeneratorService;
use App\Services\SmsService;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Propaganistas\LaravelPhone\PhoneNumber;
use Throwable;

/**
 * Class RegistrationController
 * @package App\Http\Controllers\Site\Auth
 */
class RegistrationController extends Controller
{
    /**
     * @var \App\Services\GeneratorService
     */
    protected $generatorService;

    /**
     * @var \App\Services\SmsService
     */
    protected $smsService;

    /**
     * RegistrationController constructor.
     * @param \App\Services\GeneratorService $generatorService
     * @param \App\Services\SmsService $smsService
     */
    public function __construct(GeneratorService $generatorService, SmsService $smsService)
    {
        $this->generatorService = $generatorService;
        $this->smsService = $smsService;
    }

    /**
     * @param \App\Http\Requests\Site\Auth\RegistrationRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function register(RegistrationRequest $request)
    {
        $phone = $request->post('phone');
        $phone = PhoneNumber::make($phone, 'AUTO')->formatE164();
        $name = $request->post('name');

        if (User::where('phone', $phone)->exists()) {
            return response()->json([
                'message' => 'Номер телефона уже используется на сайте',
                'errors' => [
                    'phone' => [
                        'Номер телефона уже используется на сайте',
                    ],
                ],
            ], 422);
        }

        // If code for selected phone exists in db...
        $code = VerificationCode::where('phone', $phone)->first();
        if ($code instanceof VerificationCode) {

            // ...and it's still not expired, redirect to verification page
            if ($code->expires_at->isFuture()) {
                return response()->json([
                    'message' => 'Введите код подтверждения, отправленный на ваш номер',
                    'code' => $code->code,
                ]);
            }

            // Otherwise delete expired code
            try {
                $code->delete();
            } catch (Throwable $exception) {
                report($exception);
                return response()->json([
                    'message' => 'Произошла непредвиденная ошибка, попробуйте еще раз',
                    'errors' => [
                        'code' => [
                            'Произошла непредвиденная ошибка, попробуйте еще раз',
                        ],
                    ],
                ], 500);
            }
        }

        // If same verification code already exists in db, generate another one
        $code = $this->generatorService->generateVerificationCode();
        while (VerificationCode::where('code', $code)->exists()) {
            $code = $this->generatorService->generateVerificationCode();
        }

        VerificationCode::insert([
            'code' => $code,
            'phone' => $phone,
            'user_name' => $name,
            'expires_at' => Carbon::now()->addSeconds(VerificationCode::LIFETIME_IN_SECONDS),
        ]);

        $message = "Код подтверждения: {$code}";
        $isSended = $this->smsService->send($phone, $message);
        if (! $isSended) {
            return response()->json([
                'message' => 'Произошла непредвиденная ошибка, попробуйте еще раз',
                'errors' => [
                    'sms' => [
                        'Произошла непредвиденная ошибка, попробуйте еще раз',
                    ],
                ],
            ], 500);
        }

        return response()->json([
            'message' => 'Введите код подтверждения, отправленный на ваш номер',
        ]);
    }

    /**
     * @param \App\Http\Requests\Site\Auth\VerificationRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function verify(VerificationRequest $request)
    {
        $code = $request->post('code');

        $existingCode = VerificationCode::where('code', $code)->first();
        if ($existingCode->expires_at->isPast()) {
            try {
                $existingCode->delete();
            } catch (Throwable $exception) {
                report($exception);
                return response()->json([
                    'message' => 'Произошла непредвиденная ошибка, попробуйте еще раз',
                    'errors' => [
                        'code' => [
                            'Произошла непредвиденная ошибка, попробуйте еще раз',
                        ],
                    ],
                ], 500);
            }

            return response()->json([
                'message' => 'Код подтверждения истек',
                'errors' => [
                    'code' => [
                        'Срок жизни кода подтверждения истек',
                    ],
                ],
            ], 422);
        }

        $phone = $existingCode->phone;
        if (User::where('phone', $phone)->exists()) {
            return response()->json([
                'message' => 'Код подтверждения истек',
                'errors' => [
                    'code' => [
                        'Срок жизни кода подтверждения истек',
                    ],
                ],
            ], 422);
        }

        $name = $existingCode->user_name;
        $password = $this->generatorService->generateRandomCode();

        try {
            $user = new User();
            $user->name = $name;
            $user->phone = $phone;
            $user->password = Hash::make($password);
            $user->created_at = Carbon::now();
            $user->updated_at = Carbon::now();
            $user->saveOrFail();
        } catch (Throwable $exception) {
            report($exception);
            return response()->json([
                'message' => 'Произошла непредвиденная ошибка, попробуйте еще раз',
                'errors' => [
                    'code' => [
                        'Произошла непредвиденная ошибка, попробуйте еще раз',
                    ],
                ],
            ], 500);
        }

        $message = "Пароль от вашего аккаунта на masterclass.ru: {$password}";
        $isSended = $this->smsService->send($phone, $message);
        if (! $isSended) {
            $user->delete();
            return response()->json([
                'message' => 'Произошла непредвиденная ошибка, попробуйте еще раз',
                'errors' => [
                    'sms' => [
                        'Произошла непредвиденная ошибка, попробуйте еще раз',
                    ],
                ],
            ], 500);
        }

        try {
            $existingCode->delete();
        } catch (Throwable $exception) {
            report($exception);
        }

        return response()->json([
            'message' => "Вы успешно зарегистрированы на сайте! Пароль для входа выслан на указанный номер телефона",
        ]);
    }
}
