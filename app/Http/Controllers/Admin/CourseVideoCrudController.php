<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CourseVideo\StoreCrudRequest;
use App\Http\Requests\Admin\CourseVideo\UpdateCrudRequest;
use App\Models\Course;
use App\Models\CoursePart;
use App\Models\CourseVideo;
use App\Models\Person;
use Backpack\CRUD\app\Http\Controllers\CrudController;

/**
 * Class CoursePartCrudController
 * @package App\Http\Controllers\Admin
 */
class CourseVideoCrudController extends CrudController
{
    /**
     * @throws \Exception
     */
    public function setup()
    {
        $this->crud->setModel(CourseVideo::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/course-video');
        $this->crud->setEntityNameStrings('видео курсов', 'видео курсов');

        $this->crud->addColumns([
            [
                'name' => 'title',
                'type' => 'text',
                'label' => 'Название',
            ],
            [
                'name' => 'course_id',
                'type' => 'select',
                'entity' => 'course',
                'attribute' => 'title',
                'model' => Course::class,
                'label' => 'Курс',
            ],
            [
                'name' => 'course_part_id',
                'type' => 'select',
                'entity' => 'part',
                'attribute' => 'title',
                'model' => CoursePart::class,
                'label' => 'Часть курса',
            ],
            [
                'name' => 'person_id',
                'type' => 'select',
                'entity' => 'person',
                'attribute' => 'title',
                'model' => Person::class,
                'label' => 'Личность',
            ],
            [
                'name' => 'alias',
                'type' => 'text',
                'label' => 'Алиас',
            ],
        ]);

        $this->crud->addFields([
            [
                'name' => 'title',
                'type' => 'text',
                'label' => 'Название',
            ],
            [
                'name' => 'description',
                'type' => 'textarea',
                'label' => 'Описание',
                'attributes' => [
                    'rows' => 4,
                ],
            ],
            [
                'name' => 'alias',
                'type' => 'text',
                'label' => 'Алиас',
                'hint' => 'Автоматически сгенерируется из названия, если оставить пустым',
            ],
            [
                'name' => 'video_code',
                'type' => 'textarea',
                'attributes' => [
                    'rows' => 4,
                ],
                'label' => 'Добавьте iframe для видео',
            ],
            [
                'name' => 'big_image',
                'type' => 'image',
                'label' => 'Превью-изображение',
                'hint' => 'Изображение размером 1200x600 пикселей. <br>Если изображение не загружено, выводится изображение части/курса или плейсхолдер из настроек',
                'upload' => true,
                'disk' => 'images',
                'crop' => true,
                'aspect_ratio' => 2,
            ],
            [
                'name' => 'course_part_id',
                'type' => 'select2',
                'entity' => 'part',
                'attribute' => 'title',
                'model' => CoursePart::class,
                'label' => 'Укажите часть курса',
            ],
            [
                'name' => 'person_id',
                'type' => 'select2',
                'entity' => 'person',
                'attribute' => 'title',
                'model' => Person::class,
                'label' => 'Укажите личность',
            ],
        ]);

        $this->crud->setRequiredFields(StoreCrudRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateCrudRequest::class, 'edit');
    }

    /**
     * @param \App\Http\Requests\Admin\CourseVideo\StoreCrudRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreCrudRequest $request)
    {
        return parent::storeCrud($request);
    }

    /**
     * @param \App\Http\Requests\Admin\CourseVideo\UpdateCrudRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateCrudRequest $request)
    {
        return parent::updateCrud($request);
    }
}
