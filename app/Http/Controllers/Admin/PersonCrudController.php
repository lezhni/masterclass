<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Person\StoreCrudRequest;
use App\Http\Requests\Admin\Person\UpdateCrudRequest;
use App\Models\Person;
use Backpack\CRUD\app\Http\Controllers\CrudController;

/**
 * Class PersonCrudController
 * @package App\Http\Controllers\Admin
 */
class PersonCrudController extends CrudController
{
    /**
     * @throws \Exception
     */
    public function setup()
    {
        $this->crud->setModel(Person::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/person');
        $this->crud->setEntityNameStrings('личность', 'личности');

        $this->crud->addColumns([
            [
                'name' => 'title',
                'type' => 'text',
                'label' => 'Имя личности',
            ],
            [
                'name' => 'price',
                'type' => 'text',
                'label' => 'Стоимость (RUB)',
            ],
            [
                'name' => 'alias',
                'type' => 'text',
                'label' => 'Алиас',
            ],
        ]);

        $this->crud->addFields([
            [
                'name' => 'show_as_main',
                'type' => 'checkbox',
                'label' => 'Главная на странице личностей',
                'fake' => true,
                'tab' => 'Основная информация',
            ],
            [
                'name' => 'title',
                'type' => 'text',
                'label' => 'Имя личности',
                'tab' => 'Основная информация',
            ],
            [
                'name' => 'alias',
                'type' => 'text',
                'label' => 'Алиас',
                'hint' => 'Автоматически сгенерируется из имени личности, если оставить пустым',
                'tab' => 'Основная информация',
            ],
            [
                'name' => 'price',
                'type' => 'number',
                'value' => 777,
                'label' => 'Стоимость (RUB)',
                'suffix' => '₽',
                'tab' => 'Основная информация',
            ],
            [
                'name' => 'trailer_code',
                'type' => 'textarea',
                'attributes' => [
                    'rows' => 4,
                ],
                'label' => 'Добавьте iframe для видео трейлера',
                'tab' => 'Основная информация',
            ],
            [
                'name' => 'image',
                'type' => 'image',
                'label' => 'Изображение для списков',
                'hint' => 'Изображение размером 590x365 пикселей. <br>Если изображение не загружено, выводится плейсхолдер из настроек',
                'upload' => true,
                'disk' => 'images',
                'crop' => true,
                'aspect_ratio' => 1.61643835616,
                'tab' => 'Основная информация',
            ],
            [
                'name' => 'big_image',
                'type' => 'image',
                'label' => 'Превью-изображение',
                'hint' => 'Изображение размером 1200x600 пикселей. <br>Если изображение не загружено, выводится плейсхолдер из настроек',
                'upload' => true,
                'disk' => 'images',
                'crop' => true,
                'aspect_ratio' => 2,
                'tab' => 'Основная информация',
            ],
            [
                'name' => 'slider_image',
                'type' => 'image',
                'label' => 'Изображение для слайдера',
                'hint' => 'Изображение размером 1370x675 пикселей. <br>Если изображение не загружено, выводится плейсхолдер из настроек',
                'upload' => true,
                'disk' => 'images',
                'crop' => true,
                'aspect_ratio' => 2.02962962963,
                'tab' => 'Основная информация',
            ],
            [
                'name' => 'adv_1',
                'type' => 'custom_html',
                'value' => '<h4>Первое преимущество</h4>',
                'fake' => true,
                'tab' => 'Продающие элементы',
            ],
            [
                'name' => 'advantage-1-title',
                'type' => 'text',
                'label' => 'Заголовок',
                'fake' => true,
                'tab' => 'Продающие элементы',
            ],
            [
                'name' => 'advantage-1-text',
                'type' => 'textarea',
                'label' => 'Описание',
                'attributes' => [
                    'rows' => 2,
                ],
                'fake' => true,
                'tab' => 'Продающие элементы',
            ],
            [
                'name' => 'adv_2',
                'type' => 'custom_html',
                'value' => '<h4>Второе преимущество</h4>',
                'fake' => true,
                'tab' => 'Продающие элементы',
            ],
            [
                'name' => 'advantage-2-title',
                'type' => 'text',
                'label' => 'Заголовок',
                'fake' => true,
                'tab' => 'Продающие элементы',
            ],
            [
                'name' => 'advantage-2-text',
                'type' => 'textarea',
                'label' => 'Описание',
                'attributes' => [
                    'rows' => 2,
                ],
                'fake' => true,
                'tab' => 'Продающие элементы',
            ],
            [
                'name' => 'offer',
                'type' => 'custom_html',
                'value' => '<h4>Оффер</h4>',
                'fake' => true,
                'tab' => 'Продающие элементы',
            ],
            [
                'name' => 'offer-title',
                'type' => 'text',
                'label' => 'Заголовок',
                'fake' => true,
                'tab' => 'Продающие элементы',
            ],
            [
                'name' => 'offer-text',
                'type' => 'textarea',
                'label' => 'Текст',
                'attributes' => [
                    'rows' => 4,
                ],
                'fake' => true,
                'tab' => 'Продающие элементы',
            ],
            [
                'name' => 'offer_image',
                'type' => 'image',
                'label' => 'Изображение',
                'hint' => 'Изображение размером 480x415 пикселей',
                'upload' => true,
                'disk' => 'images',
                'crop' => true,
                'aspect_ratio' => 1.15662650602,
                'tab' => 'Продающие элементы',
            ],
            [
                'name' => 'val_1',
                'type' => 'custom_html',
                'value' => '<h4>Первая ценность</h4>',
                'fake' => true,
                'tab' => 'Продающие элементы',
            ],
            [
                'name' => 'value-1-title',
                'type' => 'text',
                'label' => 'Заголовок',
                'fake' => true,
                'tab' => 'Продающие элементы',
            ],
            [
                'name' => 'value-1-text',
                'type' => 'textarea',
                'label' => 'Описание',
                'attributes' => [
                    'rows' => 2,
                ],
                'fake' => true,
                'tab' => 'Продающие элементы',
            ],
            [
                'name' => 'val_2',
                'type' => 'custom_html',
                'value' => '<h4>Вторая ценность</h4>',
                'fake' => true,
                'tab' => 'Продающие элементы',
            ],
            [
                'name' => 'value-2-title',
                'type' => 'text',
                'label' => 'Заголовок',
                'fake' => true,
                'tab' => 'Продающие элементы',
            ],
            [
                'name' => 'value-2-text',
                'type' => 'textarea',
                'label' => 'Описание',
                'attributes' => [
                    'rows' => 2,
                ],
                'fake' => true,
                'tab' => 'Продающие элементы',
            ],
            [
                'name' => 'val_3',
                'type' => 'custom_html',
                'value' => '<h4>Третья ценность</h4>',
                'fake' => true,
                'tab' => 'Продающие элементы',
            ],
            [
                'name' => 'value-3-title',
                'type' => 'text',
                'label' => 'Заголовок',
                'fake' => true,
                'tab' => 'Продающие элементы',
            ],
            [
                'name' => 'value-3-text',
                'type' => 'textarea',
                'label' => 'Описание',
                'attributes' => [
                    'rows' => 2,
                ],
                'fake' => true,
                'tab' => 'Продающие элементы',
            ],
            [
                'name' => 'guaranty',
                'type' => 'custom_html',
                'value' => '<h4>Гарантия</h4>',
                'fake' => true,
                'tab' => 'Продающие элементы',
            ],
            [
                'name' => 'guaranty-title',
                'type' => 'text',
                'label' => 'Заголовок',
                'fake' => true,
                'tab' => 'Продающие элементы',
            ],
            [
                'name' => 'guaranty-text',
                'type' => 'textarea',
                'label' => 'Описание',
                'attributes' => [
                    'rows' => 2,
                ],
                'fake' => true,
                'tab' => 'Продающие элементы',
            ],
        ]);

        $this->crud->setRequiredFields(StoreCrudRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateCrudRequest::class, 'edit');

        /*$this->crud->enableBulkActions();
        $this->crud->addBulkDeleteButton();*/

        /*$this->crud->addFilter(
            [
                'type' => 'range',
                'name' => 'price',
                'label' => 'Фильтр по стоимости',
                'label_from' => 'От',
                'label_to' => 'До',
            ],
            false,
            function($value) {
                $range = json_decode($value);
                if ($range->from) { $this->crud->addClause('where', 'price', '>=', (int) $range->from); }
                if ($range->to) { $this->crud->addClause('where', 'price', '<=', (int) $range->to); }
            }
        );*/
    }

    /**
     * @param \App\Http\Requests\Admin\Person\StoreCrudRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreCrudRequest $request)
    {
        return parent::storeCrud($request);
    }

    /**
     * @param \App\Http\Requests\Admin\Person\UpdateCrudRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateCrudRequest $request)
    {
        return parent::updateCrud($request);
    }
}
