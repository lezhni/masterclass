<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Subscription\StoreCrudRequest;
use App\Http\Requests\Admin\Subscription\UpdateCrudRequest;
use App\Models\Subscription;
use App\Models\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;

/**
 * Class SubscriptionCrudController
 * @package App\Http\Controllers\Admin
 */
class SubscriptionCrudController extends CrudController
{
    /**
     * @throws \Exception
     */
    public function setup()
    {
        $this->crud->setModel(Subscription::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/subscription');
        $this->crud->setEntityNameStrings('подписку', 'подписки на премиум-доступ');

        $this->crud->addColumns([
            [
                'name' => 'user_id',
                'type' => 'select',
                'entity' => 'user',
                'attribute' => 'name',
                'model' => User::class,
                'label' => 'Пользователь',
            ],
            [
                'name' => 'expires_at',
                'type' => 'datetime',
                'label' => 'Дата окончания подписки',
            ]
        ]);

        $this->crud->addFields([
            [
                'name' => 'user_id',
                'type' => 'select2',
                'entity' => 'user',
                'attribute' => 'name',
                'model' => User::class,
                'label' => 'Выберите пользователя',
                'options' => (function ($query) {
                    /** @var \Illuminate\Database\Query\Builder $query */
                    return $query->where('is_admin', false)->get();
                }),
            ],
            [
                'name' => 'expires_at',
                'type' => 'datetime_picker',
                'allows_null' => true,
                'label' => 'Дата окончания подписки',
                'hint' => 'Чтобы сделать бессрочную подписку, оставьте это поле пустым',
            ],
        ]);

        $this->crud->setRequiredFields(StoreCrudRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateCrudRequest::class, 'edit');
    }

    /**
     * @param \App\Http\Requests\Admin\Subscription\StoreCrudRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreCrudRequest $request)
    {
        return parent::storeCrud($request);
    }

    /**
     * @param \App\Http\Requests\Admin\Subscription\UpdateCrudRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateCrudRequest $request)
    {
        return parent::updateCrud($request);
    }
}
