<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CoursePart\StoreCrudRequest;
use App\Http\Requests\Admin\CoursePart\UpdateCrudRequest;
use App\Models\Course;
use App\Models\CoursePart;
use Backpack\CRUD\app\Http\Controllers\CrudController;

/**
 * Class CoursePartCrudController
 * @package App\Http\Controllers\Admin
 */
class CoursePartCrudController extends CrudController
{
    /**
     * @throws \Exception
     */
    public function setup()
    {
        $this->crud->setModel(CoursePart::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/course-part');
        $this->crud->setEntityNameStrings('часть курса', 'части курса');

        $this->crud->addColumns([
            [
                'name' => 'title',
                'type' => 'text',
                'label' => 'Название',
            ],
            [
                'name' => 'course_id',
                'type' => 'select',
                'entity' => 'course',
                'attribute' => 'title',
                'model' => Course::class,
                'label' => 'Курс',
            ],
            [
                'name' => 'alias',
                'type' => 'text',
                'label' => 'Алиас',
            ],
        ]);

        $this->crud->addFields([
            [
                'name' => 'title',
                'type' => 'text',
                'label' => 'Название',
            ],
            [
                'name' => 'description',
                'type' => 'textarea',
                'label' => 'Описание',
                'attributes' => [
                    'rows' => 4,
                ],
            ],
            [
                'name' => 'alias',
                'type' => 'text',
                'label' => 'Алиас',
                'hint' => 'Автоматически сгенерируется из названия, если оставить пустым',
            ],
            [
                'name' => 'trailer_code',
                'type' => 'textarea',
                'attributes' => [
                    'rows' => 4,
                ],
                'label' => 'Добавьте iframe для видео трейлера',
            ],
            [
                'name' => 'big_image',
                'type' => 'image',
                'label' => 'Превью-изображение',
                'hint' => 'Изображение размером 1200x600 пикселей. <br>Если изображение не загружено, выводится изображение курса или плейсхолдер из настроек',
                'upload' => true,
                'disk' => 'images',
                'crop' => true,
                'aspect_ratio' => 2,
            ],
            [
                'name' => 'course_id',
                'type' => 'select2',
                'entity' => 'course',
                'attribute' => 'title',
                'model' => Course::class,
                'label' => 'Укажите курс',
            ],
        ]);

        $this->crud->setRequiredFields(StoreCrudRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateCrudRequest::class, 'edit');
    }

    /**
     * @param \App\Http\Requests\Admin\CoursePart\StoreCrudRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreCrudRequest $request)
    {
        return parent::storeCrud($request);
    }

    /**
     * @param \App\Http\Requests\Admin\CoursePart\UpdateCrudRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateCrudRequest $request)
    {
        return parent::updateCrud($request);
    }
}
