<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\User\StoreCrudRequest;
use App\Http\Requests\Admin\User\UpdateCrudRequest;
use App\Models\Course;
use App\Models\Person;
use App\Models\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Illuminate\Support\Facades\Hash;
use Propaganistas\LaravelPhone\PhoneNumber;

/**
 * Class UserCrudController
 * @package App\Http\Controllers\Admin
 */
class UserCrudController extends CrudController
{
    /**
     * @throws \Exception
     */
    public function setup()
    {
        $this->crud->setModel(User::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/user');
        $this->crud->setEntityNameStrings('пользователя', 'пользователи');

        $this->crud->setRequiredFields(StoreCrudRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateCrudRequest::class, 'edit');

        $this->crud->addColumns([
            [
                'name' => 'name',
                'type' => 'text',
                'label' => 'Имя',
            ],
            [
                'name' => 'phone',
                'type' => 'text',
                'label' => 'Номер телефона',
            ],
            [
                'name' => 'persons',
                'type' => 'select_multiple',
                'entity' => 'persons',
                'attribute' => 'title',
                'model' => Person::class,
                'label' => 'Доступ к личностям',
            ],
            [
                'name' => 'courses',
                'type' => 'select_multiple',
                'entity' => 'courses',
                'attribute' => 'title',
                'model' => Course::class,
                'label' => 'Доступ к курсам',
            ],
        ]);

        $this->crud->addFields([
            [
                'name' => 'name',
                'type' => 'text',
                'label' => 'Имя',
                'tab' => 'Основная информация',
            ],
            [
                'name' => 'phone',
                'type' => 'text',
                'label' => 'Номер телефона в международном формате',
                'tab' => 'Основная информация',
            ],
            [
                'name' => 'password',
                'type' => 'password',
                'label' => 'Пароль',
                'hint' => 'Убедитесь, что выбранный пароль удовлетворяет всем требованиям безопасности',
                'tab' => 'Основная информация',
            ],
            [
                'name' => 'password_repeat',
                'type' => 'password',
                'label' => 'Повторите пароль',
                'tab' => 'Основная информация',
            ],
            [
                'name' => 'persons',
                'type' => 'select2_multiple',
                'entity' => 'persons',
                'attribute' => 'title',
                'model' => Person::class,
                'label' => 'Доступ к выбранным личностям',
                'tab' => 'Доступ к контенту',
                'pivot' => true,
            ],
            [
                'name' => 'courses',
                'type' => 'select2_multiple',
                'entity' => 'courses',
                'attribute' => 'title',
                'model' => Course::class,
                'label' => 'Доступ к выбранным курсам',
                'tab' => 'Доступ к контенту',
                'pivot' => true,
            ],
        ]);

        $this->crud->addClause('where', 'is_admin', '=', false);
    }

    /**
     * @param \App\Http\Requests\Admin\User\StoreCrudRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreCrudRequest $request)
    {
        $phone = $request->get('phone');
        $formattedPhone = PhoneNumber::make($phone, 'AUTO')->formatE164();

        $password = $request->get('password');
        $passwordHash = Hash::make($password);

        $request->offsetSet('password', $passwordHash);
        $request->offsetSet('phone', $formattedPhone);
        return parent::storeCrud($request);
    }

    /**
     * @param \App\Http\Requests\Admin\User\UpdateCrudRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function update(UpdateCrudRequest $request)
    {
        $phone = $request->get('phone');
        $formattedPhone = PhoneNumber::make($phone, 'AUTO')->formatE164();

        $password = $request->get('password');
        if ($password !== null) {
            $passwordHash = Hash::make($password);
        } else {
            $id = $request->get('id');
            $passwordHash = User::findOrFail($id)->password;
        }

        $request->offsetSet('password', $passwordHash);
        $request->offsetSet('phone', $formattedPhone);
        return parent::updateCrud($request);
    }
}
