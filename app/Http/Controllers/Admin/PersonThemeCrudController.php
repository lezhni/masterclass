<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\PersonTheme\StoreCrudRequest;
use App\Http\Requests\Admin\PersonTheme\UpdateCrudRequest;
use App\Models\Person;
use App\Models\PersonTheme;
use Backpack\CRUD\app\Http\Controllers\CrudController;

class PersonThemeCrudController extends CrudController
{
    /**
     * @throws \Exception
     */
    public function setup()
    {
        $this->crud->setModel(PersonTheme::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/person-theme');
        $this->crud->setEntityNameStrings('тему личности', 'темы личностей');

        $this->crud->addColumns([
            [
                'name' => 'show_in_slider',
                'type' => 'check',
                'label' => 'Отображать в слайдере',
            ],
            [
                'name' => 'title',
                'type' => 'text',
                'label' => 'Название',
            ],
            [
                'name' => 'person_id',
                'type' => 'select',
                'entity' => 'person',
                'attribute' => 'title',
                'model' => Person::class,
                'label' => 'Личность',
            ],
            [
                'name' => 'alias',
                'type' => 'text',
                'label' => 'Алиас',
            ],
        ]);

        $this->crud->addFields([
            [
                'name' => 'show_in_slider',
                'type' => 'checkbox',
                'label' => 'Отображать в слайдере',
                'fake' => true,
            ],
            [
                'name' => 'title',
                'type' => 'text',
                'label' => 'Название',
            ],
            [
                'name' => 'description',
                'type' => 'textarea',
                'label' => 'Описание',
                'attributes' => [
                    'rows' => 4,
                ],
            ],
            [
                'name' => 'alias',
                'type' => 'text',
                'label' => 'Алиас',
                'hint' => 'Автоматически сгенерируется из названия, если оставить пустым',
            ],
            [
                'name' => 'video_code',
                'type' => 'textarea',
                'attributes' => [
                    'rows' => 4,
                ],
                'label' => 'Добавьте iframe для видео',
            ],
            [
                'name' => 'person_id',
                'type' => 'select2',
                'entity' => 'person',
                'attribute' => 'title',
                'model' => Person::class,
                'label' => 'Укажите личность темы',
            ],
        ]);

        $this->crud->setRequiredFields(StoreCrudRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateCrudRequest::class, 'edit');
    }

    /**
     * @param \App\Http\Requests\Admin\PersonTheme\StoreCrudRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreCrudRequest $request)
    {
        return parent::storeCrud($request);
    }

    /**
     * @param \App\Http\Requests\Admin\PersonTheme\UpdateCrudRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateCrudRequest $request)
    {
        return parent::updateCrud($request);
    }
}
