<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\GiftCrudRequest;
use App\Models\Gift;
use App\Models\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;

class GiftCrudController extends CrudController
{
    /**
     * @throws \Exception
     */
    public function setup()
    {
        $this->crud->setModel(Gift::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/gift');
        $this->crud->setEntityNameStrings('подарок', 'подарки');

        $this->crud->addColumns([
            [
                'name' => 'title',
                'type' => 'text',
                'label' => 'Название',
            ],
            [
                'name' => 'user_id',
                'type' => 'select',
                'entity' => 'users',
                'attribute' => 'name',
                'model' => User::class,
                'label' => 'Пользователи',
            ],
        ]);

        $this->crud->addFields([
            [
                'name' => 'title',
                'type' => 'text',
                'label' => 'Название',
            ],
            [
                'name' => 'description',
                'type' => 'textarea',
                'label' => 'Описание',
                'attributes' => [
                    'rows' => 3,
                ],
            ],
            [
                'name' => 'image',
                'type' => 'image',
                'label' => 'Выберите изображение',
                'upload' => true,
                'crop' => true,
                'hint' => 'Минимальный размер 390x260 пикселей',
                'aspect_ratio' => 1.5,
                'disk' => 'images',

            ],
            [
                'name' => 'users',
                'type' => 'select2_multiple',
                'entity' => 'users',
                'attribute' => 'name',
                'model' => User::class,
                'pivot' => true,
                'label' => 'Выберите пользователей',
                'options'   => (function ($query) {
                    return $query->where('is_admin', false)->get();
                }),
            ],
        ]);

        $this->crud->setRequiredFields(GiftCrudRequest::class, 'create');
        $this->crud->setRequiredFields(GiftCrudRequest::class, 'edit');
    }

    /**
     * @param \App\Http\Requests\Admin\GiftCrudRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(GiftCrudRequest $request)
    {
        return parent::storeCrud($request);
    }

    /**
     * @param \App\Http\Requests\Admin\GiftCrudRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(GiftCrudRequest $request)
    {
        return parent::updateCrud($request);
    }
}
