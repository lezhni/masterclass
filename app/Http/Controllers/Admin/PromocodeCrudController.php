<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\PromocodeCrudRequest;
use App\Models\Promocode;
use App\Models\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;

class PromocodeCrudController extends CrudController
{
    /**
     * @throws \Exception
     */
    public function setup()
    {
        $this->crud->setModel(Promocode::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/promocode');
        $this->crud->setEntityNameStrings('промокод', 'промокоды');

        $this->crud->addColumns([
            [
                'name' => 'code',
                'type' => 'text',
                'label' => 'Код',
            ],
            [
                'name' => 'type',
                'type' => 'select_from_array',
                'options' => [
                    1 => 'Скидка на сумму',
                    2 => 'Скидка в процентах',
                    3 => 'Бесплатная личность/курс',
                    4 => 'Премиум-доступ',
                ],
                'label' => 'Тип',
            ],
            [
                'name' => 'discount',
                'type' => 'promocode-discount',
                'label' => 'Значение скидки',
            ],
            [
                'name' => 'user_id',
                'type' => 'select',
                'entity' => 'user',
                'attribute' => 'phone',
                'model' => User::class,
                'label' => 'Активирован пользователем',
            ],
            [
                'name' => 'activated_at',
                'type' => 'datetime',
                'label' => 'Дата активации',
            ],
        ]);

        $this->crud->addFields([
            [
                'name' => 'code',
                'type' => 'code',
                'label' => 'Код',
            ],
            [
                'name' => 'type',
                'type' => 'toggle',
                'options' => [
                    1 => 'Скидка на сумму',
                    2 => 'Скидка в процентах',
                    3 => 'Бесплатная личность/курс',
                    4 => 'Премиум-доступ',
                ],
                'default' => 1,
                'hide_when' => [
                    3 => ['discount'],
                ],
                'label' => 'Тип',
            ],
            [
                'name' => 'discount',
                'type' => 'number',
                'label' => 'Значение скидки / срок премиум-подписки',
                'hint' => 'Для промокода на скидку укажите сумму скидки в рублях (10000) или процентах (10, 15). Для промокода премиум-доступа укажите срок действия подписки в месяцах',
            ],
        ]);

        $this->crud->setRequiredFields(PromocodeCrudRequest::class, 'create');
        $this->crud->setRequiredFields(PromocodeCrudRequest::class, 'edit');

        $this->crud->denyAccess(['update']);
        $this->crud->orderBy('created_at', 'desc');
    }

    /**
     * @param \App\Http\Requests\Admin\PromocodeCrudRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PromocodeCrudRequest $request)
    {
        return parent::storeCrud($request);
    }

    /**
     * @param \App\Http\Requests\Admin\PromocodeCrudRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(PromocodeCrudRequest $request)
    {
        return parent::updateCrud($request);
    }
}
