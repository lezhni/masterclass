<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Achievement\StoreCrudRequest;
use App\Http\Requests\Admin\Achievement\UpdateCrudRequest;
use App\Models\Achievement;
use App\Models\Course;
use Backpack\CRUD\app\Http\Controllers\CrudController;

class AchievementCrudController extends CrudController
{
    /**
     * @throws \Exception
     */
    public function setup()
    {
        $this->crud->setModel(Achievement::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/achievement');
        $this->crud->setEntityNameStrings('награду', 'награды');

        $this->crud->addColumns([
            [
                'name' => 'title',
                'type' => 'text',
                'label' => 'Название',
            ],
            [
                'name' => 'course_id',
                'type' => 'select',
                'entity' => 'course',
                'attribute' => 'title',
                'model' => Course::class,
                'label' => 'Курс',
            ],
        ]);

        $this->crud->addFields([
            [
                'name' => 'title',
                'type' => 'text',
                'label' => 'Название',
            ],
            [
                'name' => 'description',
                'type' => 'textarea',
                'label' => 'Описание',
                'attributes' => [
                    'rows' => 3,
                ],
            ],
            [
                'name' => 'course_id',
                'type' => 'select2',
                'entity' => 'course',
                'attribute' => 'title',
                'model' => Course::class,
                'label' => 'Добавить награду для курса',
                'hint' => 'Если курс не выбран, награда назначается за прохождение всех курсов',
            ],
            [
                'name' => 'image',
                'type' => 'image',
                'label' => 'Выберите изображение',
                'hint' => 'Минимальный размер 390x260 пикселей',
                'upload' => true,
                'crop' => true,
                'aspect_ratio' => 1.5,
                'disk' => 'images',
            ],
        ]);

        $this->crud->setRequiredFields(StoreCrudRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateCrudRequest::class, 'edit');
    }

    /**
     * @param \App\Http\Requests\Admin\Achievement\StoreCrudRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreCrudRequest $request)
    {
        return parent::storeCrud($request);
    }

    /**
     * @param \App\Http\Requests\Admin\Achievement\UpdateCrudRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateCrudRequest $request)
    {
        return parent::updateCrud($request);
    }
}
