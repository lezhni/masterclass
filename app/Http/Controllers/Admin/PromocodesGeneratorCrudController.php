<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\PromocodesGeneratorCrudRequest;
use App\Jobs\GeneratePromocodes;
use App\Models\PromocodesGenerator;
use Backpack\CRUD\app\Http\Controllers\CrudController;

class PromocodesGeneratorCrudController extends CrudController
{
    /**
     * @throws \Exception
     */
    public function setup()
    {
        $this->crud->setModel(PromocodesGenerator::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/promocodes-generator');
        $this->crud->setEntityNameStrings('генерацию промокодов', 'генерации промокодов');

        $this->crud->addColumns([
            [
                'name' => 'type',
                'type' => 'select_from_array',
                'options' => [
                    1 => 'Скидка на сумму',
                    2 => 'Скидка в процентах',
                    3 => 'Бесплатная личность/курс',
                    4 => 'Премиум-доступ',
                ],
                'label' => 'Тип',
            ],
            [
                'name' => 'discount',
                'type' => 'promocode-discount',
                'label' => 'Значение скидки',
            ],
            [
                'name' => 'amount',
                'type' => 'text',
                'label' => 'Количество',
            ],
            [
                'name' => 'status',
                'type' => 'status',
                'label' => 'Статус',
            ],
        ]);

        $this->crud->addFields([
            [
                'name' => 'type',
                'type' => 'toggle',
                'options' => [
                    1 => 'Скидка на сумму',
                    2 => 'Скидка в процентах',
                    3 => 'Бесплатная личность/курс',
                    4 => 'Премиум-доступ',
                ],
                'default' => 1,
                'hide_when' => [
                    3 => ['discount'],
                ],
                'label' => 'Тип',
            ],
            [
                'name' => 'discount',
                'type' => 'number',
                'label' => 'Значение скидки / срок премиум-подписки',
                'hint' => 'Для промокода на скидку укажите сумму скидки в рублях (10000) или процентах (10, 15). Для промокода премиум-доступа укажите срок действия подписки в месяцах',
            ],
            [
                'name' => 'amount',
                'type' => 'number',
                'label' => 'Количество промокодов',
            ],
        ]);

        $this->crud->setRequiredFields(PromocodesGeneratorCrudRequest::class, 'create');
        $this->crud->setRequiredFields(PromocodesGeneratorCrudRequest::class, 'edit');

        $this->crud->denyAccess(['update', 'delete']);
        $this->crud->orderBy('created_at', 'desc');
    }

    /**
     * @param \App\Http\Requests\Admin\PromocodesGeneratorCrudRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PromocodesGeneratorCrudRequest $request)
    {
        $redirect = parent::storeCrud($request);

        $promocodesGenerator = $this->crud->entry;
        if ($promocodesGenerator instanceof PromocodesGenerator) {
            GeneratePromocodes::dispatch($promocodesGenerator);
        }

        return $redirect;
    }
}
