<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\ExerciseCrudRequest;
use App\Models\CourseVideo;
use App\Models\Exercise;
use Backpack\CRUD\app\Http\Controllers\CrudController;

class ExerciseCrudController extends CrudController
{
    /**
     * @throws \Exception
     */
    public function setup()
    {
        $this->crud->setModel(Exercise::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/exercise');
        $this->crud->setEntityNameStrings('задание', 'задания');

        $this->crud->addColumns([
            [
                'name' => 'course_video_id',
                'type' => 'select',
                'entity' => 'video',
                'attribute' => 'title',
                'model' => CourseVideo::class,
                'label' => 'Видео',
            ],
            [
                'name' => 'title',
                'type' => 'text',
                'label' => 'Название',
            ],
            [
                'name' => 'description',
                'type' => 'text',
                'label' => 'Текст задания',
            ]
        ]);

        $this->crud->addFields([
            [
                'name' => 'course_video_id',
                'type' => 'select2',
                'entity' => 'video',
                'attribute' => 'title',
                'model' => CourseVideo::class,
                'label' => 'Добавить задание к видео',
            ],
            [
                'name' => 'title',
                'type' => 'text',
                'label' => 'Название',
            ],
            [
                'name' => 'description',
                'type' => 'textarea',
                'label' => 'Текст задания',
                'attributes' => [
                    'rows' => 4,
                ],
            ],
        ]);

        $this->crud->setRequiredFields(ExerciseCrudRequest::class, 'create');
        $this->crud->setRequiredFields(ExerciseCrudRequest::class, 'edit');
    }

    /**
     * @param \App\Http\Requests\Admin\ExerciseCrudRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ExerciseCrudRequest $request)
    {
        return parent::storeCrud($request);
    }

    /**
     * @param \App\Http\Requests\Admin\ExerciseCrudRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ExerciseCrudRequest $request)
    {
        return parent::updateCrud($request);
    }
}
