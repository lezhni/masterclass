<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(\App\Helpers\PersonHelper::class);
        $this->app->singleton(\App\Helpers\CourseHelper::class);
        $this->app->singleton(\App\Helpers\FeedbackHelper::class);
        $this->app->singleton(\App\Helpers\HomePageHelper::class);
        $this->app->singleton(\App\Helpers\UserCabinetHelper::class);
        $this->app->singleton(\App\Helpers\PricingPlans\Helper::class);
        $this->app->singleton(\App\Helpers\Formatter::class);

        $this->app->singleton(\App\Services\PromocodesService::class);
        $this->app->singleton(\App\Services\UserAccessService::class);
        $this->app->singleton(\App\Services\GeneratorService::class);
        $this->app->singleton(\App\Services\SmsService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
