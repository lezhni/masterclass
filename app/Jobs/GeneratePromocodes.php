<?php

namespace App\Jobs;

use App\Models\Promocode;
use App\Models\PromocodesGenerator;
use App\Services\GeneratorService;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GeneratePromocodes implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var \App\Models\PromocodesGenerator
     */
    protected $promocodesGenerator;

    /**
     * Create a new job instance.
     *
     * @param \App\Models\PromocodesGenerator $promocodesGenerator
     */
    public function __construct(PromocodesGenerator $promocodesGenerator)
    {
        $this->promocodesGenerator = $promocodesGenerator;
    }

    /**
     * Execute the job.
     *
     * @param \App\Services\GeneratorService $generatorService
     * @return void
     * @throws \Throwable
     */
    public function handle(GeneratorService $generatorService)
    {
        $this->promocodesGenerator->status = PromocodesGenerator::PROCESS_STATUS;
        $this->promocodesGenerator->save();

        $now = Carbon::now();
        $promocodes = [];

        for ($i = 0; $i < $this->promocodesGenerator->amount; $i++) {
            $code = $generatorService->generateRandomCode(16);
            while (Promocode::where('code', $code)->exists()) {
                $code = $generatorService->generateRandomCode(16);
            }

            $promocodes[] = [
                'code' => $code,
                'type' => $this->promocodesGenerator->type,
                'discount' => $this->promocodesGenerator->discount,
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }

        DB::transaction(function () use ($promocodes) {
            DB::table('promocodes')->insert($promocodes);
        });

        $this->promocodesGenerator->status = PromocodesGenerator::SUCCESS_STATUS;
        $this->promocodesGenerator->save();
    }

    public function failed(Exception $exception)
    {
        $this->promocodesGenerator->status = PromocodesGenerator::FAILED_STATUS;
        $this->promocodesGenerator->save();
        report($exception);
    }
}
