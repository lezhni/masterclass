<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Support\Str;
use Image;
use Storage;

class Page extends Model
{
    use CrudTrait;
    use Sluggable;
    use SluggableScopeHelpers;

    const IMAGES_DISK = 'uploads';
    const IMAGES_DIR = 'images';

    protected $table = 'pages';

    protected $fillable = [
        'template',
        'name',
        'title',
        'slug',
        'content',
        'extras',
        'image',
        'image_1',
        'image_2',
        'image_3',
    ];

    protected $fakeColumns = [
        'extras',
    ];

    protected $casts = [
        'extras' => 'array',
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'slug_or_title',
            ],
        ];
    }

    public function getTemplateName()
    {
        return str_replace('_', ' ', title_case($this->template));
    }

    public function getPageLink()
    {
        return url($this->slug);
    }

    public function getOpenButton()
    {
        return '<a class="btn btn-default btn-xs" href="'.$this->getPageLink().'" target="_blank">'.
            '<i class="fa fa-eye"></i>Просмотреть</a>';
    }

    public function getSlugOrTitleAttribute()
    {
        if ($this->slug != '') {
            return $this->slug;
        }

        return $this->title;
    }

    public function setImageAttribute($value)
    {
        $this->processImageField($value, 'image');
    }

    public function setImage1Attribute($value)
    {
        $this->processImageField($value, 'image_1');
    }

    public function setImage2Attribute($value)
    {
        $this->processImageField($value, 'image_2');
    }

    public function setImage3Attribute($value)
    {
        $this->processImageField($value, 'image_3');
    }

    private function processImageField($value, string $attributeName)
    {
        if ($value == null) {
            Storage::disk(self::IMAGES_DISK)->delete($this->{$attributeName});
            $this->attributes[$attributeName] = null;
        }

        if (starts_with($value, 'data:image')) {
            $image = Image::make($value)->encode('png', 100);
            $filename = md5($value.time()) . '.png';
            Storage::disk(self::IMAGES_DISK)->put(self::IMAGES_DIR . '/' . $filename, $image->stream());
            $this->attributes[$attributeName] = self::IMAGES_DIR . '/' . $filename;
        }
    }
}
