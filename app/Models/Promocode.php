<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Promocode
 * @package App\Models
 */
class Promocode extends Model
{
    use CrudTrait, SoftDeletes;

    /**
     * @var int
     */
    const DISCOUNT_SUM_TYPE = 1;

    /**
     * @var int
     */
    const DISCOUNT_PERCENTS_TYPE = 2;

    /**
     * @var int
     */
    const FREE_CONTENT_TYPE = 3;

    /**
     * @var int
     */
    const PREMIUM_TYPE = 4;

    /**
     * @var string
     */
    protected $table = 'promocodes';

    /**
     * @var array
     */
    protected $fillable = [
        'code',
        'type',
        'discount',
    ];

    /**
     * @var array
     */
    public $casts = [
        'type' => 'integer',
        'user_id' => 'integer',
        'discount' => 'integer',
        'activated_at' => 'datetime',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Scope a query to only include activated promocodes.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActivated($query)
    {
        return $query->has('user');
    }

    /**
     * Scope a query to only include unactivated promocodes.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUnactivated($query)
    {
        return $query->doesntHave('user');
    }
}
