<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Image;
use Storage;

/**
 * Class Achievement
 * @package App\Models
 */
class Achievement extends Model
{
    use CrudTrait;

    /**
     * @var string
     */
    const IMAGES_DIR = 'achievements';

    /**
     * @var string
     */
    protected $table = 'achievements';

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'image',
        'course_id',
    ];

    /**
     * @var array
     */
    public $casts = [
        'course_id' => 'integer',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Course::class, 'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(User::class, 'users_achievements');
    }

    /**
     * @param $value
     */
    public function setImageAttribute($value)
    {
        if ($value === null) {
            Storage::disk('images')->delete($this->image);
            $this->attributes['image'] = null;
            return;
        }

        if (! Str::startsWith($value, 'data:image')) {
            return;
        }

        $image = Image::make($value)->encode('png', 100);
        $filename = md5($value.time()).'.png';
        $path = self::IMAGES_DIR . '/' . $filename;

        Storage::disk('images')->put($path, $image->stream());
        $this->attributes['image'] = $path;
    }

    /**
     *
     */
    public static function boot()
    {
        parent::boot();
        static::deleting(function($achievement) {
            Storage::disk('images')->delete($achievement->image);
        });
    }
}
