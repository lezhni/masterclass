<?php

namespace App\Models;

use App\Traits\HasImageFields;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Support\Str;
use Storage;
use Znck\Eloquent\Traits\BelongsToThrough;

/**
 * Class CourseVideo
 * @package App\Models
 */
class CourseVideo extends Model
{
    use CrudTrait, BelongsToThrough, HasImageFields;

    /**
     * @var string
     */
    const IMAGES_DIR = 'courses';

    /**
     * @var string
     */
    protected $table = 'coursesVideos';

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'course_part_id',
        'person_id',
        'alias',
        'video_code',
        'big_image',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'course_part_id' => 'integer',
        'person_id' => 'integer',
    ];

    /**
     * @return \Znck\Eloquent\Relations\BelongsToThrough
     */
    public function course(): \Znck\Eloquent\Relations\BelongsToThrough
    {
        return $this->belongsToThrough(Course::class, CoursePart::class, null, '', [CoursePart::class => 'course_part_id']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function person(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Person::class, 'person_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function part(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(CoursePart::class, 'course_part_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function exercises()
    {
        return $this->hasMany(Exercise::class, 'course_video_id');
    }

    /**
     *
     */
    public static function boot()
    {
        parent::boot();

        static::deleting(function($item) {
            Storage::disk('images')->delete($item->image);
            Storage::disk('images')->delete($item->big_image);
            Storage::disk('images')->delete($item->slider_image);
            Storage::disk('images')->delete($item->offer_image);
        });

        static::saving(function($item) {
            if ($item->alias === null) {
                $item->alias = Str::slug($item->title);
            }
        });
    }
}
