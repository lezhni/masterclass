<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class VerificationCode
 * @package App\Models
 */
class VerificationCode extends Model
{
    /**
     * Lifetime of the verification code, in seconds.
     *
     * @var int
     */
    const LIFETIME_IN_SECONDS = 300;

    /**
     * @var string
     */
    protected $table = 'verificationCodes';

    /**
     * @var array
     */
    protected $fillable = [
        'code',
        'user_name',
        'phone',
        'expires_at',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'expires_at' => 'datetime',
    ];

    /**
     * @var bool
     */
    public $timestamps = false;
}
