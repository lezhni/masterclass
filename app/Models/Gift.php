<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Image;
use Storage;

/**
 * Class Gift
 * @package App\Models
 */
class Gift extends Model
{
    use CrudTrait;

    /**
     * @var string
     */
    const IMAGES_DIR = 'gifts';

    /**
     * @var string
     */
    protected $table = 'gifts';

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'image',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(User::class, 'users_gifts');
    }

    /**
     * @param $value
     */
    public function setImageAttribute($value)
    {
        if ($value === null) {
            Storage::disk('images')->delete($this->image);
            $this->attributes['image'] = null;
            return;
        }

        if (! Str::startsWith($value, 'data:image')) {
            return;
        }

        $image = Image::make($value)->encode('png', 100);
        $filename = md5($value.time()).'.png';
        $path = self::IMAGES_DIR . '/' . $filename;

        Storage::disk('images')->put($path, $image->stream());
        $this->attributes['image'] = $path;
    }

    /**
     *
     */
    public static function boot()
    {
        parent::boot();
        static::deleting(function($gift) {
            Storage::disk('images')->delete($gift->image);
        });
    }
}
