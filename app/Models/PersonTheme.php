<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Support\Str;
use Storage;

/**
 * Class PersonTheme
 * @package App\Models
 */
class PersonTheme extends Model
{
    use CrudTrait;

    /**
     * @var string
     */
    protected $table = 'personsThemes';

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'person_id',
        'alias',
        'video_code',
        'extras',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'person_id' => 'integer',
    ];

    /**
     * @var array
     */
    protected $fakeColumns = [
        'extras',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function person(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Person::class);
    }

    /**
     *
     */
    public static function boot()
    {
        parent::boot();

        static::deleting(function($item) {
            Storage::disk('images')->delete($item->image);
            Storage::disk('images')->delete($item->big_image);
            Storage::disk('images')->delete($item->slider_image);
            Storage::disk('images')->delete($item->offer_image);
        });

        static::saving(function($item) {
            if ($item->alias === null) {
                $item->alias = Str::slug($item->title);
            }
        });
    }
}
