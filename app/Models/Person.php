<?php

namespace App\Models;

use App\Traits\HasImageFields;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Storage;

/**
 * Class Person
 * @package App\Models
 */
class Person extends Model
{
    use CrudTrait, HasImageFields;

    /**
     * @var string
     */
    const IMAGES_DIR = 'persons';

    /**
     * @var string
     */
    protected $table = 'persons';

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'price',
        'alias',
        'trailer_code',
        'extras',
        'big_image',
        'slider_image',
        'offer_image',
        'image',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'price' => 'integer',
        'extras' => 'array',
    ];

    /**
     * @var array
     */
    protected $fakeColumns = [
        'extras',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function themes(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(PersonTheme::class, 'person_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function videos(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(CourseVideo::class, 'person_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(User::class, 'users_persons');
    }

    /**
     *
     */
    public static function boot()
    {
        parent::boot();

        static::deleting(function($item) {
            Storage::disk('images')->delete($item->image);
            Storage::disk('images')->delete($item->big_image);
            Storage::disk('images')->delete($item->slider_image);
            Storage::disk('images')->delete($item->offer_image);
        });

        static::saving(function($item) {
            if ($item->alias === null) {
                $item->alias = Str::slug($item->title);
            }
        });
    }
}
