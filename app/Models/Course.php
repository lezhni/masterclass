<?php

namespace App\Models;

use App\Traits\HasImageFields;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Support\Str;
use Staudenmeir\EloquentHasManyDeep\HasRelationships;
use Storage;

/**
 * Class Course
 * @package App\Models
 */
class Course extends Model
{
    use CrudTrait, HasRelationships, HasImageFields;

    /**
     * @var string
     */
    const IMAGES_DIR = 'courses';

    /**
     * @var string
     */
    protected $table = 'courses';

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'price',
        'alias',
        'trailer_code',
        'extras',
        'big_image',
        'slider_image',
        'offer_image',
        'image',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'price' => 'integer',
        'extras' => 'array',
    ];

    /**
     * @var array
     */
    protected $fakeColumns = [
        'extras',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function parts(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(CoursePart::class, 'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function videos(): \Illuminate\Database\Eloquent\Relations\HasManyThrough
    {
        return $this->hasManyThrough(CourseVideo::class, CoursePart::class, 'course_id', 'course_part_id');
    }

    public function exercises()
    {
        return $this->hasManyDeep(Exercise::class, [
            CoursePart::class,
            CourseVideo::class
        ], [
            'course_id',
            'course_part_id',
            'course_video_id'
        ]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(User::class, 'users_courses');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function achievement(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Achievement::class, 'course_id');
    }

    /**
     *
     */
    public static function boot()
    {
        parent::boot();

        static::deleting(function($item) {
            Storage::disk('images')->delete($item->image);
            Storage::disk('images')->delete($item->big_image);
            Storage::disk('images')->delete($item->slider_image);
            Storage::disk('images')->delete($item->offer_image);
        });

        static::saving(function($item) {
            if ($item->alias === null) {
                $item->alias = Str::slug($item->title);
            }
        });
    }
}
