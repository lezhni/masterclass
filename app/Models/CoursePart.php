<?php

namespace App\Models;

use App\Traits\HasImageFields;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Storage;

/**
 * Class CoursePart
 * @package App\Models
 */
class CoursePart extends Model
{
    use CrudTrait, HasImageFields;

    /**
     * @var string
     */
    const IMAGES_DIR = 'courses';

    /**
     * @var string
     */
    protected $table = 'coursesParts';

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'course_id',
        'alias',
        'trailer_code',
        'big_image',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'course_id' => 'integer',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Course::class, 'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function videos(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(CourseVideo::class, 'course_part_id');
    }

    /**
     *
     */
    public static function boot()
    {
        parent::boot();

        static::deleting(function($item) {
            Storage::disk('images')->delete($item->image);
            Storage::disk('images')->delete($item->big_image);
            Storage::disk('images')->delete($item->slider_image);
            Storage::disk('images')->delete($item->offer_image);
        });

        static::saving(function($item) {
            if ($item->alias === null) {
                $item->alias = Str::slug($item->title);
            }
        });
    }
}
