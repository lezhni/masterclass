<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PromocodesGenerator
 * @package App\Models
 */
class PromocodesGenerator extends Model
{
    use CrudTrait;

    /**
     * @var int
     */
    const WAITING_STATUS = 0;

    /**
     * @var int
     */
    const PROCESS_STATUS = 1;

    /**
     * @var int
     */
    const FAILED_STATUS = 2;

    /**
     * @var int
     */
    const SUCCESS_STATUS = 3;

    /**
     * @var string
     */
    protected $table = 'promocodesGenerators';

    /**
     * @var array
     */
    protected $fillable = [
        'type',
        'discount',
        'amount',
        'status',
    ];

    /**
     * @var array
     */
    public $casts = [
        'type' => 'integer',
        'discount' => 'integer',
        'amount' => 'integer',
        'status' => 'integer',
    ];
}
