<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class Subscription
 * @package App\Models
 */
class Subscription extends Model
{
    use CrudTrait;

    /**
     * @var string
     */
    protected $table = 'subscriptions';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'expires_at',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'expires_at' => 'datetime',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Scope a query to only include active subscriptions.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query
            ->where('expires_at', '>', Carbon::now())
            ->orWhere('expires_at', null);
    }

    /**
     * Scope a query to only include expired subscriptions.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeExpired($query)
    {
        return $query
            ->where('expires_at', '<', Carbon::now());
    }
}
