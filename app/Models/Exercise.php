<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Staudenmeir\EloquentHasManyDeep\HasRelationships;

/**
 * Class Exercise
 * @package App\Models
 */
class Exercise extends Model
{
    use CrudTrait, HasRelationships;

    /**
     * @var string
     */
    protected $table = 'exercises';

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'course_video_id',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'course_video_id' => 'integer',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(User::class, 'users_exercises');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function video(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(CourseVideo::class, 'course_video_id');
    }

    /**
     * @return \Staudenmeir\EloquentHasManyDeep\HasManyDeep
     */
    public function course(): \Staudenmeir\EloquentHasManyDeep\HasManyDeep
    {
        return $this->hasManyDeep(Course::class, [
            CourseVideo::class,
            CoursePart::class
        ], [
            'course_id',
            'course_video_id',
            'course_part_id',
        ]);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $course
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereCourseIs($query, $course)
    {
        $videosIds = $course->videos->pluck('id');
        return $query->whereIn('course_video_id', $videosIds);
    }
}
