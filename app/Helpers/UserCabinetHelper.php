<?php

namespace App\Helpers;

use App\Models\Page;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Throwable;

/**
 * Class UserCabinetHelper
 * @package App\Helpers
 */
class UserCabinetHelper
{
    public function getContest(): array
    {
        try {
            $contentPage = Page::findBySlugOrFail('/gifts-achievements');
        } catch (Throwable $exception) {
            report($exception);
            return [];
        }

        $data = $contentPage->toArray();

        $contestImage = Arr::get($data, 'image');
        if ($contestImage !== null && $contestImage !== '') {
            $contestImage = Storage::disk('uploads')->url($contestImage);
        }

        $winnerImage = Arr::get($data, 'image_1');
        if ($winnerImage !== null && $winnerImage !== '') {
            $winnerImage = Storage::disk('uploads')->url($winnerImage);
        }

        return [
            'title' => Arr::get($data, 'extras.contest-title'),
            'description' => Arr::get($data, 'extras.contest-desc'),
            'full-description' => Arr::get($data, 'extras.contest-full-desc'),
            'date' => Arr::get($data, 'extras.contest-date'),
            'image' => $contestImage,
            'prize' => [
                'title' => Arr::get($data, 'extras.contest-prize'),
                'description' => Arr::get($data, 'extras.contest-prize-desc'),
            ],
            'winner' => [
                'title' => Arr::get($data, 'extras.contest-winner-title'),
                'city' => Arr::get($data, 'extras.contest-winner-city'),
                'description' => Arr::get($data, 'extras.contest-winner-desc'),
                'image' => $winnerImage,
            ],
        ];
    }

    /**
     * @param \Illuminate\Database\Eloquent\Collection $achievements
     * @return array
     */
    public function prepareAchievementsData($achievements): array
    {
        if (! $achievements instanceof Collection) {
            return [];
        }

        $items = $achievements->toArray();
        $items = array_map(function ($item) {
            $image = Arr::get($item, 'image');
            return [
                'title' => Arr::get($item, 'title'),
                'description' => Arr::get($item, 'description'),
                'image' => Storage::disk('images')->url($image),
            ];
        }, $items);

        return $items;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Collection $gifts
     * @return array
     */
    public function prepareGiftsData($gifts): array
    {
        if (! $gifts instanceof Collection) {
            return [];
        }

        $items = $gifts->toArray();
        $items = array_map(function ($item) {
            $image = Arr::get($item, 'image');
            return [
                'title' => Arr::get($item, 'title'),
                'description' => Arr::get($item, 'description'),
                'image' => Storage::disk('images')->url($image),
                'users' => Arr::get($item, 'users'),
            ];
        }, $items);

        return $items;
    }
}