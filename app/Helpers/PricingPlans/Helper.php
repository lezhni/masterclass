<?php

namespace App\Helpers\PricingPlans;

use App\Helpers\Formatter;
use App\Models\Page;
use Illuminate\Support\Arr;
use Throwable;

class Helper
{
    /**
     * @var \App\Helpers\Formatter
     */
    private $formatter;

    /**
     * HomePageHelper constructor.
     * @param \App\Helpers\Formatter $formatter
     */
    public function __construct(Formatter $formatter)
    {
        $this->formatter = $formatter;
    }

    /**
     * Get a pricing plans.
     *
     * @return array
     */
    public function getPricingPlans(): array
    {
        try {
            /** @var \App\Models\Page $pricingPageConfigs */
            $pricingPageConfigs = Page::findBySlugOrFail('/pricing');
        } catch (Throwable $exception) {
            return [];
        }

        $data = $pricingPageConfigs->toArray();
        $rawPlans = Arr::get($data, 'extras');
        if ($rawPlans === null) {
            return [];
        }

        $plans = [
            PricingPlans::ULTIMATE['key'] => [
                'name' => Arr::get($rawPlans, PricingPlans::ULTIMATE['key'] . '-name'),
                'desc' => Arr::get($rawPlans, PricingPlans::ULTIMATE['key'] . '-desc'),
                'price' => Arr::get($rawPlans, PricingPlans::ULTIMATE['key'] . '-price', 0),
                'duration' => 3,
            ],
            PricingPlans::ULTIMATE_PLUS['key'] => [
                'name' => Arr::get($rawPlans, PricingPlans::ULTIMATE_PLUS['key'] . '-name'),
                'desc' => Arr::get($rawPlans, PricingPlans::ULTIMATE_PLUS['key'] . '-desc'),
                'price' => Arr::get($rawPlans, PricingPlans::ULTIMATE_PLUS['key'] . '-price', 0),
                'duration' => 12,
            ],
            PricingPlans::COURSE_ACCESS['key'] => [
                'name' => Arr::get($rawPlans, PricingPlans::COURSE_ACCESS['key'] . '-name'),
                'desc' => Arr::get($rawPlans, PricingPlans::COURSE_ACCESS['key'] . '-desc'),
                'price' => Arr::get($rawPlans, PricingPlans::COURSE_ACCESS['key'] . '-price', 0),
            ],
            PricingPlans::PERSON_ACCESS['key'] => [
                'name' => Arr::get($rawPlans, PricingPlans::PERSON_ACCESS['key'] . '-name'),
                'desc' => Arr::get($rawPlans, PricingPlans::PERSON_ACCESS['key'] . '-desc'),
                'price' => Arr::get($rawPlans, PricingPlans::PERSON_ACCESS['key'] . '-price', 0),
            ],
        ];

        $plans = array_map(function ($plan) {
            // TODO : Process prices with promocodes?
            if (key_exists('duration', $plan)) {
                $monthlyPrice = $plan['price'] / $plan['duration'];
                $plan['monthly-price'] = $this->formatter->formatPricePerMonth($monthlyPrice);
                $plan['duration'] = $this->formatter->formatDurationInMonths($plan['duration']);
            }
            $plan['price-formatted'] = $this->formatter->formatPrice($plan['price']);
            return $plan;
        }, $plans);

        return $plans;
    }
}