<?php

namespace App\Helpers\PricingPlans;

class PricingPlans
{
    public  const ULTIMATE = [
        'key' => 'ultimate',
        'label' => 'Тариф «Универсальный»',
    ];

    public const ULTIMATE_PLUS = [
        'key' => 'ultimate-plus',
        'label' => 'Тариф «Универсальный Плюс»',
    ];

    public const COURSE_ACCESS = [
        'key' => 'course',
        'label' => 'Доступ к курсу',
    ];

    public const PERSON_ACCESS = [
        'key' => 'person',
        'label' => 'Доступ к личности',
    ];
}