<?php

namespace App\Helpers;

use App\Models\Course;
use App\Models\CoursePart;
use App\Models\CourseVideo;
use App\Models\Page;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Storage;
use Throwable;

/**
 * Class CourseHelper
 * @package App\Helpers
 */
class CourseHelper
{
    /**
     * @var \App\Helpers\PersonHelper
     */
    private $personHelper;

    /**
     * CourseHelper constructor.
     * @param \App\Helpers\PersonHelper $personHelper
     */
    public function __construct(PersonHelper $personHelper)
    {
        $this->personHelper = $personHelper;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection $courses
     * @return array
     */
    public function prepareItemsData($courses): array
    {
        if (! $courses instanceof Collection) {
            return [];
        }

        $items = $courses->toArray();

        $items = array_map(function ($item) {
            $image = Arr::get($item, 'image');
            $bigImage = Arr::get($item, 'big_image');

            return [
                'title' => Arr::get($item, 'title'),
                'alias' => Arr::get($item, 'alias'),
                'price' => Arr::get($item, 'price'),
                'trailer_code' => Arr::get($item, 'trailer_code'),
                'show_as_main' => (bool) Arr::get($item, 'extras.show_as_main'),
                'image' => $this->getImage($image),
                'big_image' => $this->getBigImage($bigImage),
                'advantages' => [
                    0 => [
                        'title' => Arr::get($item, 'extras.advantage-1-title'),
                        'text' => Arr::get($item, 'extras.advantage-1-text'),
                    ],
                    1 => [
                        'title' => Arr::get($item, 'extras.advantage-2-title'),
                        'text' => Arr::get($item, 'extras.advantage-2-text'),
                    ],
                ],
                'offer' => [
                    'title' => Arr::get($item, 'extras.offer-title'),
                    'text' => Arr::get($item, 'extras.offer-text'),
                    'image' => Storage::disk('images')->url($item['offer_image']),
                ],
            ];
        }, $items);

        return $items;
    }

    /**
     * @param \App\Models\Course|\Illuminate\Database\Eloquent\Builder $course
     * @return array
     */
    public function prepareItemData($course): array
    {
        if (! $course instanceof Course && $course instanceof Builder) {
            return [];
        }

        $parts = $course->parts;
        $videos = $course->videos;

        $item = $course->toArray();

        $image = Arr::get($item, 'image');
        $bigImage = Arr::get($item, 'big_image');

        $item = [
            'id' => Arr::get($item, 'id'),
            'title' => Arr::get($item, 'title'),
            'alias' => Arr::get($item, 'alias'),
            'price' => Arr::get($item, 'price'),
            'trailer_code' => Arr::get($item, 'trailer_code'),
            'parts' => $this->preparePartsData($parts),
            'videos' => $this->prepareVideosData($videos),
            'show_as_main' => (bool) Arr::get($item, 'extras.show_as_main'),
            'image' => $this->getImage($image),
            'big_image' => $this->getBigImage($bigImage),
            'advantages' => [
                0 => [
                    'title' => Arr::get($item, 'extras.advantage-1-title'),
                    'text' => Arr::get($item, 'extras.advantage-1-text'),
                ],
                1 => [
                    'title' => Arr::get($item, 'extras.advantage-2-title'),
                    'text' => Arr::get($item, 'extras.advantage-2-text'),
                ],
            ],
            'values' => [
                0 => [
                    'title' => Arr::get($item, 'extras.value-1-title'),
                    'text' => Arr::get($item, 'extras.value-1-text'),
                ],
                1 => [
                    'title' => Arr::get($item, 'extras.value-2-title'),
                    'text' => Arr::get($item, 'extras.value-2-text'),
                ],
                2 => [
                    'title' => Arr::get($item, 'extras.value-3-title'),
                    'text' => Arr::get($item, 'extras.value-3-text'),
                ],
            ],
            'offer' => [
                'title' => Arr::get($item, 'extras.offer-title'),
                'text' => Arr::get($item, 'extras.offer-text'),
                'image' => Storage::disk('images')->url($item['offer_image']),
            ],
            'guaranty' => [
                'title' => Arr::get($item, 'extras.guaranty-title'),
                'text' => Arr::get($item, 'extras.guaranty-text'),
            ]
        ];

        return $item;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Collection $parts
     * @return array
     */
    public function preparePartsData($parts): array
    {
        if (! $parts instanceof Collection) {
            return [];
        }

        $items = $parts->toArray();
        $items = array_map(function ($item, $index) {
            $description = Arr::get($item, 'description');
            $description = Str::limit($description);
            return [
                'title' => Arr::get($item, 'title'),
                'description' => $description,
                'number' => str_pad(++$index, 2, '0', STR_PAD_LEFT),
                'alias' => Arr::get($item, 'alias'),
            ];
        }, $items, array_keys($items));

        return $items;
    }

    /**
     * @param \App\Models\CoursePart|\Illuminate\Database\Eloquent\Builder $part
     * @return array
     */
    public function preparePartData($part): array
    {
        if (! $part instanceof CoursePart && $part instanceof Builder) {
            return [];
        }

        $videos = $part->videos;
        $item = $part->toArray();

        $bigImage = Arr::get($item, 'big_image');

        $item = [
            'title' => Arr::get($item, 'title'),
            'description' => Arr::get($item, 'description'),
            'trailer_code' => Arr::get($item, 'trailer_code'),
            'big_image' => $this->getBigImage($bigImage),
            'alias' => Arr::get($item, 'alias'),
            'videos' => $this->prepareVideosData($videos),
        ];

        return $item;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Collection $videos
     * @return array
     */
    public function prepareVideosData($videos): array
    {
        if (! $videos instanceof Collection) {
            return [];
        }

        $videos->load('person');

        $items = $videos->toArray();
        $items = array_map(function ($item, $index) {
            $image = Arr::get($item, 'person.image');

            return [
                'title' => Arr::get($item, 'person.title'),
                'number' => str_pad(++$index, 2, '0', STR_PAD_LEFT),
                'alias' => Arr::get($item, 'alias'),
                'image' => $this->personHelper->getImage($image),
            ];
        }, $items, array_keys($items));

        return $items;
    }

    /**
     * @param \App\Models\CourseVideo|\Illuminate\Database\Eloquent\Builder $video
     * @return array
     */
    public function prepareVideoData($video): array
    {
        if (!$video instanceof CourseVideo && $video instanceof Builder) {
            return [];
        }

        $item = $video->toArray();

        $bigImage = Arr::get($item, 'person.big_image');

        $item = [
            'title' => Arr::get($item, 'person.title'),
            'description' => Arr::get($item, 'description'),
            'video_code' => Arr::get($item, 'video_code'),
            'big_image' => $this->personHelper->getBigImage($bigImage),
            'alias' => Arr::get($item, 'alias'),
            'exercises' => Arr::get($item, 'exercises', []),
        ];

        return $item;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Collection $exercises
     * @return array
     */
    public function prepareExercisesData($exercises): array
    {
        if (! $exercises instanceof Collection) {
            return [];
        }

        $items = $exercises->toArray();
        return $items;
    }

    /**
     * Get highlighted course.
     *
     * @param array $courses
     * @return array
     */
    public function getHighlightedCourse(array $courses): array
    {
        foreach ($courses as $course) {
            if ($course['show_as_main'] === true) {
                return $course;
            }
        }

        $highlightedCourse = end($courses);
        return $highlightedCourse;
    }

    /**
     * Return an image path (or placeholder image path).
     *
     * @param $imagePath
     * @return mixed
     */
    public function getImage($imagePath)
    {
        if ($imagePath !== null && $imagePath !== '') {
            return Storage::disk('images')->url($imagePath);
        }

        try {
            $config = Page::findBySlugOrFail('/courses');
            $config = $config->toArray();
        } catch (Throwable $exception) {
            abort(404);
        }

        $placeholderImagePath = Arr::get($config, 'image_1');
        return Storage::disk('uploads')->url($placeholderImagePath);
    }

    /**
     * Return a preview image path (or placeholder image path).
     *
     * @param $imagePath
     * @return mixed
     */
    public function getBigImage($imagePath)
    {
        if ($imagePath !== null && $imagePath !== '') {
            return Storage::disk('images')->url($imagePath);
        }

        try {
            $config = Page::findBySlugOrFail('/courses');
            $config = $config->toArray();
        } catch (Throwable $exception) {
            abort(404);
        }

        $placeholderImagePath = Arr::get($config, 'image');
        return Storage::disk('uploads')->url($placeholderImagePath);
    }

    // TODO : Add method to get part image (part -> video -> placeholder)

    // TODO : Add method to get video image (video -> person -> placeholder)
}