<?php

namespace App\Helpers;

use App\Models\Page;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Throwable;

/**
 * Class HomePageHelper
 * @package App\Helpers\Page
 */
class HomePageHelper
{
    /**
     * @var array
     */
    private $personImagesPlaceholders;

    /**
     * HomePageHelper constructor.
     */
    public function __construct()
    {
        try {
            $personsPage = Page::findBySlugOrFail('/persons');
            $personsPage = $personsPage->toArray();
        } catch (Throwable $exception) {
            $this->personImagesPlaceholders = [
                'image' => null,
                'slider_image' => null,
            ];
        }

        $this->personImagesPlaceholders = [
            'image' => Arr::get($personsPage, 'image_1'),
            'slider_image' => Arr::get($personsPage, 'image_2'),
        ];
    }

    /**
     * @param \App\Models\Page $page
     * @return array
     */
    public function prepareHomePageData(Page $page): array
    {
        $data = $page->toArray();

        $image = Arr::get($data, 'image');
        if ($image !== null && $image !== '') {
            $image = Storage::disk(Page::IMAGES_DISK)->url($image);
        }

        $data = [
            'video' => Arr::get($data, 'extras.project-video'),
            'image' => $image,
            'about' => [
                0 => [
                    'title' => Arr::get($data, 'extras.about-1-title'),
                    'text' => Arr::get($data, 'extras.about-1-text'),
                ],
                1 => [
                    'title' => Arr::get($data, 'extras.about-2-title'),
                    'text' => Arr::get($data, 'extras.about-2-text'),
                ],
                2 => [
                    'title' => Arr::get($data, 'extras.about-3-title'),
                    'text' => Arr::get($data, 'extras.about-3-text'),
                ],
            ],
            'advantages' => [
                0 => Arr::get($data, 'extras.project-advantage-1'),
                1 => Arr::get($data, 'extras.project-advantage-2'),
                2 => Arr::get($data, 'extras.project-advantage-3'),
            ],
            'project' => [
                'title' => Arr::get($data, 'extras.project-subtitle'),
                'text' => Arr::get($data, 'extras.project-text'),
            ],
        ];

        return $data;
    }

    /**
     * @param $personThemes
     * @return array
     */
    public function prepareSliderData($personThemes): array
    {
        if (! $personThemes instanceof Collection) {
            return [];
        }

        $slides = $personThemes->toArray();
        $slides = array_map(function ($slide) {
            $image = Arr::get($slide, 'person.image');
            $sliderImage = Arr::get($slide, 'person.slider_image');
            return [
                'person' => Arr::get($slide, 'person.title'),
                'title' => Arr::get($slide, 'title'),
                'image' => $this->getImagePath($image, 'image'),
                'slider_image' => $this->getImagePath($sliderImage, 'slider_image'),
            ];
        }, $slides);

        return $slides;
    }

    /**
     * Get a full image/placeholder path.
     *
     * @param string|null $image
     * @param string $imageField
     * @return string|null
     */
    public function getImagePath($image, string $imageField): ?string
    {
        if ($image !== null && $image !== '') {
            return Storage::disk('images')->url($image);
        }

        $image = $this->personImagesPlaceholders[$imageField];
        if ($image !== null) {
            return Storage::disk(Page::IMAGES_DISK)->url($image);
        }

        return null;
    }
}