<?php

namespace App\Helpers\Page;


use App\Helpers\PricingPlans\PricingPlans;

/**
 * Trait PageTemplates
 * @package App\Helpers\Page
 * @mixin \Backpack\CRUD\app\Http\Controllers\CrudController
 */
trait PageTemplates
{
    /*
    |--------------------------------------------------------------------------
    | Page Templates for Backpack\PageManager
    |--------------------------------------------------------------------------
    |
    | Each page template has its own method, that define what fields should show up using the Backpack\CRUD API.
    | Use snake_case for naming and PageManager will make sure it looks pretty in the create/update form
    | template dropdown.
    |
    | Any fields defined here will show up after the standard page fields:
    | - select template
    | - page name (only seen by admins)
    | - page title
    | - page slug
    */

    /**
     *
     */
    private function home()
    {
        $this->crud->addFields([
            [
                'name' => 'separator_about_1',
                'type' => 'custom_html',
                'value' => '<h4>О нас - блок 1</h4>'
            ],
            [
                'name' => 'about-1-title',
                'type' => 'text',
                'label' => 'Заголовок',
                'attributes' => [
                    'required' => 'required',
                ],
                'fake' => true,
            ],
            [
                'name' => 'about-1-text',
                'type' => 'textarea',
                'label' => 'Текст',
                'attributes' => [
                    'required' => 'required',
                    'rows' => 2,
                ],
                'fake' => true,
            ],
            [
                'name' => 'separator_about_2',
                'type' => 'custom_html',
                'value' => '<h4>О нас - блок 2</h4>'
            ],
            [
                'name' => 'about-2-title',
                'type' => 'text',
                'label' => 'Заголовок',
                'attributes' => [
                    'required' => 'required',
                ],
                'fake' => true,
            ],
            [
                'name' => 'about-2-text',
                'type' => 'textarea',
                'label' => 'Текст',
                'attributes' => [
                    'required' => 'required',
                    'rows' => 2,
                ],
                'fake' => true,
            ],
            [
                'name' => 'separator_about_3',
                'type' => 'custom_html',
                'value' => '<h4>О нас - блок 3</h4>'
            ],
            [
                'name' => 'about-3-title',
                'type' => 'text',
                'label' => 'Заголовок',
                'attributes' => [
                    'required' => 'required',
                ],
                'fake' => true,
            ],
            [
                'name' => 'about-3-text',
                'type' => 'textarea',
                'label' => 'Текст',
                'attributes' => [
                    'required' => 'required',
                    'rows' => 2,
                ],
                'fake' => true,
            ],
            [
                'name' => 'separator_project',
                'type' => 'custom_html',
                'value' => '<h4>О проекте</h4>'
            ],
            [
                'name' => 'project-subtitle',
                'type' => 'textarea',
                'label' => 'Подзаголовок',
                'attributes' => [
                    'required' => 'required',
                    'rows' => 2,
                ],
                'fake' => true,
            ],
            [
                'name' => 'project-text',
                'type' => 'textarea',
                'label' => 'Продающий текст',
                'attributes' => [
                    'required' => 'required',
                    'rows' => 6,
                ],
                'fake' => true,
            ],
            [
                'name' => 'project-advantage-1',
                'type' => 'textarea',
                'label' => 'Преимущество №1',
                'attributes' => [
                    'required' => 'required',
                    'rows' => 2,
                ],
                'fake' => true,
            ],
            [
                'name' => 'project-advantage-2',
                'type' => 'textarea',
                'label' => 'Преимущество №2',
                'attributes' => [
                    'required' => 'required',
                    'rows' => 2,
                ],
                'fake' => true,
            ],
            [
                'name' => 'project-advantage-3',
                'type' => 'textarea',
                'label' => 'Преимущество №3',
                'attributes' => [
                    'required' => 'required',
                    'rows' => 2,
                ],
                'fake' => true,
            ],
            [
                'name' => 'image',
                'type' => 'image',
                'label' => 'Обложка ролика о проекте',
                'upload' => true,
                'disk' => 'uploads',
                'crop' => true,
                'aspect_ratio' => 1.7773359841,
                'hint' => 'Изображение размером 895x505 пикселей',
            ],
            [
                'name' => 'project-video',
                'type' => 'textarea',
                'label' => 'Iframe видео о проекте (Youtube/Vimeo)',
                'attributes' => [
                    'required' => 'required',
                ],
                'fake' => true,
            ]
        ]);
    }

    public function pricing()
    {
        $this->crud->addFields([
            // Ultimate
            [
                'type' => 'text',
                'name' => PricingPlans::ULTIMATE['key'] . '-name',
                'label' => 'Название',
                'attributes' => [
                    'required' => 'required',
                ],
                'fake' => true,
                'tab' => PricingPlans::ULTIMATE['label'],
            ],
            [
                'type' => 'textarea',
                'name' => PricingPlans::ULTIMATE['key'] . '-desc',
                'label' => 'Описание',
                'attributes' => [
                    'required' => 'required',
                    'rows' => 2,
                ],
                'fake' => true,
                'tab' => PricingPlans::ULTIMATE['label'],
            ],
            [
                'type' => 'number',
                'name' => PricingPlans::ULTIMATE['key'] . '-price',
                'label' => 'Стоимость (RUB)',
                'attributes' => [
                    'required' => 'required',
                ],
                'suffix' => '₽',
                'fake' => true,
                'tab' => PricingPlans::ULTIMATE['label'],
            ],
            // Ultimate Plus
            [
                'type' => 'text',
                'name' => PricingPlans::ULTIMATE_PLUS['key'] . '-name',
                'label' => 'Название',
                'attributes' => [
                    'required' => 'required',
                ],
                'fake' => true,
                'tab' => PricingPlans::ULTIMATE_PLUS['label'],
            ],
            [
                'type' => 'textarea',
                'name' => PricingPlans::ULTIMATE_PLUS['key'] . '-desc',
                'label' => 'Описание',
                'attributes' => [
                    'required' => 'required',
                    'rows' => 2,
                ],
                'fake' => true,
                'tab' => PricingPlans::ULTIMATE_PLUS['label'],
            ],
            [
                'type' => 'number',
                'name' => PricingPlans::ULTIMATE_PLUS['key'] . '-price',
                'label' => 'Стоимость (RUB)',
                'attributes' => [
                    'required' => 'required',
                ],
                'suffix' => '₽',
                'fake' => true,
                'tab' => PricingPlans::ULTIMATE_PLUS['label'],
            ],
            // Course Access
            [
                'type' => 'text',
                'name' => PricingPlans::COURSE_ACCESS['key'] . '-name',
                'label' => 'Название',
                'attributes' => [
                    'required' => 'required',
                ],
                'fake' => true,
                'tab' => PricingPlans::COURSE_ACCESS['label'],
            ],
            [
                'type' => 'textarea',
                'name' => PricingPlans::COURSE_ACCESS['key'] . '-desc',
                'label' => 'Описание',
                'attributes' => [
                    'required' => 'required',
                    'rows' => 2,
                ],
                'fake' => true,
                'tab' => PricingPlans::COURSE_ACCESS['label'],
            ],
            [
                'type' => 'number',
                'name' => PricingPlans::COURSE_ACCESS['key'] . '-price',
                'label' => 'Стоимость (RUB)',
                'attributes' => [
                    'required' => 'required',
                ],
                'suffix' => '₽',
                'fake' => true,
                'tab' => PricingPlans::COURSE_ACCESS['label'],
            ],
            // Person Access
            [
                'type' => 'text',
                'name' => PricingPlans::PERSON_ACCESS['key'] . '-name',
                'label' => 'Название',
                'attributes' => [
                    'required' => 'required',
                ],
                'fake' => true,
                'tab' => PricingPlans::PERSON_ACCESS['label'],
            ],
            [
                'type' => 'textarea',
                'name' => PricingPlans::PERSON_ACCESS['key'] . '-desc',
                'label' => 'Описание',
                'attributes' => [
                    'required' => 'required',
                    'rows' => 2,
                ],
                'fake' => true,
                'tab' => PricingPlans::PERSON_ACCESS['label'],
            ],
            [
                'type' => 'number',
                'name' => PricingPlans::PERSON_ACCESS['key'] . '-price',
                'label' => 'Стоимость (RUB)',
                'attributes' => [
                    'required' => 'required',
                ],
                'suffix' => '₽',
                'fake' => true,
                'tab' => PricingPlans::PERSON_ACCESS['label'],
            ],
        ]);
    }

    public function courses()
    {
        $this->crud->addFields([
            [
                'name' => 'image',
                'type' => 'image',
                'label' => 'Плейсхолдер превью-изображения',
                'upload' => true,
                'disk' => 'uploads',
                'crop' => true,
                'aspect_ratio' => 2,
                'hint' => 'Изображение размером 1200x600 пикселей',
            ],
            [
                'name' => 'image_1',
                'type' => 'image',
                'label' => 'Плейсхолдер изображения для списков',
                'upload' => true,
                'disk' => 'uploads',
                'crop' => true,
                'aspect_ratio' => 1.61643835616,
                'hint' => 'Изображение размером 590x365 пикселей',
            ],
        ]);
    }

    public function persons()
    {
        $this->crud->addFields([
            [
                'name' => 'image',
                'type' => 'image',
                'label' => 'Плейсхолдер превью-изображения',
                'upload' => true,
                'disk' => 'uploads',
                'crop' => true,
                'aspect_ratio' => 2,
                'hint' => 'Изображение размером 1200x600 пикселей',
            ],
            [
                'name' => 'image_1',
                'type' => 'image',
                'label' => 'Плейсхолдер изображения для списков',
                'upload' => true,
                'disk' => 'uploads',
                'crop' => true,
                'aspect_ratio' => 1.61643835616,
                'hint' => 'Изображение размером 590x365 пикселей',
            ],
            [
                'name' => 'image_2',
                'type' => 'image',
                'label' => 'Плейсхолдер изображения для слайдера',
                'hint' => 'Изображение размером 1370x675 пикселей',
                'upload' => true,
                'disk' => 'uploads',
                'crop' => true,
                'aspect_ratio' => 2.02962962963,
            ],
        ]);
    }

    public function achievements()
    {
        $this->crud->addFields([
            [
                'name' => 'contest_sep',
                'type' => 'custom_html',
                'value' => '<h4>Информация о конкурсе</h4>',
            ],
            [
                'name' => 'image',
                'type' => 'image',
                'label' => 'Превью-изображение конкурса',
                'upload' => true,
                'disk' => 'uploads',
                'crop' => true,
                'aspect_ratio' => 1.475,
                'hint' => 'Изображение размером 590x400 пикселей',
            ],
            [
                'name' => 'contest-title',
                'type' => 'text',
                'label' => 'Название',
                'attributes' => [
                    'required' => 'required',
                ],
                'fake' => true,
            ],
            [
                'name' => 'contest-date',
                'type' => 'text',
                'label' => 'Дата проведения',
                'fake' => true,
            ],
            [
                'name' => 'contest-desc',
                'type' => 'textarea',
                'label' => 'Краткое описание',
                'attributes' => [
                    'rows' => 4,
                    'required' => 'required',
                ],
                'fake' => true,
            ],
            [
                'name' => 'contest-full-desc',
                'type' => 'summernote',
                'label' => 'Полное описание конкурса',
                'fake' => true,
                'hint' => 'Если заполнено, будет отображаться в соответствующем модальном окне',
            ],
            [
                'name' => 'contest-prize',
                'type' => 'text',
                'label' => 'Название приза',
                'fake' => true,
            ],
            [
                'name' => 'contest-prize-desc',
                'type' => 'textarea',
                'label' => 'Краткое описание награды',
                'attributes' => [
                    'rows' => 2,
                ],
                'fake' => true,
            ],
            [
                'name' => 'winner_sep',
                'type' => 'custom_html',
                'value' => '<h4>Информация о победителе</h4>',
            ],
            [
                'name' => 'image_1',
                'type' => 'image',
                'label' => 'Фото победителя',
                'upload' => true,
                'disk' => 'uploads',
                'crop' => true,
                'aspect_ratio' => 1.15662650602,
                'hint' => 'Изображение размером 480x415 пикселей',
            ],
            [
                'name' => 'contest-winner-title',
                'type' => 'text',
                'label' => 'Имя',
                'attributes' => [
                    'required' => 'required',
                ],
                'fake' => true,
            ],
            [
                'name' => 'contest-winner-city',
                'type' => 'text',
                'label' => 'Город',
                'fake' => true,
            ],
            [
                'name' => 'contest-winner-desc',
                'type' => 'textarea',
                'label' => 'Краткое описание',
                'attributes' => [
                    'rows' => 2,
                ],
                'fake' => true,
            ],
        ]);
    }
}
