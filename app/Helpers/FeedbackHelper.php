<?php

namespace App\Helpers;

use Illuminate\Support\Arr;

/**
 * Class FeedbackHelper
 * @package App\Helpers
 */
class FeedbackHelper
{
    /**
     * @param $data
     * @return array
     */
    public function prepareApplicationData($data): array
    {
        return [
            'contact' => Arr::get($data, 'contact'),
            'phone' => Arr::get($data, 'phone'),
            'email' => Arr::get($data, 'email'),
            'message' => Arr::get($data, 'message'),
            'subject' => Arr::get($data, 'subject'),
        ];
    }
}