<?php

namespace App\Helpers;

use App\Models\Page;
use App\Models\Person;
use App\Models\PersonTheme;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Storage;
use Throwable;

/**
 * Class PersonHelper
 * @package App\Helpers
 */
class PersonHelper
{
    /**
     * @param \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection $persons
     * @return array
     */
    public function prepareItemsData($persons): array
    {
        if (! $persons instanceof Collection) {
            return [];
        }

        $items = $persons->toArray();
        $items = array_map(function ($item) {
            $image = Arr::get($item, 'image');
            $bigImage = Arr::get($item, 'big_image');

            return [
                'title' => Arr::get($item, 'title'),
                'alias' => Arr::get($item, 'alias'),
                'price' => Arr::get($item, 'price'),
                'trailer_code' => Arr::get($item, 'trailer_code'),
                'show_as_main' => (bool) Arr::get($item, 'extras.show_as_main'),
                'image' => $this->getImage($image),
                'big_image' => $this->getBigImage($bigImage),
                'advantages' => [
                    0 => [
                        'title' => Arr::get($item, 'extras.advantage-1-title'),
                        'text' => Arr::get($item, 'extras.advantage-1-text'),
                    ],
                    1 => [
                        'title' => Arr::get($item, 'extras.advantage-2-title'),
                        'text' => Arr::get($item, 'extras.advantage-2-text'),
                    ],
                ],
                'offer' => [
                    'title' => Arr::get($item, 'extras.offer-title'),
                    'text' => Arr::get($item, 'extras.offer-text'),
                    'image' => Storage::disk('images')->url($item['offer_image']),
                ],
            ];
        }, $items);

        return $items;
    }

    /**
     * @param \App\Models\Person|\Illuminate\Database\Eloquent\Builder $person
     * @return array
     */
    public function prepareItemData($person): array
    {
        if (! $person instanceof Person && ! $person instanceof Builder) {
            return [];
        }

        $themes = $person->themes;
        $item = $person->toArray();

        $image = Arr::get($item, 'image');
        $bigImage = Arr::get($item, 'big_image');

        $item = [
            'title' => Arr::get($item, 'title'),
            'alias' => Arr::get($item, 'alias'),
            'price' => Arr::get($item, 'price'),
            'trailer_code' => Arr::get($item, 'trailer_code'),
            'themes' => $this->prepareThemesData($themes),
            'show_as_main' => (bool) Arr::get($item, 'extras.show_as_main'),
            'image' => $this->getImage($image),
            'big_image' => $this->getBigImage($bigImage),
            'advantages' => [
                0 => [
                    'title' => Arr::get($item, 'extras.advantage-1-title'),
                    'text' => Arr::get($item, 'extras.advantage-1-text'),
                ],
                1 => [
                    'title' => Arr::get($item, 'extras.advantage-2-title'),
                    'text' => Arr::get($item, 'extras.advantage-2-text'),
                ],
            ],
            'values' => [
                0 => [
                    'title' => Arr::get($item, 'extras.value-1-title'),
                    'text' => Arr::get($item, 'extras.value-1-text'),
                ],
                1 => [
                    'title' => Arr::get($item, 'extras.value-2-title'),
                    'text' => Arr::get($item, 'extras.value-2-text'),
                ],
                2 => [
                    'title' => Arr::get($item, 'extras.value-3-title'),
                    'text' => Arr::get($item, 'extras.value-3-text'),
                ],
            ],
            'offer' => [
                'title' => Arr::get($item, 'extras.offer-title'),
                'text' => Arr::get($item, 'extras.offer-text'),
                'image' => Storage::disk('images')->url($item['offer_image']),
            ],
            'guaranty' => [
                'title' => Arr::get($item, 'extras.guaranty-title'),
                'text' => Arr::get($item, 'extras.guaranty-text'),
            ]
        ];

        return $item;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Collection $themes
     * @return array
     */
    public function prepareThemesData($themes): array
    {
        if (! $themes instanceof Collection) {
            return [];
        }

        $items = $themes->toArray();
        $items = array_map(function ($item, $index) {
            $description = Arr::get($item, 'description');
            $description = Str::limit($description);
            return [
                'title' => Arr::get($item, 'title'),
                'description' => $description,
                'number' => str_pad(++$index, 2, '0', STR_PAD_LEFT),
                'alias' => Arr::get($item, 'alias'),
            ];
        }, $items, array_keys($items));
        
        return $items;
    }

    /**
     * @param \App\Models\PersonTheme|\Illuminate\Database\Eloquent\Builder $theme
     * @return array
     */
    public function prepareThemeData($theme): array
    {
        if (! $theme instanceof PersonTheme && ! $theme instanceof Builder) {
            return [];
        }

        $item = $theme->toArray();
        $item = [
            'title' => Arr::get($item, 'title'),
            'description' => Arr::get($item, 'description'),
            'video_code' => Arr::get($item, 'video_code'),
            'alias' => Arr::get($item, 'alias'),
        ];

        return $item;
    }

    /**
     * Get highlighted person.
     *
     * @param array $persons
     * @return array
     */
    public function getHighlightedPerson(array $persons): array
    {
        foreach ($persons as $person) {
            if ($person['show_as_main'] === true) {
                return $person;
            }
        }

        $highlightedPerson = end($persons);
        return $highlightedPerson;
    }

    /**
     * Return an image path (or placeholder image path).
     *
     * @param $imagePath
     * @return mixed
     */
    public function getImage($imagePath)
    {
        if ($imagePath !== null && $imagePath !== '') {
            return Storage::disk('images')->url($imagePath);
        }

        try {
            $config = Page::findBySlugOrFail('/persons');
            $config = $config->toArray();
        } catch (Throwable $exception) {
            abort(404);
        }

        $placeholderImagePath = Arr::get($config, 'image_1');
        return Storage::disk('uploads')->url($placeholderImagePath);
    }

    /**
     * Return a preview image path (or placeholder image path).
     *
     * @param $imagePath
     * @return mixed
     */
    public function getBigImage($imagePath)
    {
        if ($imagePath !== null && $imagePath !== '') {
            return Storage::disk('images')->url($imagePath);
        }

        try {
            $config = Page::findBySlugOrFail('/persons');
            $config = $config->toArray();
        } catch (Throwable $exception) {
            abort(404);
        }

        $placeholderImagePath = Arr::get($config, 'image');
        return Storage::disk('uploads')->url($placeholderImagePath);
    }
}