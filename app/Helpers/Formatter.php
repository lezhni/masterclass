<?php

namespace App\Helpers;

/**
 * Class Formatter
 * @package App\Helpers
 */
class Formatter
{
    /**
     * Format a price.
     *
     * @param $price
     * @return string
     */
    public function formatPrice($price): string
    {
        $price = (int) $price;
        $price = number_format($price, 0, '.', ' ');
        return "{$price} руб.";
    }

    /**
     * Format a monthly price.
     *
     * @param $price
     * @return string
     */
    public function formatPricePerMonth($price): string
    {
        $price = (int) $price;
        $price = number_format($price, 0, '.', ' ');
        return "{$price} руб. / мес";
    }

    public function formatWithCurrency($sum): string
    {
        $sum = (int) $sum;
        return $this->declOfNum($sum, ['рубль', 'рубля', 'рублей']);
    }

    /**
     * Format a duration of pricing plan.
     *
     * @param $duration
     * @return string
     */
    public function formatDurationInMonths($duration): string
    {
        return $this->declOfNum($duration, ['месяц', 'месяца', 'месяцев']);
    }

    /**
     * @param int $number
     * @param array $titles
     * @return string
     */
    private function declOfNum($number, array $titles): string
    {
        $cases = [2, 0, 1, 1, 1, 2];
        return $number . " " . $titles[ ($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[ min($number % 10, 5) ] ];
    }
}