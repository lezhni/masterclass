<?php

namespace App\Console\Commands;

use App\Models\Subscription;
use Illuminate\Console\Command;

class ClearExpiredSubscriptions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscriptions:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deletes subscriptions, which already expired';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Subscription::expired()->delete();
    }
}
