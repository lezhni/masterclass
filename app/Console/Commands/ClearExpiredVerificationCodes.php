<?php

namespace App\Console\Commands;

use App\Models\VerificationCode;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class ClearExpiredVerificationCodes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'verification-codes:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deletes verification codes, which expired more than a day ago';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dayAgo = Carbon::now()->subDay();
        VerificationCode::where('expires_at', '<=', $dayAgo)->delete();
    }
}
